const path = require('path');

module.exports = {
  module: {
    loaders: [
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
        include: path.resolve(__dirname, '../'),
      },
      {
        test: /\.svg/,
        loader: 'file-loader',
      },
      {
        test: /\.(eot|ttf|woff|woff2)/,
        loader: 'url-loader',
      },
    ],
  },
};
