import React from 'react';
import { configure, addDecorator } from '@kadira/storybook';
import { createStore } from 'redux';
import { combineReducers } from 'redux-immutable';
import { Provider } from 'react-redux';

import chatReducer from '../src/reducer';
import chatMessagesReducer from '../src/Message/reducer';

import '../node_modules/react-chat-theme-default/src/index.scss';

// stories loader
const req = require.context('../src/', true, /\.stories\.js?$/);
function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

const reducers = {
  chat: chatReducer,
  chatMessagesReducer
};
const reducer = combineReducers(reducers);
const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// add store decorator
addDecorator((story) => (
  <Provider store={store}>
    {story()}
  </Provider>
));

configure(loadStories, module);
