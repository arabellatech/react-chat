// flow-typed signature: ac5bdb95c144116d29dd2c27c0380672
// flow-typed version: <<STUB>>/api-client_vhttps://bitbucket.org/arabellatech/api-client#~1.3.0/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'api-client'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'api-client' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'api-client/lib/decamelizeOrderingParam' {
  declare module.exports: any;
}

declare module 'api-client/lib/index' {
  declare module.exports: any;
}

declare module 'api-client/lib/stringifyParams' {
  declare module.exports: any;
}

declare module 'api-client/lib/toJS' {
  declare module.exports: any;
}

declare module 'api-client/scripts/coverage' {
  declare module.exports: any;
}

declare module 'api-client/src/decamelizeOrderingParam' {
  declare module.exports: any;
}

declare module 'api-client/src/decamelizeOrderingParam.test' {
  declare module.exports: any;
}

declare module 'api-client/src/index' {
  declare module.exports: any;
}

declare module 'api-client/src/index.test' {
  declare module.exports: any;
}

declare module 'api-client/src/stringifyParams' {
  declare module.exports: any;
}

declare module 'api-client/src/stringifyParams.test' {
  declare module.exports: any;
}

declare module 'api-client/src/toJS' {
  declare module.exports: any;
}

declare module 'api-client/src/toJS.test' {
  declare module.exports: any;
}

// Filename aliases
declare module 'api-client/lib/decamelizeOrderingParam.js' {
  declare module.exports: $Exports<'api-client/lib/decamelizeOrderingParam'>;
}
declare module 'api-client/lib/index.js' {
  declare module.exports: $Exports<'api-client/lib/index'>;
}
declare module 'api-client/lib/stringifyParams.js' {
  declare module.exports: $Exports<'api-client/lib/stringifyParams'>;
}
declare module 'api-client/lib/toJS.js' {
  declare module.exports: $Exports<'api-client/lib/toJS'>;
}
declare module 'api-client/scripts/coverage.js' {
  declare module.exports: $Exports<'api-client/scripts/coverage'>;
}
declare module 'api-client/src/decamelizeOrderingParam.js' {
  declare module.exports: $Exports<'api-client/src/decamelizeOrderingParam'>;
}
declare module 'api-client/src/decamelizeOrderingParam.test.js' {
  declare module.exports: $Exports<'api-client/src/decamelizeOrderingParam.test'>;
}
declare module 'api-client/src/index.js' {
  declare module.exports: $Exports<'api-client/src/index'>;
}
declare module 'api-client/src/index.test.js' {
  declare module.exports: $Exports<'api-client/src/index.test'>;
}
declare module 'api-client/src/stringifyParams.js' {
  declare module.exports: $Exports<'api-client/src/stringifyParams'>;
}
declare module 'api-client/src/stringifyParams.test.js' {
  declare module.exports: $Exports<'api-client/src/stringifyParams.test'>;
}
declare module 'api-client/src/toJS.js' {
  declare module.exports: $Exports<'api-client/src/toJS'>;
}
declare module 'api-client/src/toJS.test.js' {
  declare module.exports: $Exports<'api-client/src/toJS.test'>;
}
