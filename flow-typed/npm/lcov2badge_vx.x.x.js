// flow-typed signature: 71501314a9fd4d4f3dd7b8eb05680654
// flow-typed version: <<STUB>>/lcov2badge_v^0.1.0/flow_v0.46.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'lcov2badge'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'lcov2badge' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'lcov2badge/gulpfile' {
  declare module.exports: any;
}

declare module 'lcov2badge/test/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'lcov2badge/gulpfile.js' {
  declare module.exports: $Exports<'lcov2badge/gulpfile'>;
}
declare module 'lcov2badge/index' {
  declare module.exports: $Exports<'lcov2badge'>;
}
declare module 'lcov2badge/index.js' {
  declare module.exports: $Exports<'lcov2badge'>;
}
declare module 'lcov2badge/test/index.js' {
  declare module.exports: $Exports<'lcov2badge/test/index'>;
}
