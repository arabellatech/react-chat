[![bitHound Overall Score](https://www.bithound.io/bitbucket/arabellatech/react-chat/badges/score.svg)](https://www.bithound.io/bitbucket/arabellatech/react-chat)
[![bitHound Dependencies](https://www.bithound.io/bitbucket/arabellatech/react-chat/badges/dependencies.svg)](https://www.bithound.io/bitbucket/arabellatech/react-chat/develop/dependencies/npm)
[![bitHound Dev Dependencies](https://www.bithound.io/bitbucket/arabellatech/react-chat/badges/devDependencies.svg)](https://www.bithound.io/bitbucket/arabellatech/react-chat/develop/dependencies/npm)
[![bitHound Code](https://www.bithound.io/bitbucket/arabellatech/react-chat/badges/code.svg)](https://www.bithound.io/bitbucket/arabellatech/react-chat)
![IMAGE](./coverage.svg)

## Quick start

## Install example component in your-project
1. Run `yarn add https://bitbucket.org/arabellatech/react-chat`

## Contribution
1. Run `yarn global add getstorybook` to instal StoryBook.
1. Clone this repo using `git clone git@bitbucket.org:arabellatech/react-chat.git`
1. Run `cd react-chat/`
1. Run `yarn` to install dependencies.
1. Run `yarn run storybook` to start StoryBook.
1. Run `yarn watch` to rebuild lib automatically on file change.

## Link local package to your project
1. Run `cd react-chat/`
1. Run `yarn link` to create a symbolic link from a global folder to the react-chat/ folder.
1. Run `cd your-project-dir`
1. Run `yarn link react-chat`

## Unlink local package to your project
1. Run `cd your-project-dir`
1. Run `yarn unlink react-chat`
1. Run `yarn` to install back the repo version

## Upgrading dependencies
1. Run `yarn`
1. Commit `package.json` and `yarn.lock` changes
1. Run `flow-typed install --overwrite`
1. Check if `yarn flow` returns any new errors, if not, commit `flow-typed/` changes
