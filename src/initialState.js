/* @flow */

import {
  fromJS,
} from 'immutable';

export default fromJS({
  loadingSchema: false,
  loadingMessages: false,
  showAnimations: false,
  questions: [],
  currentQuestion: {
    answers: [],
  },
  answeredQuestions: [],
  questionsReplies: [],
  schemaQueueStatus: {
    isStarted: false,
    isEnded: false,
    isFailed: false,
    failureReason: null,
    progress: 0,
  },
  schemaId: null,
  messages: [],
  thread: null,
  threadId: null,
  animations: {},
  customVariables: {},
  variablesPool: {},
  variablesPoolHistory: [],
});
