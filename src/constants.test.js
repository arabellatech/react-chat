/* @flow */

import _mapKeys from 'lodash/mapKeys';

import * as messageContants from './Message/constants';
import * as schemaContants from './Schema/constants';
import * as threadContants from './Thread/constants';
import * as variablesPoolContants from './VariablesPool/constants';
import * as allContants from './constants';

describe('export: constants', () => {
  it('should export all message constants', () => {
    expect(Object.keys(messageContants)).toMatchSnapshot();

    _mapKeys(
      messageContants,
      (selector, selectorName) => expect(allContants[selectorName]).toBe(messageContants[selectorName])
    );
  });

  it('should export all schema constants', () => {
    expect(Object.keys(schemaContants)).toMatchSnapshot();

    _mapKeys(
      schemaContants,
      (selector, selectorName) => expect(allContants[selectorName]).toBe(schemaContants[selectorName])
    );
  });

  it('should export all thread constants', () => {
    expect(Object.keys(threadContants)).toMatchSnapshot();

    _mapKeys(
      threadContants,
      (selector, selectorName) => expect(allContants[selectorName]).toBe(threadContants[selectorName])
    );
  });

  it('should export all variables pool constants', () => {
    expect(Object.keys(variablesPoolContants)).toMatchSnapshot();

    _mapKeys(
      variablesPoolContants,
      (selector, selectorName) => expect(allContants[selectorName]).toBe(variablesPoolContants[selectorName])
    );
  });
});
