import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Chat from './index';

storiesOf('Chat', module)
  .add('without messages', () => (
    <Chat />
  ));
