/* @flow */

import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import ReactResizeDetector from 'react-resize-detector';
import { Map } from 'immutable';
import fecha from 'fecha';
import _isEmpty from 'lodash/isEmpty';
import _map from 'lodash/map';
import MessagesGroup from '../MessagesGroup';

import type {
  ChatProps,
  ChatMessagesGroupProps,
} from '../types';

import {
  chatThreadRollbackAction,
} from '../Schema/actions';

import {
  chatLoadMessagesAction,
  chatResetMessagesAction,
  chatSetSenderIdAction,
} from '../Message/actions';

import {
  selectChatAnimations,
} from './selectors';

import {
  selectChatCustomVariables,
} from '../VariablesPool/selectors';

import {
  selectChatShowAnimations,
  selectChatLoadingMessagesList,
  selectChatMessagesGrouppedByDate,
} from '../Message/selectors';
import {
  stickToBottom,
  getStartedAnimations,
} from '../utils/animations';

const mapStateToProps = (state) => ({
  showAnimations: selectChatShowAnimations(state),
  loadingMessages: selectChatLoadingMessagesList(state),
  messages: selectChatMessagesGrouppedByDate(state),
  animations: selectChatAnimations(state),
  customVariables: selectChatCustomVariables(state),
});

const mapDispatchToProps = {
  loadMessages: chatLoadMessagesAction,
  resetMessages: chatResetMessagesAction,
  rollbackThread: chatThreadRollbackAction,
  setSenderId: chatSetSenderIdAction,
};

class Chat extends PureComponent {
  static defaultProps = {
    messages: Map(),
    threadId: null,
    loadingMessages: false,
    messageDateFormat: null,
    messageTimeFormat: null,
    showAvatarsOnRight: false,
  };

  componentDidMount() {
    const {
      loadMessages,
      resetMessages,
      senderId,
      setSenderId,
      threadId,
    } = this.props;

    resetMessages();

    if (threadId) {
      loadMessages(threadId);
      setSenderId(senderId);
    }
  }

  componentWillReceiveProps(nextProps) {
    const startedAnimations = getStartedAnimations(this.props.animations, nextProps.animations);

    if (this.props.senderId !== nextProps.senderId) {
      this.props.setSenderId(nextProps.senderId);
    }

    if (this.props.threadId && this.props.threadId !== nextProps.threadId) {
      this.props.resetMessages();
    }

    if (
      !_isEmpty(startedAnimations)
      ||
      (this.props.messages.size === 0 && nextProps.messages.size > 0)
    ) {
      this.stickToBottom(1000);
    }
  }

  getOnEditClickHandler() {
    const {
      onEditClick,
      threadId,
      rollbackThread,
    } = this.props;

    if (!onEditClick) {
      return null;
    }

    const editHandler = (feId?: string, questionId?: ?number) => onEditClick(threadId, feId, questionId, rollbackThread);

    return editHandler;
  }

  props: ChatProps;
  chatWindow: HTMLElement;
  currentChatWindowHeight: number;

  chatWindowResize = (width, height) => {
    if (height < this.currentChatWindowHeight) {
      this.stickToBottom(0, true);
    }
    this.currentChatWindowHeight = height;
  }

  stickToBottom = (time = 1000, once = false) => {
    setTimeout(() => {
      stickToBottom(this.chatWindow, time, once);
    }, 0);
  }

  setChatWindowRef = (chatWindow) => {
    this.chatWindow = chatWindow;

    if (chatWindow) {
      this.currentChatWindowHeight = chatWindow.offsetHeight;
    }
  }

  renderEmptyList() {
    const { renderEmptyList } = this.props;

    if (renderEmptyList) {
      return renderEmptyList();
    }

    return null;
  }

  renderMessages() {
    const { messages } = this.props;

    if (!messages.size) {
      return this.renderEmptyList();
    }

    const messagesGroups = _map(
      messages.toJS(),
      this.renderMessagesGroupsWithDate,
    );

    return (
      <div key="chatMessages" className="chat-window-wrap" ref={this.setChatWindowRef}>
        <div className="chat-window">
          <div className="chat-window-inner">
            {messagesGroups}
          </div>
        </div>

        <ReactResizeDetector handleHeight onResize={this.chatWindowResize} />
      </div>
    );
  }

  renderMessagesGroupsWithDate = (messagesGroups, date: string) => {
    const datePartial = this.renderDate(date);
    const groupsPartial = messagesGroups.map(this.renderMessagesGroup);

    return (
      <Fragment key={date}>
        {datePartial}
        {groupsPartial}
      </Fragment>
    );
  }

  renderDate(date: string) {
    const {
      messageDateFormat,
    } = this.props;

    if (!messageDateFormat || !date || date === 'NO_DATE') {
      return null;
    }

    const dateFormatted = fecha.format(date, messageDateFormat);

    return (
      <div className="chat-msg-date">
        {dateFormatted}
      </div>
    );
  }

  renderMessagesGroup = (messagesGroup, key) => {
    const {
      incomingSender,
      outgoingSender,
      showAnimations,
      renderMessagesGroup,
      customVariables,
      messageDateFormat,
      messageTimeFormat,
      showAvatarsOnRight,
      renderAvatar,
      renderNoSenderAvatar,
      renderTimestamp,
      renderMessageFooter,
    } = this.props;

    const groupProps: ChatMessagesGroupProps = {
      incomingSender,
      outgoingSender,
      showAnimations,
      customVariables,
      key,
      messageDateFormat,
      messageTimeFormat,
      messagesGroup,
      showAvatarsOnRight,
      renderAvatar,
      renderNoSenderAvatar,
      renderTimestamp,
      renderMessageFooter,
    };
    const editClickHandler = this.getOnEditClickHandler();

    if (editClickHandler) {
      groupProps.onEditClick = editClickHandler;
    }

    if (renderMessagesGroup) {
      return renderMessagesGroup(groupProps);
    }

    return (
      <MessagesGroup {...groupProps} />
    );
  }

  renderMessageForm() {
    const {
      threadId,
      renderMessageForm,
    } = this.props;

    if (!renderMessageForm) {
      return null;
    }

    return renderMessageForm(threadId);
  }

  render() {
    if (this.props.loadingMessages) {
      return this.renderEmptyList();
    }

    const messagesList = this.renderMessages();
    const messageForm = this.renderMessageForm();

    return (
      <div className="chat">
        {messagesList}
        {messageForm}
      </div>
    );
  }
}

// override state and dispatch props with own props
const mergeProps = (stateProps?, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(Chat);
