/* @flow */

import {
  List,
  Map,
} from 'immutable';
import { createSelector } from 'reselect';
import _indexOf from 'lodash/indexOf';

export type StateType = Map<string, any>;
export type StateListType = List<StateType>;

/* get */

export const getChatDomain = (state: StateType) => state.get('chat');
export const getAnimations = (state: StateType) => state.get('animations');

/* helpers */

export const findBy = (list: StateListType, paramName: string, paramValue: any) => list.find(
  (item) => item.get(paramName) === paramValue
);
export const filterBy = (list: StateListType, paramName: string, paramValue: any) => list.filter(
  (item) => item.get(paramName) === paramValue
);

export const filterByMultiple = (list: StateListType, paramName: string, paramValues: Array<any>) => list.filter(
  (item) => _indexOf(
    paramValues,
    item.get(paramName)
  ) > -1
);

export const listPick = (list: StateListType, paramName: string) => list.map(
  (item) => item.get(paramName)
);

/* top level selectors (for use in mapStateToProps or global reducers) */

export const selectChatAnimations = createSelector(
  getChatDomain,
  getAnimations,
);
