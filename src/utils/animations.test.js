/* @flow */

import {
  fromJS,
} from 'immutable';
import {
  getStartedAnimations,
} from './animations';

describe('animations', () => {
  describe('getStartedAnimations', () => {
    const noAnimations = fromJS({});
    const animationStarted = fromJS({
      animation: 'started',
    });
    const animationEnded = fromJS({
      animation: 'ended',
    });
    const customAnimationStarted = fromJS({
      animation: 'custom-started',
    });
    const customAnimationEnded = fromJS({
      animation: 'custom-ended',
    });

    describe('no animation started', () => {
      it(`should return ${noAnimations} when called with: ${noAnimations}, ${animationEnded}`, () => {
        const result = getStartedAnimations(noAnimations, animationEnded);

        expect(result).toEqual(noAnimations);
      });

      it(`should return ${noAnimations} when called with: ${noAnimations}, ${customAnimationEnded}`, () => {
        const result = getStartedAnimations(noAnimations, customAnimationEnded);

        expect(result).toEqual(noAnimations);
      });

      it(`should return ${noAnimations} when called with: ${animationStarted}, ${animationEnded}`, () => {
        const result = getStartedAnimations(animationStarted, animationEnded);

        expect(result).toEqual(noAnimations);
      });

      it(`should return ${noAnimations} when called with: ${customAnimationStarted}, ${customAnimationEnded}`, () => {
        const result = getStartedAnimations(customAnimationStarted, customAnimationEnded);

        expect(result).toEqual(noAnimations);
      });
    });

    describe('single animation started', () => {
      it(`should return ${animationStarted} when called with: ${noAnimations}, ${animationStarted}`, () => {
        const result = getStartedAnimations(noAnimations, animationStarted);

        expect(result).toEqual(animationStarted);
      });

      it(`should return ${customAnimationStarted} when called with: ${noAnimations}, ${customAnimationStarted}`, () => {
        const result = getStartedAnimations(noAnimations, customAnimationStarted);

        expect(result).toEqual(customAnimationStarted);
      });

      it(`should return ${animationStarted} when called with: ${animationEnded}, ${animationStarted}`, () => {
        const result = getStartedAnimations(animationEnded, animationStarted);

        expect(result).toEqual(animationStarted);
      });

      it(`should return ${customAnimationStarted} when called with: ${customAnimationEnded}, ${customAnimationStarted}`, () => {
        const result = getStartedAnimations(customAnimationEnded, customAnimationStarted);

        expect(result).toEqual(customAnimationStarted);
      });
    });
  });
});
