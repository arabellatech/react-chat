/* @flow */

import {
  Map,
} from 'immutable';

export const stickToBottom = (target: Element, time: number = 2000) => {
  const targetCopy = target;
  let canPlay = false;
  let lastTop = 0;

  canPlay = time > 0;

  function tick() {
    if (target) {
      const destScrollPos = target.scrollHeight - target.clientHeight;

      if (lastTop < destScrollPos) {
        lastTop = destScrollPos;
        targetCopy.scrollTop = destScrollPos;
      }
    }

    if (canPlay) {
      requestAnimationFrame(tick);
    }
  }

  tick();

  setTimeout(() => {
    canPlay = false;
  }, time);
};

export const getStartedAnimations = (animations: Map<string, string>, nextAnimations: Map<string, string>) => {
  const startedAnimations = nextAnimations.filter(
    (animationProgress = '', animationType) => (
      animations.get(animationType) !== animationProgress
      &&
      animationProgress.indexOf('started') > -1
    )
  );

  return startedAnimations;
};
