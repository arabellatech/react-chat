/* @flow */

import React from 'react';
import renderer from 'react-test-renderer';

import Bubble from './index';

import type {
  BubbleProps,
} from '../types';

import { shortText } from './index.helper';

describe('<Bubble />', () => {
  const renderWithProps = (props: BubbleProps) => renderer.create(
    <Bubble {...props}>{props.children}</Bubble>
  ).toJSON();

  it('should render bubble with text', () => {
    const testProps = {
      children: shortText,
    };

    const tree = renderWithProps({ ...testProps });
    expect(tree).toMatchSnapshot();
  });
});
