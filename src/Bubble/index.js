/* @flow */

import React, { PureComponent } from 'react';

import type {
  BubbleProps,
} from '../types';

type DefaultProps = BubbleProps;
type Props = BubbleProps;

// $FlowBug - https://github.com/facebook/flow/issues/1594
export default class Bubble extends PureComponent<DefaultProps, Props> {
  static props: BubbleProps;

  static defaultProps = {
    children: undefined,
  };

  render() {
    return (
      <div className="chat-msg-bubble-outer_wrap">
        <div className="chat-msg-bubble-wrap">
          <div className="chat-msg-bubble chat-msg-bubble--loaded">
            <div className="chat-msg-bubble-inner">
              <div className="chat-msg-bubble-bg"></div>
              <div className="chat-msg-text">{this.props.children}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
