import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Bubble from './index';

import { shortText, longText } from './index.helper';

storiesOf('Bubble', module)
  .add('with short text', () => (
    <Bubble>{shortText}</Bubble>
  ))
  .add('with long text', () => (
    <Bubble>{longText}</Bubble>
  ));
