import React from 'react';

export default () => (
  <div className="chat-loading">
    <div className="chat-loading-dot"></div>
    <div className="chat-loading-dot"></div>
    <div className="chat-loading-dot"></div>
  </div>
);
