import React from 'react';
import { storiesOf } from '@kadira/storybook';
import MarchingAnts from './index';

storiesOf('MarchingAnts', module)
  // TODO: MM: change to
  .add('Marching ants standalone', () => (
    <div className="chat-msg-bubble--loading" style={{ margin: 20 }}>
      <MarchingAnts />
    </div>
  ))
  .add('Marching ants in bubble', () => (
    <div className="chat-msg-bubble-outer_wrap">
      <div className="chat-msg-bubble-wrap">
        <div className="chat-msg-bubble chat-msg-bubble--loading">
          <div className="chat-msg-bubble-inner">
            <div className="chat-msg-bubble-bg"></div>
            <MarchingAnts />
          </div>
        </div>
      </div>
    </div>
  ));
