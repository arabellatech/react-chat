/* @flow */

import React from 'react';
import renderer from 'react-test-renderer';

import MarchingAnts from './index';

describe('<MarchingAnts />', () => {
  const renderWithProps = () => renderer.create(
    <MarchingAnts />
  ).toJSON();

  it('should render marching ants', () => {
    const tree = renderWithProps();
    expect(tree).toMatchSnapshot();
  });
});
