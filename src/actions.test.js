/* @flow */

import _mapKeys from 'lodash/mapKeys';

import * as messageActions from './Message/actions';
import * as schemaActions from './Schema/actions';
import * as threadActions from './Thread/actions';
import * as variablesPoolActions from './VariablesPool/actions';
import * as allActions from './actions';

describe('export: actions', () => {
  it('should export all message actions', () => {
    expect(Object.keys(messageActions)).toMatchSnapshot();

    _mapKeys(
      messageActions,
      (selector, selectorName) => expect(allActions[selectorName]).toBe(messageActions[selectorName])
    );
  });

  it('should export all thread actions', () => {
    expect(Object.keys(threadActions)).toMatchSnapshot();

    _mapKeys(
      threadActions,
      (selector, selectorName) => expect(allActions[selectorName]).toBe(threadActions[selectorName])
    );
  });

  it('should export all schema actions', () => {
    expect(Object.keys(schemaActions)).toMatchSnapshot();

    _mapKeys(
      schemaActions,
      (selector, selectorName) => expect(allActions[selectorName]).toBe(schemaActions[selectorName])
    );
  });

  it('should export all variables pool actions', () => {
    expect(Object.keys(variablesPoolActions)).toMatchSnapshot();

    _mapKeys(
      variablesPoolActions,
      (selector, selectorName) => expect(allActions[selectorName]).toBe(variablesPoolActions[selectorName])
    );
  });
});
