/* @flow */

import { Map } from 'immutable';

export type AvatarProps = {
  alt?: string,
  src?: string,
}

// TODO
// {
//  questions: Array<ChatQuestionType>,
//  currentQuestion: ChatQuestionType | null,
//  confirmedQuestions: Array<ChatQuestionType> | null,
// }
export type ChatStateType = Map<string, any>;

export type ChatSchemaQueueFailedReasonType = 'RETRIES_LIMIT_REACHED' | 'UNAUTHORIZED';

/* chat message */

// Possible statuses to determine the current state of sending the message to API.
const chatMessageStatuses = {
  loading: 'loading',
  success: 'success',
  error: 'error',
};
type ChatMessageStatusType = $Keys<typeof chatMessageStatuses>;

// Shape of the single message
export type ChatMessageType = $Exact<{
  id?: number,
  feId?: string,
  threadId: number,
  questionId?: ?number,
  created?: string,
  editable?: boolean,
  status?: ChatMessageStatusType,
  type: string,
  text?: string,
  delay?: number,
  attachments?: Array<ChatAttachmentType>,
  meta?: Object,
  // answers: Array<string>,
}>;

// Shape of the message bubble group
// Bubbles are grouped by the sender to handle single avatar for each group
// of messages sent by the same user
// showOnRight is to determine the side of appearance
//  if true show bubbles group on the right-hand side
//  if false show bubbles group on the left-hand side with avatar
export type ChatMessagesListGroupType = $Exact<{
  showOnRight: boolean,
  sender: ?ChatSenderType,
  items: Array<ChatMessageType>,
  type: string,
}>;

// Shape of the list of messages grouped by the sender
export type ChatGroupedMessagesListType = Array<ChatMessagesListGroupType>

// Shape of the sender
// TODO: MM: possibly should be moved to more generic file.
export type ChatSenderType = $Exact<{
  id: number,
  photo: string,
  firstName: string,
  lastName: string,
}>;

// TODO: MM: create shape
// TODO: MM: possibly should be moved to more generic file.
export type ChatAttachmentType = $Exact<{
  // TBD
}>;

/* chat question */

export type ChatQuestionType = {
  id: number,
  qid: number,
  // optional, when there are no answers, we will just display next bubble
  // end flow if next_qid = -1
  nextQid: ?number,
  text: string,
  answers: Array<ChatAnswerType>,
  type?: ChatQuestionTypeType,
  meta?: ChatQuestionMetaType,
}

export type ChatQuestionTypeType = 'choice' | 'multichoice' | 'text' | 'message' | 'control' | 'fork';

export type ChatQuestionMetaType = {
  component?: string, // name of component we use to display question
  type?: string,
}

export type ChatQuestionReplyType = {
  threadId: number,
  question: number,
  answers?: Array<number>,
  text?: string,
  meta?: Object,
  attachments?: Array<Object>,
}

export type ChatAnswerType = {
  id: number,
  nextQid: number, // if -1, end flow
  text?: string,
  value?: string | number | boolean, // TODO remove, we will use id as value
  isSelected?: boolean,
  extra?: {}, // TODO replace with meta
  meta?: ChatAnswerMetaType,
}

export type ChatAnswerMetaType = {
  // TBD
}

/* props */

export type ChatProps = {
  messages: Map<*, *>,
  threadId: number,
  loadMessages: (threadId: number) => void,
  currentQuestion: ChatStateType,
  showNextQuestion: () => void,
  // TODO: type returned by renderMessageForm should be
  // react component or stateless function
  renderEmptyList: () => ?React$Element<*>,
  renderMessageForm: (threadId: number) => mixed,
  resetMessages: () => mixed,
  customVariables: Object,
  animations: ChatStateType,
  shouldShowNextQuestion: boolean,
  showAnimations: boolean,
  incomingSender: ?ChatSenderType,
  outgoingSender: ?ChatSenderType,
  renderMessagesGroup?: () => mixed,
  onEditClick?: (threadId: number, feId?: string) => void,
  rollbackThread: (threadId: number, feId?: string) => void,
  messageDateFormat?: string,
  messageTimeFormat?: string,
  showAvatarsOnRight?: boolean,
  renderAvatar?: () => mixed,
  renderTimestamp?: () => mixed,
}

export type ChatMessagesGroupProps = {
  messagesGroup: ChatMessagesListGroupType,
  showAnimations: boolean,
  showAvatarsOnRight?: boolean,
  incomingSender: ?ChatSenderType,
  outgoingSender: ?ChatSenderType,
  customVariables: Object,
  onEditClick?: (feId?: string) => void,
  messageDateFormat: ?string,
  messageTimeFormat: ?string,
  renderAvatar?: () => mixed,
}

export type BubbleProps = {
  children: Element,
}

export type ChatMessageFormProps = {
  threadId: number,
  sendMessage: (message: ChatMessageType) => void,
  sendMessageAndCloseThread: (message: ChatMessageType) => void,
  setAnswer: (message: ChatMessageType) => void,
  component: any,
}

export type ChatQuestionFormProps = {
  threadId: number,
  type: string,
  // TODO: PP: add type for component
  component: any,
  getType?: (ChatStateType) => string,
  currentQuestion: ChatStateType,
  currentQuestionReply: ChatStateType,
  customVariables: ChatStateType,
  selectAnswer: (answer: ChatAnswerType) => void,
  unselectAnswer: (answer: ChatAnswerType) => void,
  sendQuestionReply: (reply: ChatQuestionReplyType) => void,
  setCustomVariables: (variables: Object) => ChatSetCustomVariablesActionType,
  showNextQuestion: () => void,
  setAnswer: () => void,
  animations: ChatStateType,
  animationStart: () => ChatQuestionAnimationStartActionType,
  animationEnd: () => ChatQuestionAnimationEndActionType,
}

/* Backend data format */
/* this is exactly what we get from API response */
export type ChatMessageSerializerType = $Exact<{
  relatedQuestion: string,
  updated: string,
  meta: Object,
  created: string,
  isRead: boolean,
  hasAttachments: boolean,
  thread: number,
  sender: number,
  id: number,
  text: string,
  type: string,
  senderData: ChatAccountSerializerType,
  attachmentsData: Array<any>,
  isOwn: boolean,
}>;

export type ChatMessageListSerializerType = $Exact<{
  next: ?string,
  prev: ?string,
  results: Array<ChatMessageSerializerType>,
}>;

export type ChatAccountSerializerType = $Exact<{
  id: number,
  idString: string,
  firstName: string,
  lastName: string,
  email: string,
  photo: string,
  role: string,
}>;

/* actions */
/* thread */

export type ChatSetThreadIdActionType = {
  type: 'app/Chat/SET_THREAD_ID',
  id: number,
};

export type ChatThreadOpenActionType = {
  type: 'app/Chat/THREAD_OPEN',
  threadId: number,
}

export type ChatThreadOpenSuccessActionType = {
  type: 'app/Chat/THREAD_OPEN_SUCCESS',
  payload: Object,
}

export type ChatThreadOpenFailedActionType = {
  type: 'app/Chat/THREAD_OPEN_FAILED',
  payload: Object,
}

export type ChatThreadCloseActionType = {
  type: 'app/Chat/THREAD_CLOSE',
  threadId: number,
}

export type ChatThreadCloseSuccessActionType = {
  type: 'app/Chat/THREAD_CLOSE_SUCCESS',
  payload: Object,
}

export type ChatThreadCloseFailedActionType = {
  type: 'app/Chat/THREAD_CLOSE_FAILED',
  payload: Object,
}

export type ChatThreadWaitForResponseActionType = {
  type: 'app/Chat/THREAD_WAIT_FOR_RESPONSE',
  threadId: number,
}

export type ChatThreadWaitForResponseSuccessActionType = {
  type: 'app/Chat/THREAD_WAIT_FOR_RESPONSE_SUCCESS',
  payload: Object,
}

export type ChatThreadWaitForResponseFailedActionType = {
  type: 'app/Chat/THREAD_WAIT_FOR_RESPONSE_FAILED',
  payload: Object,
}

/* schema */

export type ChatLoadSchemaActionType = {
  type: 'app/Chat/LOAD_SCHEMA',
  id: number,
};

export type ChatLoadSchemaSuccessActionType = {
  type: 'app/Chat/LOAD_SCHEMA_SUCCESS',
  payload: Object,
};

export type ChatLoadSchemaFailedActionType = {
  type: 'app/Chat/LOAD_SCHEMA_FAILED',
  payload: Object,
};

/* onboarding schema */

export type ChatLoadOnboardingSchemaActionType = {
  type: 'app/Chat/LOAD_ONBOARDING_SCHEMA',
};

export type ChatLoadOnboardingSchemaSuccessActionType = {
  type: 'app/Chat/LOAD_ONBOARDING_SCHEMA_SUCCESS',
  payload: Object,
};

export type ChatLoadOnboardingSchemaFailedActionType = {
  type: 'app/Chat/LOAD_ONBOARDING_SCHEMA_FAILED',
  payload: Object,
};

/* answers */

// TODO pass only answer id
export type ChatSelectAnswerActionType = {
  type: 'app/Chat/SELECT_ANSWER',
  answer: ChatAnswerType,
};

// TODO pass only answer id
export type ChatUnselectAnswerActionType = {
  type: 'app/Chat/UNSELECT_ANSWER',
  answer: ChatAnswerType,
};

// TODO pass only answer id
export type ChatSetAnswerActionType = {
  type: 'app/Chat/SET_ANSWER',
  answer: ChatAnswerType,
};

/* questions */

export type ChatSendQuestionReplyActionType = {
  type: 'app/Chat/SEND_QUESTION_REPLY',
  reply: ChatQuestionReplyType,
};

export type ChatSendQuestionReplySuccessActionType = {
  type: 'app/Chat/SEND_QUESTION_REPLY_SUCCESS',
  payload: Object,
};

export type ChatSendQuestionReplyFailedActionType = {
  type: 'app/Chat/SEND_QUESTION_REPLY_FAILED';
  payload: Object,
};

export type ChatChangeQuestionActionType = {
  type: 'app/Chat/CHANGE_QUESTION',
  question: ChatQuestionType;
};

export type ChatNextQuestionActionType = {
  type: 'app/Chat/NEXT_QUESTION',
};

export type ChatQuestionAnimationStartActionType = {
  type: 'app/Chat/QUESTION_ANIMATION_START',
  animation: string,
};

export type ChatQuestionAnimationEndActionType = {
  type: 'app/Chat/QUESTION_ANIMATION_END',
  animation: string,
};

export type ChatThreadRollbackActionType = $Exact<{
  type: 'app/Chat/THREAD_ROLLBACK',
  threadId: number,
  feId?: string,
  questionId?: number,
}>;

export type ChatThreadRollbackSuccessActionType = $Exact<{
  type: 'app/Chat/THREAD_ROLLBACK_SUCCESS',
  payload: Object
}>;

export type ChatThreadRollbackFailedActionType = $Exact<{
  type: 'app/Chat/THREAD_ROLLBACK_FAILED',
  payload: Object
}>;

/* message */

export type ChatLoadMessagesActionType = $Exact<{
  type: 'app/Chat/LOAD_MESSAGES',
  threadId: number,
}>;

export type ChatLoadMessagesSuccessActionType = $Exact<{
  type: 'app/Chat/LOAD_MESSAGES_SUCCESS',
  payload: ChatMessageListSerializerType,
}>;

export type ChatLoadMessagesFailedActionType = $Exact<{
  type: 'app/Chat/LOAD_MESSAGES_FAILED',
  payload: Object,
}>;

export type ChatSendMessageAndCloseThreadActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE_AND_CLOSE_THREAD',
  payload: ChatMessageType,
}>;

export type ChatSendMessageAndOpenThreadActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE_AND_OPEN_THREAD',
  payload: ChatMessageType,
}>;

export type ChatSendMessageAndWaitForResponseActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE_AND_WAIT_FOR_RESPONSE',
  payload: ChatMessageType,
}>;

export type ChatSendMessageActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE',
  payload: ChatMessageType,
}>;

export type ChatSendMessageSuccessActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE_SUCCESS',
  payload: ChatMessageType,
}>;

export type ChatSendMessageFailedActionType = $Exact<{
  type: 'app/Chat/SEND_MESSAGE_FAILED',
  payload: Object,
}>;

export type ChatBubbleAnimationStartActionType = $Exact<{
  type: 'app/Chat/BUBBLE_ANIMATION_START',
}>;

export type ChatBubbleAnimationEndActionType = $Exact<{
  type: 'app/Chat/BUBBLE_ANIMATION_END',
}>;

export type ChatResetMessagesActionType = $Exact<{
  type: 'app/Chat/RESET_MESSAGES',
}>;

/* schema queue */

export type ChatSchemaQueueStartedActionType = {
  type: 'app/Chat/SCHEMA_QUEUE_STARTED',
}

export type ChatSchemaQueueEndedActionType = {
  type: 'app/Chat/SCHEMA_QUEUE_ENDED',
}

export type ChatSchemaQueueFailedActionType = {
  type: 'app/Chat/SCHEMA_QUEUE_FAILED',
  reason: ChatSchemaQueueFailedReasonType,
  payload?: Object,
}

/* variables pool */

export type ChatSetVariablesPoolActionType = {
  type: 'app/Chat/SET_VARIABLES_POOL',
  variablesPool: Object,
};

export type ChatSetCustomVariablesActionType = {
  type: 'app/Chat/SET_CUSTOM_VARIABLES',
  variables: Object,
};

export type ChatActionType =
  ChatSetThreadIdActionType |
  ChatThreadOpenActionType |
  ChatThreadOpenSuccessActionType |
  ChatThreadOpenFailedActionType |
  ChatThreadCloseActionType |
  ChatThreadCloseSuccessActionType |
  ChatThreadCloseFailedActionType |
  ChatThreadRollbackActionType |
  ChatThreadRollbackSuccessActionType |
  ChatThreadRollbackFailedActionType |

  ChatLoadSchemaActionType |
  ChatLoadOnboardingSchemaActionType |
  ChatLoadOnboardingSchemaSuccessActionType |
  ChatLoadOnboardingSchemaFailedActionType |
  ChatLoadSchemaSuccessActionType |
  ChatLoadSchemaFailedActionType |
  ChatSelectAnswerActionType |
  ChatUnselectAnswerActionType |
  ChatSetAnswerActionType |
  ChatSendQuestionReplyActionType |
  ChatSendQuestionReplySuccessActionType |
  ChatSendQuestionReplyFailedActionType |
  ChatChangeQuestionActionType |
  ChatNextQuestionActionType |
  ChatQuestionAnimationStartActionType |
  ChatQuestionAnimationEndActionType |

  ChatLoadMessagesActionType |
  ChatLoadMessagesSuccessActionType |
  ChatLoadMessagesFailedActionType |
  ChatSendMessageAndCloseThreadActionType |
  ChatSendMessageActionType |
  ChatSendMessageSuccessActionType |
  ChatSendMessageFailedActionType |
  ChatBubbleAnimationStartActionType |
  ChatBubbleAnimationEndActionType |
  ChatResetMessagesActionType |

  ChatSchemaQueueStartedActionType |
  ChatSchemaQueueEndedActionType |
  ChatSchemaQueueFailedActionType |

  ChatSetVariablesPoolActionType |
  ChatSetCustomVariablesActionType;
