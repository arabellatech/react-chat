/* @flow */

import messageSagas from './Message/sagas';
import schemaSagas from './Schema/sagas';
import threadSagas from './Thread/sagas';
import allSagas from './sagas';

describe('export: sagas', () => {
  it('should export all message sagas', () => {
    messageSagas.map(
      (saga) => expect(allSagas.indexOf(saga)).not.toBe(-1)
    );
  });

  it('should export all schema sagas', () => {
    schemaSagas.map(
      (saga) => expect(allSagas.indexOf(saga)).not.toBe(-1)
    );
  });

  it('should export all thread sagas', () => {
    threadSagas.map(
      (saga) => expect(allSagas.indexOf(saga)).not.toBe(-1)
    );
  });
});
