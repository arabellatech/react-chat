// TODO: use flow

import {
  call,
  put,
  take,
  takeEvery,
} from 'redux-saga/effects';
import _get from 'lodash/get';
import {
  chatThreadCloseAction,
  chatThreadCloseSuccessAction,
  chatThreadCloseFailedAction,
  chatThreadOpenAction,
  chatThreadOpenSuccessAction,
  chatThreadOpenFailedAction,
  chatThreadWaitForResponseAction,
  chatThreadWaitForResponseSuccessAction,
  chatThreadWaitForResponseFailedAction,
} from './actions';
import {
  CHAT_THREAD_OPEN_ACTION,
  CHAT_THREAD_CLOSE_ACTION,
  CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION,
} from './constants';
import {
  CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,
  CHAT_SEND_MESSAGE_SUCCESS_ACTION,
  CHAT_SEND_MESSAGE_FAILED_ACTION,
} from '../Message/constants';
import {
  threadOpen,
  threadClose,
  threadWaitForResponse,
} from '../api';

export function* watchChatThreadOpenSaga() {
  yield takeEvery(CHAT_THREAD_OPEN_ACTION, chatThreadOpenSaga);
}

export function* chatThreadOpenSaga(action) {
  try {
    const response = yield call(threadOpen, action.threadId);

    yield put(chatThreadOpenSuccessAction(response.data));
  } catch (error) {
    yield put(chatThreadOpenFailedAction(error));
  }
}

export function* watchChatThreadCloseSaga() {
  yield takeEvery(CHAT_THREAD_CLOSE_ACTION, chatThreadCloseSaga);
}

export function* chatThreadCloseSaga(action) {
  try {
    const response = yield call(threadClose, action.threadId);

    yield put(chatThreadCloseSuccessAction(response.data));
  } catch (error) {
    yield put(chatThreadCloseFailedAction(error));
  }
}

export function* watchChatThreadWaitForResponseSaga() {
  yield takeEvery(CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION, chatThreadWaitForResponseSaga);
}

export function* chatThreadWaitForResponseSaga(action) {
  try {
    const response = yield call(threadWaitForResponse, action.threadId);

    yield put(chatThreadWaitForResponseSuccessAction(response.data));
  } catch (error) {
    yield put(chatThreadWaitForResponseFailedAction(error));
  }
}

export function* watchChatSendMessageAndCloseThreadSaga() {
  yield takeEvery(CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION, chatThreadCloseAfterMessageSendSaga);
}

export function* chatThreadCloseAfterMessageSendSaga() {
  const action = yield take([
    CHAT_SEND_MESSAGE_SUCCESS_ACTION,
    CHAT_SEND_MESSAGE_FAILED_ACTION,
  ]);
  const threadId = _get(action, 'payload.thread');

  if (threadId && action.type === CHAT_SEND_MESSAGE_SUCCESS_ACTION) {
    yield put(chatThreadCloseAction(threadId));
  }
}

export function* watchChatSendMessageAndOpenThreadSaga() {
  yield takeEvery(CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION, chatThreadOpenAfterMessageSendSaga);
}

export function* chatThreadOpenAfterMessageSendSaga() {
  const action = yield take([
    CHAT_SEND_MESSAGE_SUCCESS_ACTION,
    CHAT_SEND_MESSAGE_FAILED_ACTION,
  ]);
  const threadId = _get(action, 'payload.thread');

  if (threadId && action.type === CHAT_SEND_MESSAGE_SUCCESS_ACTION) {
    yield put(chatThreadOpenAction(threadId));
  }
}

export function* watchChatSendMessageAndWaitForResponseSaga() {
  yield takeEvery(CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION, chatThreadWaitForResponseAfterMessageSendSaga);
}

export function* chatThreadWaitForResponseAfterMessageSendSaga() {
  const action = yield take([
    CHAT_SEND_MESSAGE_SUCCESS_ACTION,
    CHAT_SEND_MESSAGE_FAILED_ACTION,
  ]);
  const threadId = _get(action, 'payload.thread');

  if (threadId && action.type === CHAT_SEND_MESSAGE_SUCCESS_ACTION) {
    yield put(chatThreadWaitForResponseAction(threadId));
  }
}

// All sagas to be loaded
export default [
  watchChatThreadOpenSaga,
  watchChatThreadCloseSaga,
  watchChatThreadWaitForResponseSaga,
  watchChatSendMessageAndCloseThreadSaga,
  watchChatSendMessageAndOpenThreadSaga,
  watchChatSendMessageAndWaitForResponseSaga,
];
