/* @flow */

import {
  CHAT_THREAD_OPEN_ACTION,
  CHAT_THREAD_OPEN_SUCCESS_ACTION,
  CHAT_THREAD_OPEN_FAILED_ACTION,

  CHAT_THREAD_CLOSE_ACTION,
  CHAT_THREAD_CLOSE_SUCCESS_ACTION,
  CHAT_THREAD_CLOSE_FAILED_ACTION,

  CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION,
  CHAT_THREAD_WAIT_FOR_RESPONSE_SUCCESS_ACTION,
  CHAT_THREAD_WAIT_FOR_RESPONSE_FAILED_ACTION,
} from './constants';

import type {
  ChatThreadOpenActionType,
  ChatThreadOpenSuccessActionType,
  ChatThreadOpenFailedActionType,

  ChatThreadCloseActionType,
  ChatThreadCloseSuccessActionType,
  ChatThreadCloseFailedActionType,
} from '../types';

/* thread */

export const chatThreadOpenAction = (threadId: number): ChatThreadOpenActionType => ({
  type: CHAT_THREAD_OPEN_ACTION,
  threadId,
});

export const chatThreadOpenSuccessAction = (payload: Object): ChatThreadOpenSuccessActionType => ({
  type: CHAT_THREAD_OPEN_SUCCESS_ACTION,
  payload,
});

export const chatThreadOpenFailedAction = (payload: Object): ChatThreadOpenFailedActionType => ({
  type: CHAT_THREAD_OPEN_FAILED_ACTION,
  payload,
});

export const chatThreadCloseAction = (threadId: number): ChatThreadCloseActionType => ({
  type: CHAT_THREAD_CLOSE_ACTION,
  threadId,
});

export const chatThreadCloseSuccessAction = (payload: Object): ChatThreadCloseSuccessActionType => ({
  type: CHAT_THREAD_CLOSE_SUCCESS_ACTION,
  payload,
});

export const chatThreadCloseFailedAction = (payload: Object): ChatThreadCloseFailedActionType => ({
  type: CHAT_THREAD_CLOSE_FAILED_ACTION,
  payload,
});

export const chatThreadWaitForResponseAction = (threadId: number): ChatThreadWaitForResponseActionType => ({
  type: CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION,
  threadId,
});

export const chatThreadWaitForResponseSuccessAction = (payload: Object): ChatThreadWaitForResponseSuccessActionType => ({
  type: CHAT_THREAD_WAIT_FOR_RESPONSE_SUCCESS_ACTION,
  payload,
});

export const chatThreadWaitForResponseFailedAction = (payload: Object): ChatThreadWaitForResponseFailedActionType => ({
  type: CHAT_THREAD_WAIT_FOR_RESPONSE_FAILED_ACTION,
  payload,
});
