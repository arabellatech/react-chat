/* @flow */
import React from 'react';

import type {
  AvatarProps,
} from '../types';

const Avatar = (props: AvatarProps) => {
  const avatarStyle = {
    backgroundImage: `url('${props.src}')`,
  };

  const image = (
    <div className="chat-msg-avatar-thumb" style={avatarStyle}></div>
  );
  const alt = (
    <div className="chat-msg-avatar-letter">
      { props.alt }
    </div>
  );

  const avatar = props.src ? image : alt;

  return (
    <div className="chat-msg-user">
      <div className="chat-msg-avatar">
        { avatar }
      </div>
    </div>
  );
};

Avatar.defaultProps = {
  alt: '!',
  src: undefined,
};

export default Avatar;
