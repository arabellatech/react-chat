/* @flow */

import React from 'react';
import renderer from 'react-test-renderer';

import Avatar from './index';

import type {
  AvatarProps,
} from '../types';


describe('<Avatar />', () => {
  const avatarUrl = 'https://s3.amazonaws.com/uifaces/faces/twitter/rem/128.jpg';
  const alternativeString = 'M';

  const renderWithProps = (props: AvatarProps) => renderer.create(
    <Avatar {...props} />
  ).toJSON();

  it('should render avatar with as background image', () => {
    const testProps = {
      src: avatarUrl,
    };

    const tree = renderWithProps({ ...testProps });
    expect(tree).toMatchSnapshot();
  });

  it('should render alternative string only', () => {
    const testProps = {
      alt: alternativeString,
    };

    const tree = renderWithProps({ ...testProps });
    expect(tree).toMatchSnapshot();
  });

  it('should render default alternative string', () => {
    const testProps = {};

    const tree = renderWithProps({ ...testProps });
    expect(tree).toMatchSnapshot();
  });
});
