import React from 'react';
import { storiesOf } from '@kadira/storybook';
import Avatar from './index';

const avatarUrl = 'https://s3.amazonaws.com/uifaces/faces/twitter/rem/128.jpg';
const alternativeString = 'M';

storiesOf('Avatar', module)
  .add('with alternative string', () => (
    <Avatar alt={alternativeString} />
  ))
  .add('with image', () => (
    <Avatar src={avatarUrl} alt={alternativeString} />
  ))
  .add('without parameters', () => (
    <Avatar />
  ));
