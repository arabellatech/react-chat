// TODO: use flow

import _get from 'lodash/get';
import {
  buffers,
  delay,
} from 'redux-saga';
import {
  actionChannel,
  call,
  flush,
  put,
  select,
  take,
  takeEvery,
} from 'redux-saga/effects';
import {
  chatLoadOnboardingSchemaFailedAction,
  chatLoadOnboardingSchemaSuccessAction,
  chatLoadSchemaFailedAction,
  chatLoadSchemaSuccessAction,
  chatSchemaPutRequestToQueue,
  chatSchemaQueueEndedAction,
  chatSchemaQueueFailedAction,
  chatSchemaQueueSetRequestsProgressAction,
  chatSchemaQueueStartedAction,
  chatSendQuestionReplyFailedAction,
  chatSendQuestionReplySuccessAction,
  chatThreadRollbackFailedAction,
  chatThreadRollbackSuccessAction,
} from './actions';
import {
  selectChatSchemaQueueStatusIsEnded,
  selectChatSchemaQueueStatusIsFailed,
} from './selectors';
import {
  CHAT_LOAD_ONBOARDING_SCHEMA_ACTION,
  CHAT_LOAD_SCHEMA_ACTION,
  CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION,
  CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION,
  CHAT_SEND_QUESTION_REPLY_ACTION,
  CHAT_THREAD_ROLLBACK_ACTION,
  QUEUE_FAILURE_BAD_REQUEST,
  QUEUE_FAILURE_RETRIES_LIMIT_REACHED,
  QUEUE_RETRIES_LIMIT,
  QUEUE_RETRY_DELAY,
  QUEUE_SUCCESS_DELAY,
} from './constants';
import {
  getChatSchema,
  getChatOnboardingSchema,
  sendQuestionReply,
  threadRollback,
} from '../api';
import {
  MIN_PROGRESS_DIFF,
  createRequestChannel,
  addPendingRequest,
  updateRequestProgress,
  clearFinishedRequests,
  getTotalRequestsProgress,
} from './requestsProgess';

export function* watchChatLoadSchemaSaga() {
  yield takeEvery(CHAT_LOAD_SCHEMA_ACTION, chatLoadSchemaSaga);
}

export function* chatLoadSchemaSaga(action/* : ChatLoadSchemaActionType */) {
  try {
    const response = yield call(getChatSchema, action.id);

    yield put(chatLoadSchemaSuccessAction(response.data));
  } catch (error) {
    yield put(chatLoadSchemaFailedAction(error));
  }
}

export function* watchChatLoadOnboardingSchemaSaga() {
  yield takeEvery(CHAT_LOAD_ONBOARDING_SCHEMA_ACTION, chatLoadOnboardingSchemaSaga);
}

export function* chatLoadOnboardingSchemaSaga() {
  try {
    const response = yield call(getChatOnboardingSchema);

    yield put(chatLoadOnboardingSchemaSuccessAction(response.data));
  } catch (error) {
    yield put(chatLoadOnboardingSchemaFailedAction(error));
  }
}

export function* watchSendQuestionReplySaga() {
  yield [
    takeEvery(CHAT_SEND_QUESTION_REPLY_ACTION, onSendQuestionReplySaga),
    takeEvery(CHAT_THREAD_ROLLBACK_ACTION, onSendQuestionReplySaga),
  ];
}

export function* onSendQuestionReplySaga(action) {
  yield put(chatSchemaPutRequestToQueue(action));
  yield call(addPendingRequest, action.requestUid);
}

export function* watchSchemaQueueSaga() {
  const schemaRepliesBuffer = buffers.expanding(30);
  const schemaRepliesChannel = yield actionChannel(CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION, schemaRepliesBuffer);

  while (true) {
    const action = yield take(schemaRepliesChannel);
    const {
      requestAction,
      requestAction: {
        type,
      },
    } = action;

    yield put(chatSchemaQueueStartedAction());

    if (type === CHAT_SEND_QUESTION_REPLY_ACTION) {
      yield call(chatSendQuestionReplySaga, requestAction);
    } else if (type === CHAT_THREAD_ROLLBACK_ACTION) {
      yield call(chatThreadRollbackSaga, requestAction);
    }

    const isEndedSchemaQueueStatus = yield select(selectChatSchemaQueueStatusIsEnded);
    const isFailedSchemaQueueStatus = yield select(selectChatSchemaQueueStatusIsFailed);

    if (!isEndedSchemaQueueStatus && schemaRepliesBuffer.isEmpty()) {
      yield put(chatSchemaQueueEndedAction());
    } else if (isFailedSchemaQueueStatus) {
      yield flush(schemaRepliesChannel);
    }

    yield call(delay, QUEUE_SUCCESS_DELAY);
  }
}

export function* chatSendQuestionReplySaga(action) {
  return yield retryUntilLimitReached({
    requestUid: action.requestUid,
    apiCallFn: sendQuestionReply,
    apiCallFnArguments: [action.reply],
    successAction: chatSendQuestionReplySuccessAction,
    failedAction: chatSendQuestionReplyFailedAction,
  });
}

export function* chatThreadRollbackSaga(action) {
  return yield retryUntilLimitReached({
    requestUid: action.requestUid,
    apiCallFn: threadRollback,
    apiCallFnArguments: [action.threadId, action.questionId],
    successAction: chatThreadRollbackSuccessAction,
    failedAction: chatThreadRollbackFailedAction,
  });
}

export function* watchChatSchemaQueueClearFinishedRequestsSaga() {
  yield takeEvery(CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION, chatSchemaQueueClearFinishedRequestsSaga);
}

let lastTotalRequestsProgress = 0;

export function* chatSchemaQueueClearFinishedRequestsSaga() {
  yield call(clearFinishedRequests);

  const currentTotalRequestsProgress = yield call(getTotalRequestsProgress);
  lastTotalRequestsProgress = currentTotalRequestsProgress;

  yield put(chatSchemaQueueSetRequestsProgressAction(currentTotalRequestsProgress));
}

export function* retryUntilLimitReached({
  apiCallFn,
  apiCallFnArguments,
  failedAction,
  requestUid,
  successAction,
}) {
  for (let retries = 0; retries < QUEUE_RETRIES_LIMIT; retries += 1) {
    const requestChannel = yield call(createRequestChannel, apiCallFn, apiCallFnArguments);

    while (true) {
      const {
        loaded = 0,
        total = 0,
        error,
        response,
      } = yield take(requestChannel);

      if (response) {
        return yield put(successAction(response.data));
      }

      if (error) {
        const status = _get(error, 'response.status');

        if (!status) {
          // if there is no status, then its a network issue,
          // so don't count this as retry
          retries -= 1;
        } else if (status >= 400 && status <= 500) {
          return yield put(chatSchemaQueueFailedAction(QUEUE_FAILURE_BAD_REQUEST, error));
        }

        yield put(failedAction(error));

        break;
      }

      yield call(updateRequestProgress, requestUid, loaded, total);
      const currentTotalRequestsProgress = yield call(getTotalRequestsProgress);
      const totalRequestsDiff = Math.abs(currentTotalRequestsProgress - lastTotalRequestsProgress);

      if (totalRequestsDiff >= MIN_PROGRESS_DIFF) {
        lastTotalRequestsProgress = currentTotalRequestsProgress;
        yield put(chatSchemaQueueSetRequestsProgressAction(currentTotalRequestsProgress));
      }
    }

    yield call(delay, QUEUE_RETRY_DELAY);
  }

  return yield put(chatSchemaQueueFailedAction(QUEUE_FAILURE_RETRIES_LIMIT_REACHED));
}

export default [
  watchChatLoadSchemaSaga,
  watchChatLoadOnboardingSchemaSaga,
  watchSchemaQueueSaga,
  watchSendQuestionReplySaga,
  watchChatSchemaQueueClearFinishedRequestsSaga,
];
