import React from 'react';
import { connect } from 'react-redux';
import {
  selectChatSchemaQueueStatusIsEnded,
  selectChatSchemaQueueStatusIsFailed,
} from './selectors';

const withSchemaQueueStatus = (WrappedComponent) => {
  const mapStateToProps = (state) => ({
    isSchemaQueueEnded: selectChatSchemaQueueStatusIsEnded(state),
    isSchemaQueueFailed: selectChatSchemaQueueStatusIsFailed(state),
  });

  const SchemaQueueStatusHOC = (props) => (
    <WrappedComponent {...props} />
  );

  return connect(mapStateToProps)(SchemaQueueStatusHOC);
};

export default withSchemaQueueStatus;
