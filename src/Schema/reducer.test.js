/* @flow */

import {
  chatSchemaQueueStartedAction,
  chatSchemaQueueEndedAction,
  chatSchemaQueueFailedAction,
} from './actions';
import {
  QUEUE_FAILURE_RETRIES_LIMIT_REACHED,
  QUEUE_FAILURE_BAD_REQUEST,
} from './constants';
import {
  CHAT_MESSAGE_TYPE_TEXT,
} from '../Message/constants';
import type {
  ChatQuestionType,
  ChatAnswerType,
  ChatSendQuestionReplyActionType,
} from '../types';
import chatReducer from '../reducer';

describe('chatReducer', () => {
  let initialState;
  const getState = (state?: Object = {}) => initialState.merge(state);


  beforeEach(() => {
    // $FlowExpectedError
    Date.now = jest.fn(() => 1487076708000); // 2017-02-14T13:51:48+01:00

    initialState = chatReducer(undefined, {});
  });


  describe('animations', () => {
    describe('CHAT_QUESITON_ANIMATION_START', () => {
      it('should add question:$ANIMATION-started to animations', () => {
        const state = getState();
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/QUESTION_ANIMATION_START',
            animation: '$ANIMATION',
          }
        );
        const expected = {
          question: '$ANIMATION-started',
        };

        expect(
          received.get('animations')
            .toJS()
        ).toEqual(expected);
      });
    });

    describe('CHAT_QUESITON_ANIMATION_END', () => {
      it('should add question:$ANIMATION-ended to animations', () => {
        const state = getState();
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/QUESTION_ANIMATION_END',
            animation: '$ANIMATION',
          }
        );
        const expected = {
          question: '$ANIMATION-ended',
        };

        expect(
          received.get('animations')
            .toJS()
        ).toEqual(expected);
      });
    });
  });

  describe('schema', () => {
    const defaultSchema = {
      name: 'Onboarding',
      published: false,
      version: 1,
      type: 'onboarding',
      deprecated: false,
      id: 2,
      groups: [
        {
          id: 1,
          schema: 2,
          position: 1,
          name: 'M: Customer initial page',
          questions: [
            {
              schema: 2,
              range_to: null,
              nextQid: null,
              states_inited_with: [],
              required: false,
              has_attachments: false,
              qid: 10,
              choiceOpen: false,
              type: 'choice',
              group: 1,
              id: 3,
              text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
              range_from: null,
              position: 10,
              states_with: [],
              answers: [
                {
                  question: 3,
                  nextQid: 20,
                  id: 1,
                  text: 'START ONLINE VISIT',
                  position: 1,
                },
                {
                  question: 3,
                  nextQid: 30,
                  id: 2,
                  text: 'START ONLINE VISIT',
                  position: 2,
                },
              ],
              attachments: [],
            },
          ],
        },
        {
          id: 2,
          schema: 2,
          position: 2,
          name: 'Q: Not available zip code',
          questions: [
            {
              schema: 2,
              range_to: null,
              nextQid: 21,
              states_inited_with: [],
              required: false,
              has_attachments: false,
              qid: 20,
              choiceOpen: false,
              type: 'message',
              group: 2,
              id: 4,
              text: 'Thank you. Im afraid were not available in your area yet.',
              range_from: null,
              position: 20,
              states_with: [],
              answers: [],
              attachments: [],
            },
            {
              schema: 2,
              range_to: null,
              nextQid: -1,
              states_inited_with: [],
              required: false,
              has_attachments: false,
              qid: 21,
              choiceOpen: false,
              type: 'text',
              group: 2,
              id: 5,
              text: 'Well notify you as soon as we launch in {{cityname}}. Enter your e-mail address to get $50 off when we launch.',
              range_from: null,
              position: 21,
              states_with: [],
              answers: [
                {
                  question: 5,
                  nextQid: -1,
                  id: 3,
                  text: 'YOUR EMAIL ADDRESS',
                  position: 1,
                },
              ],
              attachments: [],
            },
          ],
        },
      ],
    };
    const parsedQuestions = [
      {
        nextQid: null,
        qid: 10,
        choiceOpen: false,
        type: 'choice',
        id: 3,
        text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
        answers: [
          {
            question: 3,
            nextQid: 20,
            id: 1,
            text: 'START ONLINE VISIT',
            position: 1,
          },
          {
            question: 3,
            nextQid: 30,
            id: 2,
            text: 'START ONLINE VISIT',
            position: 2,
          },
        ],
        attachments: [],
      },
      {
        nextQid: 21,
        qid: 20,
        choiceOpen: false,
        type: 'message',
        id: 4,
        text: 'Thank you. Im afraid were not available in your area yet.',
        answers: [],
        attachments: [],
      },
      {
        nextQid: -1,
        qid: 21,
        choiceOpen: false,
        type: 'text',
        id: 5,
        text: 'Well notify you as soon as we launch in {{cityname}}. Enter your e-mail address to get $50 off when we launch.',
        answers: [
          {
            question: 5,
            nextQid: -1,
            id: 3,
            text: 'YOUR EMAIL ADDRESS',
            position: 1,
          },
        ],
        attachments: [],
      },
    ];

    describe('CHAT_LOAD_SCHEMA', () => {
      it('should set loadingSchema to true', () => {
        const state = initialState;
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_SCHEMA',
            id: 1,
          }
        );

        expect(received.get('loadingSchema')).toBe(true);
      });
    });

    describe('CHAT_LOAD_SCHEMA_SUCCESS', () => {
      let received;

      beforeEach(() => {
        const state = initialState.merge({
          loadingSchema: true,
        });

        received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_SCHEMA_SUCCESS',
            payload: defaultSchema,
          }
        );
      });

      it('should set loadingSchema to true', () => {
        expect(received.get('loadingSchema')).toEqual(false);
      });

      it('should parse schema and save in questions', () => {
        expect(received.get('questions').toJS()).toEqual(parsedQuestions);
      });

      it('should copy first question to currentQuestion', () => {
        expect(received.get('currentQuestion').toJS()).toEqual(parsedQuestions[0]);
      });
    });

    describe('CHAT_LOAD_SCHEMA_FAILED', () => {
      it('should set loadingSchema to false', () => {
        const state = initialState.merge({
          loadingSchema: true,
        });
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_SCHEMA_FAILED',
            payload: {},
          }
        );

        expect(received.get('loadingSchema')).toBe(false);
      });
    });

    describe('CHAT_LOAD_ONBOARDING_SCHEMA', () => {
      it('should set loadingSchema to true', () => {
        const state = initialState;
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_ONBOARDING_SCHEMA',
          }
        );

        expect(received.get('loadingSchema')).toBe(true);
      });
    });

    describe('CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS', () => {
      let received;

      beforeEach(() => {
        const state = initialState.merge({
          loadingSchema: true,
        });

        received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_ONBOARDING_SCHEMA_SUCCESS',
            payload: defaultSchema,
          }
        );
      });

      it('should set loadingSchema to true', () => {
        expect(received.get('loadingSchema')).toEqual(false);
      });

      it('should parse schema and save in questions', () => {
        expect(received.get('questions').toJS()).toEqual(parsedQuestions);
      });

      it('should set schemaId', () => {
        expect(received.get('schemaId')).toEqual(2);
      });

      it('should copy first question to currentQuestion', () => {
        expect(received.get('currentQuestion').toJS()).toEqual(parsedQuestions[0]);
      });
    });

    describe('CHAT_LOAD_ONBOARDING_SCHEMA_FAILED', () => {
      it('should set loadingSchema to false', () => {
        const state = initialState.merge({
          loadingSchema: true,
        });
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_ONBOARDING_SCHEMA_FAILED',
            payload: {},
          }
        );

        expect(received.get('loadingSchema')).toBe(false);
      });
    });
  });

  describe('schema queue', () => {
    let state;

    beforeEach(() => {
      state = initialState;
    });

    describe('CHAT_SCHEMA_QUEUE_STATUS_STARTED', () => {
      it('should set schemaQueueStatus to { isStarted: true, isEnded: false, isFailed: false, failureReason: null }', () => {
        state = chatReducer(
          state,
          chatSchemaQueueStartedAction(),
        );
        const expected = {
          isStarted: true,
          isEnded: false,
          isFailed: false,
          failureReason: null,
        };

        expect(state.get('schemaQueueStatus').toJS()).toEqual(expected);
      });
    });

    describe('CHAT_SCHEMA_QUEUE_STATUS_ENDED', () => {
      it('should set schemaQueueStatus to { isStarted: false, isEnded: true, isFailed: false, failureReason: null }', () => {
        state = chatReducer(
          state,
          chatSchemaQueueEndedAction(),
        );
        const expected = {
          isStarted: false,
          isEnded: true,
          isFailed: false,
          failureReason: null,
        };

        expect(state.get('schemaQueueStatus').toJS()).toEqual(expected);
      });
    });

    describe('CHAT_SCHEMA_QUEUE_STATUS_FAILED', () => {
      it('should set schemaQueueStatus to { isStarted: false, isEnded: true, isFailed: false, failureReason: RETRIES_LIMIT_REACHED }', () => {
        state = chatReducer(
          state,
          chatSchemaQueueFailedAction(QUEUE_FAILURE_RETRIES_LIMIT_REACHED),
        );
        const expected = {
          isStarted: false,
          isEnded: false,
          isFailed: true,
          failureReason: 'RETRIES_LIMIT_REACHED',
        };

        expect(state.get('schemaQueueStatus').toJS()).toEqual(expected);
      });
    });

    describe('CHAT_SCHEMA_QUEUE_STATUS_FAILED', () => {
      it('should set schemaQueueStatus to { isStarted: false, isEnded: true, isFailed: false, failureReason: UNATHORIZED }', () => {
        state = chatReducer(
          state,
          chatSchemaQueueFailedAction(QUEUE_FAILURE_BAD_REQUEST),
        );
        const expected = {
          isStarted: false,
          isEnded: false,
          isFailed: true,
          failureReason: 'UNAUTHORIZED',
        };

        expect(state.get('schemaQueueStatus').toJS()).toEqual(expected);
      });
    });
  });

  describe('answers', () => {
    const answer31: ChatAnswerType = {
      question: 3,
      nextQid: 20,
      id: 31,
      text: 'START ONLINE VISIT',
    };
    const answer31Selected: ChatAnswerType = {
      ...answer31,
      isSelected: true,
    };
    const answer32: ChatAnswerType = {
      question: 3,
      nextQid: 30,
      id: 32,
      text: 'START ONLINE VISIT',
    };
    const answer32Selected: ChatAnswerType = {
      ...answer32,
      isSelected: true,
    };
    const question: ChatQuestionType = {
      qid: 10,
      nextQid: null,
      choiceOpen: false,
      type: 'choice',
      id: 3,
      text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
      answers: [
        answer31,
        answer32,
      ],
    };
    const questionMultiple: ChatQuestionType = {
      ...question,
      type: 'multichoice',
    };

    describe('SELECT_ANSWER', () => {
      describe('single choice', () => {
        it('should set isSelected to true for answer: 31', () => {
          const state = getState({ currentQuestion: question });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/SELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          )
            .get('currentQuestion')
            .toJS();
          const expected = {
            ...question,
            answers: [
              answer31Selected,
              {
                ...answer32,
                isSelected: false,
              },
            ],
          };

          expect(received).toEqual(expected);
        });

        it('should set isSelected to false for answer: 31 and to true for answer: 32', () => {
          const state = getState({
            currentQuestion: {
              ...question,
              answers: [
                answer31Selected,
                answer32,
              ],
            },
          });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/SELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 32,
                text: 'START ONLINE VISIT',
              },
            }
          )
            .get('currentQuestion')
            .toJS();
          const expected = {
            ...question,
            answers: [
              {
                ...answer31,
                isSelected: false,
              },
              answer32Selected,
            ],
          };

          expect(received).toEqual(expected);
        });
      });
      describe('multiple choice', () => {
        it('should set isSelected to true for answer: 31', () => {
          const state = getState({ currentQuestion: questionMultiple });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/SELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          )
            .get('currentQuestion')
            .toJS();
          const expected = {
            ...questionMultiple,
            answers: [
              answer31Selected,
              {
                ...answer32,
              },
            ],
          };

          expect(received).toEqual(expected);
        });

        it('should set true for answer: 32 and leave answer: 31 untouched', () => {
          const state = getState({
            currentQuestion: {
              ...questionMultiple,
              answers: [
                answer31Selected,
                answer32,
              ],
            },
          });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/SELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 32,
                text: 'START ONLINE VISIT',
              },
            }
          )
            .get('currentQuestion')
            .toJS();
          const expected = {
            ...questionMultiple,
            answers: [
              answer31Selected,
              answer32Selected,
            ],
          };

          expect(received).toEqual(expected);
        });
      });
    });

    describe('UNSELECT_ANSWER', () => {
      describe('single choice', () => {
        it('should set isSelected to false for answer: 31 when it was selected', () => {
          const state = getState({
            currentQuestion: {
              ...question,
              answers: [
                answer31Selected,
                answer32,
              ],
            },
          });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/UNSELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          );
          const expected = {
            ...question,
            answers: [{
              ...answer31,
              isSelected: false,
            }, {
              ...answer32,
            }],
          };

          expect(received.get('currentQuestion').toJS()).toEqual(expected);
        });

        it('should keep isSelected to false for answer: 31 when it was unselected', () => {
          const state = getState({ currentQuestion: question });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/UNSELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          );
          const expected = {
            ...question,
            answers: [{
              ...answer31,
              isSelected: false,
            }, {
              ...answer32,
            }],
          };

          expect(received.get('currentQuestion').toJS()).toEqual(expected);
        });
      });

      describe('multiple choice', () => {
        it('should set isSelected to false for answer: 31 when it was selected', () => {
          const state = getState({
            currentQuestion: {
              ...questionMultiple,
              answers: [
                answer31Selected,
                answer32,
              ],
            },
          });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/UNSELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          );
          const expected = {
            ...questionMultiple,
            answers: [{
              ...answer31,
              isSelected: false,
            }, {
              ...answer32,
            }],
          };

          expect(received.get('currentQuestion').toJS()).toEqual(expected);
        });

        it('should keep isSelected to false for answer: 31 when it was unselected', () => {
          const state = getState({ currentQuestion: questionMultiple });
          const received = chatReducer(
            state,
            {
              type: 'app/Chat/UNSELECT_ANSWER',
              answer: {
                question: 3,
                nextQid: 20,
                id: 31,
                text: 'START ONLINE VISIT',
              },
            }
          );
          const expected = {
            ...questionMultiple,
            answers: [{
              ...answer31,
              isSelected: false,
            }, {
              ...answer32,
            }],
          };

          expect(received.get('currentQuestion').toJS()).toEqual(expected);
        });
      });
    });

    describe('SET_ANSWER', () => {
      it('should update content of answer: 31', () => {
        const state = getState({ currentQuestion: question });
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/SET_ANSWER',
            answer: {
              question: 3,
              nextQid: 20,
              meta: {
                meta_1: 'META_ADDED',
              },
              id: 31,
              text: 'TEXT_UPDATED',
            },
          }
        );
        const expected = {
          ...question,
          answers: [{
            ...answer31,
            meta: {
              meta_1: 'META_ADDED',
            },
            text: 'TEXT_UPDATED',
          },
          answer32,
          ],
        };

        expect(received.get('currentQuestion').toJS()).toEqual(expected);
      });
    });
  });

  describe('questions', () => {
    const question: ChatQuestionType = {
      qid: 10,
      nextQid: null,
      choiceOpen: false,
      type: 'choice',
      id: 3,
      text: 'QUESTION_CHOICE',
      threadId: 100,
      answers: [
        {
          question: 3,
          nextQid: 20,
          id: 31,
          text: 'START_ONLINE_VISIT',
          isSelected: true,
        },
        {
          question: 3,
          nextQid: 30,
          id: 32,
          text: 'END_ONLINE_VISIT',
          isSelected: false,
        },
      ],
    };
    const questionMultiple: ChatQuestionType = {
      ...question,
      type: 'multichoice',
    };
    const questionMessage: ChatQuestionType = {
      qid: 40,
      nextQid: 20,
      choiceOpen: false,
      type: 'message',
      id: 6,
      text: 'QUESTION_MESSAGE',
      threadId: 100,
      answers: [],
    };
    const questionText: ChatQuestionType = {
      qid: 20,
      nextQid: null,
      choiceOpen: false,
      type: 'text',
      id: 4,
      text: 'QUESTION_TEXT',
      threadId: 100,
      answers: [],
    };

    describe('SEND_QUESTION_REPLY', () => {
      let state;
      let received;

      describe('multichoice question', () => {
        beforeEach(() => {
          state = getState({
            currentQuestion: questionMultiple,
            answeredQuestions: [],
            questions: [
              question,
              questionText,
            ],
            messages: [],
          });
          const sendQuestionReplyAction: ChatSendQuestionReplyActionType = {
            type: 'app/Chat/SEND_QUESTION_REPLY',
            reply: {
              threadId: 100,
              answers: [31, 32],
              question: 3,
            },
          };
          received = chatReducer(
            state,
            sendQuestionReplyAction,
          );
        });

        it('should add { threadId, answers: [31, 32], quesiton } to questionsReplies', () => {
          const expected = [{
            threadId: 100,
            answers: [31, 32],
            question: 3,
          }];

          expect(received.get('questionsReplies').toJS()).toEqual(expected);
        });

        it('should add currentQuestion to answeredQuestions', () => {
          expect(received.get('answeredQuestions').toJS()).toEqual([questionMultiple]);
        });

        it('should add new messages group to messages', () => {
          const expected = [{
            showOnRight: true,
            items: [
              {
                attachments: [],
                created: '2017-02-14T13:51:48+01:00',
                editable: false,
                feId: 'SQR:10',
                questionId: 3,
                meta: undefined,
                status: 'loading',
                text: 'START_ONLINE_VISIT, END_ONLINE_VISIT',
                threadId: 100,
                type: CHAT_MESSAGE_TYPE_TEXT,
              },
            ],
            sender: null,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }];

          expect(received.get('messages').toJS()).toEqual(expected);
        });
      });

      describe('choice question', () => {
        beforeEach(() => {
          state = getState({
            currentQuestion: question,
            questions: [
              question,
              questionText,
            ],
            messages: [],
          });
          const sendQuestionReplyAction: ChatSendQuestionReplyActionType = {
            type: 'app/Chat/SEND_QUESTION_REPLY',
            reply: {
              threadId: 100,
              answers: [31],
              question: 3,
            },
          };
          received = chatReducer(
            state,
            sendQuestionReplyAction,
          );
        });

        it('should add { threadId, answers: [31], quesiton } to questionsReplies', () => {
          const expected = [{
            threadId: 100,
            answers: [31],
            question: 3,
          }];

          expect(received.get('questionsReplies').toJS()).toEqual(expected);
        });

        it('should add currentQuestion to answeredQuestions', () => {
          expect(received.get('answeredQuestions').toJS()).toEqual([question]);
        });

        it('should add new messages group to messages', () => {
          const expected = [{
            showOnRight: true,
            items: [
              {
                attachments: [],
                created: '2017-02-14T13:51:48+01:00',
                editable: false,
                feId: 'SQR:10',
                questionId: 3,
                status: 'loading',
                text: 'START_ONLINE_VISIT',
                threadId: 100,
                type: CHAT_MESSAGE_TYPE_TEXT,
                meta: undefined,
              },
            ],
            sender: null,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }];

          expect(received.get('messages').toJS()).toEqual(expected);
        });
      });

      describe('text question', () => {
        beforeEach(() => {
          state = getState({
            currentQuestion: questionText,
            questions: [
              question,
              questionText,
            ],
          });
          received = chatReducer(
            state,
            {
              type: 'app/Chat/SEND_QUESTION_REPLY',
              reply: {
                type: CHAT_MESSAGE_TYPE_TEXT,
                threadId: 100,
                question: 4,
                text: 'REPLY_TEXT',
              },
            }
          );
        });

        it('should add { text, threadId, question, type: message } to questionsReplies', () => {
          const expected = [{
            threadId: 100,
            question: 4,
            text: 'REPLY_TEXT',
            type: CHAT_MESSAGE_TYPE_TEXT,
          }];

          expect(received.get('questionsReplies').toJS()).toEqual(expected);
        });

        it('should add currentQuestion to answeredQuestions', () => {
          expect(received.get('answeredQuestions').toJS()).toEqual([questionText]);
        });

        it('should add new messages group to messages', () => {
          const expected = [{
            showOnRight: true,
            items: [
              {
                attachments: [],
                created: '2017-02-14T13:51:48+01:00',
                editable: false,
                feId: 'SQR:20',
                questionId: 4,
                status: 'loading',
                text: 'REPLY_TEXT',
                threadId: 100,
                type: 'message',
                meta: undefined,
              },
            ],
            sender: null,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }];

          expect(received.get('messages').toJS()).toEqual(expected);
        });
      });
    });

    describe('NEXT_QUESTION_ACTION', () => {
      let state;
      let received;

      describe('choice question', () => {
        beforeEach(() => {
          state = getState({
            currentQuestion: question,
            questions: [
              question,
              questionText,
            ],
            messages: [],
          });
          received = chatReducer(
            state,
            {
              type: 'app/Chat/NEXT_QUESTION',
              threadId: 100,
            }
          );
        });

        it('should put question #4 to currentQuestion', () => {
          expect(
            received.get('currentQuestion')
              .toJS()
          ).toEqual(questionText);
        });

        it('should add new message to messages', () => {
          expect(
            received.get('messages')
              .toJS()
          ).toEqual([{
            showOnRight: false,
            items: [
              {
                attachments: [],
                created: '2017-02-14T13:51:48+01:00',
                editable: false,
                feId: 'SQM:20',
                questionId: 4,
                status: 'loading',
                text: 'QUESTION_TEXT',
                threadId: 100,
                type: 'message',
                meta: undefined,
              },
            ],
            sender: null,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }]);
        });
      });
    });

    describe('CHANGE_QUESTION', () => {
      let state;

      beforeEach(() => {
        state = getState({
          currentQuestion: question,
          questions: [],
          messages: [],
        });
      });

      it('should put action.question to state.currentQuestion', () => {
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/CHANGE_QUESTION',
            question: questionMessage,
          }
        );
        expect(
          received.get('currentQuestion')
            .toJS()
        ).toEqual(questionMessage);
      });
    });

    describe('THREAD_ROLLBACK_ACTION', () => {
      let state;
      let received;
      const msgSQM40 = {
        text: 'QUESTION_MESSAGE',
        feId: 'SQM:40',
      };
      const msgSQM10 = {
        text: 'QUESTION_CHOICE',
        feId: 'SQM:10',
      };
      const msgSQR10 = {
        text: 'QUESTION_CHOICE_ANSWER',
        feId: 'SQR:10',
      };
      const messages = [{
        items: [
          msgSQM40,
          msgSQM10,
        ],
      }, {
        items: [
          msgSQR10,
        ],
      }];
      const questionReply2 = {
        question: 2,
        thread: 100,
      };
      const questionReply3 = {
        question: 3,
        thread: 100,
      };
      const questionReply4 = {
        question: 4,
        thread: 100,
      };
      const questionsReplies = [
        questionReply2,
        questionReply3,
        questionReply4,
      ];
      const rollback = () => chatReducer(
        state,
        {
          type: 'app/Chat/THREAD_ROLLBACK',
          threadId: 100,
          feId: 'SQR:10',
        }
      );
      const getCurrentQuestionQid = (s) => s.get('currentQuestion').get('qid');

      beforeEach(() => {
        state = getState({
          currentQuestion: questionText,
          answeredQuestions: [
            question,
            questionMessage,
          ],
          questions: [
            questionMessage,
            question,
            questionText,
          ],
          messages,
          questionsReplies,
        });
      });

      it('should change currentQuestion to qid:10', () => {
        expect(
          getCurrentQuestionQid(state)
        ).toBe(20);

        received = rollback();

        expect(
          getCurrentQuestionQid(received)
        ).toBe(10);
      });

      it('should remove all messages starting from SQR:10', () => {
        received = rollback();

        expect(
          received.get('messages').toJS()
        ).toEqual([{
          items: [
            msgSQM40,
            msgSQM10,
          ],
        }]);
      });

      it('should remove questionReplies from SQR:10 till end', () => {
        received = rollback();

        expect(
          received.get('questionsReplies').toJS()
        ).toEqual([
          questionReply2,
        ]);
      });

      it('should remove answeredQuestions from qid:10 till end', () => {
        received = rollback();

        expect(
          received.get('answeredQuestions').toJS()
        ).toEqual([]);
      });
    });
  });

  describe('set thread id', () => {
    it('should set threadId', () => {
      const state = initialState.merge({
        threadId: null,
      });
      const received = chatReducer(
        state,
        {
          type: 'app/Chat/SET_THREAD_ID',
          id: 1,
        }
      );

      expect(received.get('threadId')).toBe(1);
    });
  });
});
