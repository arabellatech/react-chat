/* @flow */

import {
  Map,
  List,
} from 'immutable';
import { createSelector } from 'reselect';
import _mapKeys from 'lodash/mapKeys';
import _forEach from 'lodash/forEach';
import _reduce from 'lodash/reduce';
import _camelCase from 'lodash/camelCase';
import Graph from 'node-dijkstra';
import {
  findBy,
  filterBy,
  filterByMultiple,
  listPick,
  getChatDomain,
} from '../Chat/selectors';
import {
  getVariablesPool,
  selectVariablesPool,
} from '../VariablesPool/selectors';
import {
  CHAT_QUESTION_TYPE_FORK,
} from './constants';
import type {
  ChatStateType,
  ChatQuestionTypeType,
  ChatQuestionReplyType,
} from '../types';

import { CHAT_MESSAGE_TYPE_TEXT } from '../Message/constants';
import { getForkedQid } from '../utils';

type StateType = Map<string, any>;
type StateListType = List<StateType>;

// TODO this ones should be named getX
export const getSchemaId = (state: ChatStateType) => state.get('schemaId');
export const getSchemaType = (state: ChatStateType) => state.get('schemaType');
export const getSchemaCode = (state: ChatStateType) => state.get('schemaCode');
export const getLoadingSchema = (state: ChatStateType) => state.get('loadingSchema');
export const getSchemaQueueStatus = (state: ChatStateType) => state.get('schemaQueueStatus');
export const getQuestions = (state: ChatStateType) => state.get('questions');
export const getCurrentQuestion = (state: ChatStateType) => state.get('currentQuestion');
export const getPreviousQuestion = (state: ChatStateType) => state.get('previousQuestion');
export const getAnsweredQuestions = (state: ChatStateType) => state.get('answeredQuestions');
export const getQuestionReplies = (state: ChatStateType) => state.get('questionsReplies');
export const getQuestionType = (state: ChatStateType) => state.get('type');
export const getQuestionChoiceOpen = (state: ChatStateType) => state.get('choiceOpen');
export const getQuestionId = (state: ChatStateType) => state.get('id');
export const getQuestionQid = (state: ChatStateType) => state.get('qid');
export const getQuestionNextQid = (state: ChatStateType) => state.get('nextQid');
export const getAnswerText = (state: ChatStateType) => state.get('text');
export const selectAnswers = (state: ChatStateType) => state.get('answers');
export const getQuestionMetaMessageType = (state: ChatStateType) => {
  const meta = state.get('meta');
  if (!meta || !meta.get('messageType')) {
    return CHAT_MESSAGE_TYPE_TEXT;
  }

  return meta.get('messageType');
};

export const filterSelectedAnswers = (answers: StateListType) => filterBy(answers, 'isSelected', true);

// not sure if the name is proper, can be also filterAnswersWithoutId
export const filterTextAnswers = (answers: StateListType) => filterBy(answers, 'type', 'text');

export const makeListTrimmer = (listItemIdName: string, targetIdName: string) =>
  (itemsList: List<ChatStateType>, targetItem: ChatStateType) => {
    const targetId = targetItem ? targetItem.get(targetIdName) : null;
    const size = itemsList.size;
    let trimmedList = new List();

    for (let i = 0; i < size; i += 1) {
      const currentReply = itemsList.get(i);

      trimmedList = trimmedList.push(currentReply);

      if (currentReply.get(listItemIdName) === targetId) {
        break;
      }
    }

    return trimmedList;
  };

// / TODO

/* any question level selectors */

export const selectIsQuestionChoice = createSelector(
  getQuestionType,
  (type: ChatQuestionTypeType) => type === 'choice' || type === 'multichoice' || type === 'yesno'
);

export const selectIsQuestionChoiceOpen = createSelector(
  [
    getQuestionType,
    getQuestionChoiceOpen,
  ],
  (type: ChatQuestionTypeType, choiceOpen: boolean) => type === 'choice' && choiceOpen
);

export const selectIsQuestionMultiple = createSelector(
  getQuestionType,
  (type: ChatQuestionTypeType) => type === 'multichoice'
);

export const selectIsQuestionText = createSelector(
  getQuestionType,
  (type: ChatQuestionTypeType) => type === 'text' || type === 'message' || type === 'date'
);

export const selectIsQuestionControl = createSelector(
  getQuestionType,
  (type: ChatQuestionTypeType) => type === 'control'
);

export const selectSelectedAnswers = createSelector(
  selectAnswers,
  filterSelectedAnswers,
);

export const selectChoiceOpenTextAnswers = createSelector(
  selectAnswers,
  filterTextAnswers,
);

export const selectAnswersText = createSelector(
  selectAnswers,
  (answers) => answers.map((answer) => answer.get('text'))
);

// for choice open type we acceppt also text answer
// teoretically, we can should allow only one this kinda question
// but for backward compatibility strings are joined from array
export const selectChoiceOpenAnswerText = createSelector(
  selectChoiceOpenTextAnswers,
  (answers) => answers.map((answer) => answer.get('text')),
);

export const selectFirstAnswerMeta = createSelector(
  selectAnswers,
  (answers) => {
    if (answers.size) {
      const meta = answers.last().get('meta');
      if (meta) {
        return meta.toJS();
      }
    }
    return {};
  }
);

export const selectFirstAnswerAttachments = createSelector(
  selectAnswers,
  (answers) => {
    if (answers.size) {
      const attachments = answers.first().get('attachments');
      if (attachments) {
        return attachments.toJS();
      }
    }
    return null;
  }
);

export const selectSelectedAnswersIds = createSelector(
  selectSelectedAnswers,
  (answers) => answers.map((answer) => answer.get('id'))
);

export const selectQuestionReply = createSelector(
  [
    getQuestionId,
    getQuestionMetaMessageType,
    selectIsQuestionChoice,
    selectIsQuestionChoiceOpen,
    selectIsQuestionText,
    selectSelectedAnswersIds,
    selectAnswersText,
    selectFirstAnswerMeta,
    selectFirstAnswerAttachments,
    selectChoiceOpenAnswerText,
  ],
  (
    questionId,
    messageType,
    isQuestionChoice,
    isQuestionChoiceOpen,
    isQuestionText,
    selectedAnswersIds,
    answersText,
    firstAnswerMeta,
    firstAnswerAttachments,
    textAnswers,
  ) => {
    const reply: ChatQuestionReplyType = {
      threadId: -1,
      type: messageType,
      question: questionId,
    };

    if (isQuestionChoice) {
      reply.answers = selectedAnswersIds.toJS();
      if (isQuestionChoiceOpen) {
        reply.text = textAnswers.toJS().join(', ');
      }
    } else if (isQuestionText) {
      reply.text = answersText
        .toJS()
        .join(', ');
    }
    reply.meta = firstAnswerMeta;

    if (firstAnswerAttachments) {
      reply.attachments = firstAnswerAttachments;
    }

    return reply;
  }
);

/* chat level selectors (for use in chat reducers) */

/* currentQuestion related selectors */

export const selectCurrentAnswers = createSelector(
  getCurrentQuestion,
  selectAnswers,
);

export const selectCurrentSelectedAnswers = createSelector(
  selectCurrentAnswers,
  filterSelectedAnswers,
);

export const selectCurrentIsQuestionChoice = createSelector(
  getCurrentQuestion,
  selectIsQuestionChoice,
);

export const selectCurrentIsQuestionMultiple = createSelector(
  getCurrentQuestion,
  selectIsQuestionMultiple,
);

export const selectCurrentQuestionReply = createSelector(
  getCurrentQuestion,
  selectQuestionReply,
);

export const selectCurrentNextQid = createSelector(
  getCurrentQuestion,
  getQuestionNextQid,
);

export const selectNextQuestionQid = createSelector(
  [
    selectCurrentIsQuestionChoice,
    selectCurrentSelectedAnswers,
    selectCurrentNextQid,
  ],
  (isQuestionChoice, selectedAnswers, questionNextQid) => {
    let nextQuestionQid = questionNextQid;

    if (isQuestionChoice && selectedAnswers.size) {
      nextQuestionQid = getQuestionNextQid(selectedAnswers.get(0));
    }

    return nextQuestionQid;
  }
);

/* questions related selectors */

export const makeSelectQuestionById = (questionId: number) => createSelector(
  getQuestions,
  (questions) => findBy(questions, 'id', questionId) || Map()
);

export const makeSelectQuestionByQid = (qid: number) => createSelector(
  getQuestions,
  (questions) => findBy(questions, 'qid', qid)
);

export const makeSelectAnswersForQuestion = (questionId: number) => createSelector(
  makeSelectQuestionById(questionId),
  (question) => question.get('answers'),
);

export const makeSelectAnswersByIdForQuestion = (answersIds: Array<number>, questionId: number) => createSelector(
  makeSelectAnswersForQuestion(questionId),
  (answers) => answers && filterByMultiple(answers, 'id', answersIds),
);

export const makeSelectAnswersTextByIdForQuestion = (answersIds: Array<number>, questionId: number) => createSelector(
  makeSelectAnswersByIdForQuestion(answersIds, questionId),
  (answers) => answers && listPick(answers, 'text'),
);

export const selectNextQuestion = createSelector(
  [
    getQuestions,
    selectNextQuestionQid,
    getVariablesPool,
  ],
  (questions, nextQuestionQid, variablesPool) => {
    let nextQuestion = filterBy(questions, 'qid', nextQuestionQid);

    while (nextQuestion && nextQuestion.first() && nextQuestion.first().get('type') === CHAT_QUESTION_TYPE_FORK) {
      if (!variablesPool) {
        throw Error('Custom variables are required!');
      }

      const forkingRules = nextQuestion.first().get('forkingRules');

      if (!forkingRules) {
        throw Error('Forking rules are required!');
      }

      const forkedQuestionQid = getForkedQid(
        forkingRules.toJS(),
        variablesPool.toJS()
      );

      if (!forkedQuestionQid) {
        const questionText = nextQuestion.get('text');

        throw Error(`Unable to get forked qid. Check forking rules configuration. Next fork question is ${questionText}`);
      }

      nextQuestion = filterBy(questions, 'qid', forkedQuestionQid);
    }

    return nextQuestion;
  },
);

/* questions replies related selectors */

export const makeSelectQuestionsRepliesUpToQid = (qid: number) => createSelector(
  [
    getQuestionReplies,
    makeSelectQuestionByQid(qid),
  ],
  makeListTrimmer('question', 'id')
);

/* answeredQuestions related selectors */

export const makeSelectAnsweredQuestionsUpToQid = (qid: number) => createSelector(
  [
    getAnsweredQuestions,
    makeSelectQuestionByQid(qid),
  ],
  makeListTrimmer('id', 'id')
);

/* schema related selectors */

export const selectSchemaQueueStatusIsEnded = createSelector(
  getSchemaQueueStatus,
  (queueStatus) => queueStatus.get('isEnded')
);

export const selectSchemaQueueStatusIsFailed = createSelector(
  getSchemaQueueStatus,
  (queueStatus) => queueStatus.get('isFailed')
);

/* top level selectors (for use in mapStateToProps or global reducers) */

export const selectChatCurrentQuestion = createSelector(
  getChatDomain,
  getCurrentQuestion,
);

export const selectChatPreviousQuestion = createSelector(
  getChatDomain,
  getPreviousQuestion,
);

export const selectChatAnsweredQuestions = createSelector(
  getChatDomain,
  getAnsweredQuestions,
);

export const selectChatCurrentAnswers = createSelector(
  getChatDomain,
  selectCurrentAnswers,
);

export const selectChatCurrentSelectedAnswers = createSelector(
  getChatDomain,
  selectCurrentSelectedAnswers,
);

export const selectChatCurrentReply = createSelector(
  getChatDomain,
  selectCurrentQuestionReply,
);

export const selectChatNextQuestionQid = createSelector(
  getChatDomain,
  selectNextQuestionQid,
);

export const selectChatQuestionsReplies = createSelector(
  getChatDomain,
  getQuestionReplies,
);

export const getChatLastQuestionReply = createSelector(
  getQuestionReplies,
  (replies) => replies.last()
);

export const selectChatLastQuestionReply = createSelector(
  getChatDomain,
  getChatLastQuestionReply,
);

export const selectSchemaId = createSelector(
  getChatDomain,
  getSchemaId,
);

export const selectSchemaType = createSelector(
  getChatDomain,
  getSchemaType,
);

export const selectSchemaCode = createSelector(
  getChatDomain,
  getSchemaCode,
);

export const selectLoadingSchema = createSelector(
  getChatDomain,
  getLoadingSchema,
);

export const selectChatSchemaQueueStatus = createSelector(
  getChatDomain,
  getSchemaQueueStatus,
);

export const selectSchemaQueueRequestsProgress = createSelector(
  selectChatSchemaQueueStatus,
  (queueStatus) => queueStatus.get('requestsProgress')
);

export const selectChatSchemaQueueStatusIsEnded = createSelector(
  getChatDomain,
  selectSchemaQueueStatusIsEnded,
);

export const selectChatSchemaQueueStatusIsFailed = createSelector(
  getChatDomain,
  selectSchemaQueueStatusIsFailed,
);

export const selectChatQuestions = createSelector(
  getChatDomain,
  getQuestions,
);

export const selectChatNrOfStepsReplies = createSelector(
  selectChatQuestions,
  selectChatQuestionsReplies,
  selectChatCurrentQuestion,
  (questions, questionsReplies, currentQuestion) => {
    const repliedSteps = questionsReplies
      .filter((reply) => reply.get('question') !== currentQuestion.get('id'));
    const repliedQuestions = repliedSteps
      .filter((reply) => {
        const question = findBy(questions, 'id', reply.get('question'));

        return !question.getIn(['meta', 'doNotCountAsQuestion']);
      });

    return {
      nrOfSteps: repliedSteps.size,
      nrOfQuestions: repliedQuestions.size,
    };
  }
);

export const selectChatCurrentQuestionId = createSelector(
  selectChatCurrentQuestion,
  getQuestionQid,
);

export const selectChatQuestionsGroupByQid = createSelector(
  selectChatQuestions,
  (questions) => _mapKeys(questions.toJS(), (question) => question.qid),
);

export const selectChatFinishQid = createSelector(
  selectChatQuestions,
  (questions) => {
    let finishQid = null;

    questions.forEach((question) => {
      const {
        qid,
        nextQid,
        meta,
      } = question.toJS();

      if (nextQid === -1 && meta && meta.finalStep) {
        finishQid = qid;
      }
    });

    return finishQid;
  },
);

export const selectMinNumberOfStepsToEnd = createSelector(
  selectChatQuestionsGroupByQid,
  selectVariablesPool,
  selectChatCurrentQuestionId,
  selectChatFinishQid,
  (questions, variablesPool, questionId, finishQid) => {
    if (!questionId || !finishQid) {
      return {};
    }

    const shortestPath = getShortestPath(questions, variablesPool, questionId, finishQid);
    const shortestPathWithoutNoQuestions = excludeNoQuestionStepsFromPath(questions, shortestPath);

    return {
      nrOfSteps: shortestPath && shortestPath.length,
      nrOfQuestions: shortestPathWithoutNoQuestions.length,
    };
  }
);

function excludeNoQuestionStepsFromPath(questions, shortestPath) {
  if (!shortestPath) {
    return [];
  }

  return shortestPath.filter((qid) => {
    const question = questions[qid];

    return !question.meta || !question.meta.doNotCountAsQuestion;
  });
}

export const selectQuestionsRepliesProgress = createSelector(
  selectChatNrOfStepsReplies,
  selectMinNumberOfStepsToEnd,
  (nrOfStepsReplies, minNumberOfStepsToEnd) => {
    const totalSteps = nrOfStepsReplies.nrOfSteps + minNumberOfStepsToEnd.nrOfSteps;
    const totalQuestions = nrOfStepsReplies.nrOfQuestions + minNumberOfStepsToEnd.nrOfQuestions;
    const totalProgress = Math.round((nrOfStepsReplies.nrOfSteps * 100) / totalSteps);

    return {
      minNumberOfStepsToEnd: minNumberOfStepsToEnd.nrOfSteps,
      minNumberOfQuestionsToEnd: minNumberOfStepsToEnd.nrOfQuestions,
      nrOfStepsReplies: nrOfStepsReplies.nrOfSteps,
      nrOfQuestionsReplies: nrOfStepsReplies.nrOfQuestions,
      totalQuestions,
      totalSteps,
      totalProgress,
    };
  }
);

// TODO: calculate more sophisticated equations on assignment
//       supported: a = 0
//       possibly:  a = a + 1
//       possibly:  b = a + 1
export const calculateVariablesPool = (questionId: number, answers?: Array<number>) => createSelector(
  [
    makeSelectQuestionById(questionId),
    getVariablesPool,
  ],
  (question, currentVariablesPool) => {
    const newVariablesPool = {};
    const questionVariablesModification = question.get('variableModifications');
    const questionAnswers = question.get('answers');

    if (questionVariablesModification && questionVariablesModification.size) {
      questionVariablesModification.forEach((variable) => {
        const variableName = _camelCase(variable.get(0));
        let variableValue = parseInt(variable.get(1), 10);
        if (Number.isNaN(variableValue)) {
          variableValue = variable.get(1);
        }
        newVariablesPool[variableName] = variableValue;
      });
    }

    if (answers && questionAnswers.size) {
      answers.forEach((answer) => {
        const questionAnswerVariablesMofidication = questionAnswers.find((v) => v.get('id') === answer).get('variableModifications');

        if (questionAnswerVariablesMofidication) {
          questionAnswerVariablesMofidication.forEach((variable) => {
            const variableName = _camelCase(variable.get(0));
            let variableValue = parseInt(variable.get(1), 10);
            if (Number.isNaN(variableValue)) {
              variableValue = variable.get(1);
            }
            newVariablesPool[variableName] = variableValue;
          });
        }
      });
    }

    return {
      ...currentVariablesPool.toObject(),
      ...newVariablesPool,
    };
  }
);

function getShortestPath(questions, variablesPool, fromQid, toQid) {
  if (questions && variablesPool) {
    const route = new Graph();
    const questionsRelationGraph = getQuestionsRelationGraph(questions, variablesPool);

    _forEach(questionsRelationGraph, (nodeToQids, nodeFromQid) => {
      route.addVertex(nodeFromQid, nodeToQids);
    });

    route.addVertex(toQid, {});

    return route.shortestPath(fromQid.toString(), toQid.toString());
  }

  return [];
}

function getQuestionsRelationGraph(questions, variablesPool) {
  let questionsRelationGraph = Map();
  const forkQuestions = [];

  _forEach(questions, (question) => {
    const {
      answers,
      forkingRules,
      nextQid,
      qid,
      type,
    } = question;

    if (type === 'fork') {
      forkQuestions.push(qid);
    }

    if (Array.isArray(answers) && answers.length) {
      answers.forEach((answer) => {
        if (isValidNextQid(answer.nextQid)) {
          questionsRelationGraph = questionsRelationGraph.setIn([qid, answer.nextQid], 1);
        }
      });
    } else if (nextQid) {
      if (isValidNextQid(nextQid)) {
        questionsRelationGraph = questionsRelationGraph.setIn([qid, nextQid], 1);
      }
    } else if (Array.isArray(forkingRules)) {
      let allForkedPathArePossible = true;
      const forkingRulesLength = forkingRules.length;

      for (let i = 0; i < forkingRulesLength; i += 1) {
        const forkingRule = forkingRules[i];

        if (isValidForkedPath(forkingRule, variablesPool)) {
          questionsRelationGraph = addForkedPathToQuestionsRelationGraph(questionsRelationGraph, qid, forkingRule);
          allForkedPathArePossible = false;
          break;
        }
      }

      if (allForkedPathArePossible) {
        questionsRelationGraph = addAllForkedPaths(questionsRelationGraph, qid, forkingRules);
      }
    }
  });

  return reduceForkQuestions(questionsRelationGraph, forkQuestions);
}

function isValidNextQid(nextQid) {
  return nextQid && nextQid !== -1;
}

function isValidForkedPath(forkingRule, variablesPool) {
  const rule = forkingRule[1].split('==');

  if (rule.length === 2) {
    const variableName = _camelCase(rule[0].trim());
    const variableValue = rule[1].trim();
    const variableValueInVariablesPool = `${variablesPool.get(variableName)}`;

    if (variableValueInVariablesPool === variableValue) {
      return true;
    }
  }
  return false;
}

function addAllForkedPaths(questionsRelationGraph, qid, forkingRules) {
  let questionsRelationGraphWithForkedPaths = questionsRelationGraph;

  forkingRules.forEach((forkingRule) => {
    questionsRelationGraphWithForkedPaths = addForkedPathToQuestionsRelationGraph(questionsRelationGraphWithForkedPaths, qid, forkingRule);
  });

  return questionsRelationGraphWithForkedPaths;
}

function addForkedPathToQuestionsRelationGraph(questionsRelationGraph, qid, forkingRule) {
  let questionsRelationGraphWithForkedPaths = questionsRelationGraph;
  const nextForkQid = Number(forkingRule[0]);

  if (isValidNextQid(nextForkQid)) {
    questionsRelationGraphWithForkedPaths = questionsRelationGraphWithForkedPaths.setIn([qid, nextForkQid], 1);
  }

  return questionsRelationGraphWithForkedPaths;
}

function reduceForkQuestions(questionsRelationGraph, forkQuestions) {
  const reducedQuestionsRelationGraph = questionsRelationGraph.toJS();

  forkQuestions.forEach((forkQid) => {
    const questionsRelatedWithFork = getQuestionsRelatedWithFork(reducedQuestionsRelationGraph, forkQid);

    _forEach(questionsRelatedWithFork, (questionRelation, questionRelationKey) => {
      delete reducedQuestionsRelationGraph[questionRelationKey][forkQid];

      reducedQuestionsRelationGraph[questionRelationKey] = {
        ...reducedQuestionsRelationGraph[questionRelationKey],
        ...reducedQuestionsRelationGraph[forkQid],
      };
    });
  });

  forkQuestions.forEach((forkQid) => {
    delete reducedQuestionsRelationGraph[forkQid];
  });

  return reducedQuestionsRelationGraph;
}

function getQuestionsRelatedWithFork(questionsRelationGraph, forkQid) {
  return _reduce(
    questionsRelationGraph,
    (reducedQuestionsRelationGraph, question, key) => {
      if (question[forkQid]) {
        return {
          ...reducedQuestionsRelationGraph,
          [key]: question,
        };
      }

      return reducedQuestionsRelationGraph;
    },
    {}
  );
}
