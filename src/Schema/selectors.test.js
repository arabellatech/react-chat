/* @flow */

import {
  fromJS,
  Map,
} from 'immutable';
import {
  selectCurrentAnswers,
  selectCurrentSelectedAnswers,
  selectCurrentQuestionReply,
  selectCurrentIsQuestionChoice,
  selectCurrentNextQid,
  selectNextQuestionQid,
  selectSchemaQueueStatusIsEnded,
  selectSchemaQueueStatusIsFailed,

  makeSelectQuestionById,
  makeSelectAnswersForQuestion,
  makeSelectAnswersByIdForQuestion,
} from './selectors';

describe('currentQuestion selectors:', () => {
  const answer11 = {
    id: 11,
    nextQid: 30,
  };
  const answer11Selected = {
    ...answer11,
    isSelected: true,
  };
  const answer12 = {
    id: 12,
    nextQid: 40,
  };
  const answer12Selected = {
    ...answer12,
    isSelected: true,
  };
  const defaultQuestion = {
    id: 10,
    nextQid: 20,
    type: 'choice',
  };
  const getState = (answers, question) => fromJS({
    currentQuestion: {
      ...defaultQuestion,
      ...question,
      answers,
    },
  });


  describe('selectCurrentAnswers', () => {
    it('should all items from currentQuestion.answers', () => {
      const chatState = getState([
        answer11,
        answer12Selected,
      ]);

      expect(
        selectCurrentAnswers(chatState)
          .toJS()
      ).toEqual([
        answer11,
        answer12Selected,
      ]);
    });
  });

  describe('selectCurrentSelectedAnswers', () => {
    it('should return empty array when no answer selected', () => {
      const currentQuestion = getState([
        answer11,
        answer12,
      ]);

      expect(
        selectCurrentSelectedAnswers(currentQuestion)
          .toJS()
      ).toEqual([]);
    });

    it('should return single element when just one answer selected', () => {
      const currentQuestion = getState([
        answer11,
        answer12Selected,
      ]);

      expect(
        selectCurrentSelectedAnswers(currentQuestion)
          .toJS()
      ).toEqual([
        answer12Selected,
      ]);
    });

    it('should return two elements when just two answers selected', () => {
      const currentQuestion = getState([
        answer11Selected,
        answer12Selected,
      ]);

      expect(
        selectCurrentSelectedAnswers(currentQuestion)
          .toJS()
      ).toEqual([
        answer11Selected,
        answer12Selected,
      ]);
    });
  });

  describe('selectCurrentIsQuestionChoice', () => {
    it('should return true when type is choice', () => {
      const state = getState([], {
        type: 'choice',
      });

      expect(selectCurrentIsQuestionChoice(state)).toBe(true);
    });

    it('should return true when type is multichoice', () => {
      const state = getState([], {
        type: 'multichoice',
      });

      expect(selectCurrentIsQuestionChoice(state)).toBe(true);
    });

    it('should return true when type is yesno', () => {
      const state = getState([], {
        type: 'yesno',
      });

      expect(selectCurrentIsQuestionChoice(state)).toBe(true);
    });

    it('should return false when type is text', () => {
      const state = getState([], {
        type: 'text',
      });

      expect(selectCurrentIsQuestionChoice(state)).toBe(false);
    });
  });

  describe('selectCurrentQuestionReply', () => {
    it('should return { answers: [12], question: 10 } for choice question', () => {
      const state = getState([
        answer11,
        answer12Selected,
      ]);
      const expected = {
        answers: [
          12,
        ],
        meta: {},
        question: 10,
        threadId: -1,
        type: 'message',
      };

      expect(selectCurrentQuestionReply(state)).toEqual(expected);
    });

    it('should return { answers: [11, 12], question: 10 } for multichoice question', () => {
      const state = getState([
        answer11Selected,
        answer12Selected,
      ], {
        type: 'multichoice',
      });
      const expected = {
        answers: [
          11,
          12,
        ],
        meta: {},
        question: 10,
        threadId: -1,
        type: 'message',
      };

      expect(selectCurrentQuestionReply(state)).toEqual(expected);
    });

    it('should return { text: ANSWER_TEXT, question: 10 } for text question', () => {
      const state = getState([{
        text: 'ANSWER_TEXT',
      }], {
        type: 'text',
      });
      const expected = {
        meta: {},
        question: 10,
        text: 'ANSWER_TEXT',
        threadId: -1,
        type: 'message',
      };

      expect(selectCurrentQuestionReply(state)).toEqual(expected);
    });

    it('should include meta from first answer', () => {
      const state = getState([{
        ...answer11,
        meta: {
          test: 1,
        },
      }]);

      expect(selectCurrentQuestionReply(state).meta).toEqual({
        test: 1,
      });
    });

    it('should include attachments from first answer', () => {
      const state = getState([{
        ...answer11,
        attachments: {
          test: 2,
        },
      }]);

      expect(selectCurrentQuestionReply(state).attachments).toEqual({
        test: 2,
      });
    });
  });

  describe('selectCurrentNextQid', () => {
    it('should return nextQid', () => {
      const state = getState();

      expect(selectCurrentNextQid(state)).toBe(20);
    });
  });

  describe('selectCurrentNextQuestionQid', () => {
    it('should return selected answer nextQid when type:choice', () => {
      const state = getState([
        answer11,
        answer12Selected,
      ], {
        type: 'choice',
      });

      expect(selectNextQuestionQid(state)).toEqual(40);
    });

    it('should return selected answer nextQid when type:yesno', () => {
      const state = getState([
        answer11Selected,
        answer12,
      ], {
        type: 'yesno',
      });

      expect(selectNextQuestionQid(state)).toEqual(30);
    });

    it('should return nextQid when type:text', () => {
      const state = getState([], {
        type: 'text',
      });

      expect(selectNextQuestionQid(state)).toEqual(20);
    });

    it('should return nextQid when type:message', () => {
      const state = getState([], {
        type: 'message',
      });

      expect(selectNextQuestionQid(state)).toEqual(20);
    });
  });
});

describe('questions selectors', () => {
  let state;
  const questionChoiceAnswers = [{
    id: 11,
    text: 'ANSWER_11_TEXT',
  }, {
    id: 12,
    text: 'ANSWER_12_TEXT',
  }, {
    id: 13,
    text: 'ANSWER_13_TEXT',
  }];
  const questionChoice = {
    id: 10,
    nextQid: 20,
    type: 'choice',
    answers: questionChoiceAnswers,
  };
  const questionText = {
    id: 20,
    nextQid: 30,
    text: 'QUESTION_TEXT',
    answers: [],
  };

  beforeEach(() => {
    state = fromJS({
      questions: [
        questionText,
        questionChoice,
      ],
      something: true,
    });
  });


  describe('makeSelectQuestionById', () => {
    it('should return empty Map if matching question is not found', () => {
      expect(
        makeSelectQuestionById(100)(state)
      ).toEqual(Map());
    });

    it('should return question matching by id', () => {
      expect(
        makeSelectQuestionById(10)(state)
          .toJS()
      ).toEqual(questionChoice);

      expect(
        makeSelectQuestionById(20)(state)
          .toJS()
      ).toEqual(questionText);
    });
  });

  describe('makeSelectAnswersForQuestion', () => {
    it('should return undefined if question not found', () => {
      expect(
        makeSelectAnswersForQuestion(100)(state)
      ).toEqual(undefined);
    });

    it('should return answers form matching question', () => {
      expect(
        makeSelectAnswersForQuestion(10)(state)
          .toJS()
      ).toEqual(questionChoiceAnswers);

      expect(
        makeSelectAnswersForQuestion(20)(state)
          .toJS()
      ).toEqual([]);
    });
  });

  describe('makeSelectAnswersByIdForQuestion', () => {
    it('should return undefined when question does not exist', () => {
      expect(
        makeSelectAnswersByIdForQuestion([10], 100)(state)
      ).toEqual(undefined);
    });

    it('should return undefined when question exist, but no matchin answer', () => {
      expect(
        makeSelectAnswersByIdForQuestion([15], 100)(state)
      ).toEqual(undefined);
    });

    it('should return only answers matchin passed in ids', () => {
      expect(
        makeSelectAnswersByIdForQuestion([11], 10)(state)
          .toJS()
      ).toEqual([{
        id: 11,
        text: 'ANSWER_11_TEXT',
      }]);

      expect(
        makeSelectAnswersByIdForQuestion([13], 10)(state)
          .toJS()
      ).toEqual([{
        id: 13,
        text: 'ANSWER_13_TEXT',
      }]);

      expect(
        makeSelectAnswersByIdForQuestion([11, 12], 10)(state)
          .toJS()
      ).toEqual([{
        id: 11,
        text: 'ANSWER_11_TEXT',
      }, {
        id: 12,
        text: 'ANSWER_12_TEXT',
      }]);
    });
  });
});

describe('schema status selectors', () => {
  describe('selectSchemaQueueStatusIsEnded', () => {
    it('should return true if state is QUEUE_STATUS_ENDED', () => {
      const state = fromJS({
        schemaQueueStatus: {
          isEnded: true,
        },
      });

      expect(selectSchemaQueueStatusIsEnded(state)).toBe(true);
    });

    it('should return false if state is NOT QUEUE_STATUS_ENDED', () => {
      const state = fromJS({
        schemaQueueStatus: {
          isEnded: false,
        },
      });

      expect(selectSchemaQueueStatusIsEnded(state)).toBe(false);
    });
  });

  describe('selectSchemaQueueStatusIsFailed', () => {
    it('should return true if state is CHAT_SCHEMA_QUEUE_STATUS_FAILED', () => {
      const state = fromJS({
        schemaQueueStatus: {
          isFailed: true,
          failureReason: 'SOME_REASON',
        },
      });

      expect(selectSchemaQueueStatusIsFailed(state)).toBe(true);
    });

    it('should return false if state is NOT CHAT_SCHEMA_QUEUE_STATUS_FAILED', () => {
      const state = fromJS({
        schemaQueueStatus: {
          isFailed: false,
          failureReason: null,
        },
      });

      expect(selectSchemaQueueStatusIsFailed(state)).toBe(false);
    });
  });
});
