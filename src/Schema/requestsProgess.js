import { fromJS } from 'immutable';

import {
  eventChannel,
  END,
} from 'redux-saga';

let requests = fromJS({
  prevTotal: 0,
  items: {},
});

export const MIN_PROGRESS_DIFF = 2;

export function createRequestChannel(apiCallFn, apiCallFnArguments) {
  return eventChannel((emitter) => {
    const requestConfig = {
      onUploadProgress: (event) => {
        if (event.lengthComputable) {
          const {
            loaded,
            total,
          } = event;

          emitter({
            loaded,
            total,
          });
        }
      },
    };

    apiCallFn(...apiCallFnArguments, requestConfig)
      .then((response) => {
        emitter({
          response,
        });
      })
      .catch((error) => {
        emitter({
          error,
        });
      })
      .then(() => {
        emitter(END);
      });

    // The subscriber must return an unsubscribe function
    return () => {};
  });
}

export function addPendingRequest(requestUid) {
  const approximateTotal = requests.get('prevTotal') * 1.05;

  requests = requests
    .mergeIn(['items', requestUid], {
      isApproximate: true,
      loaded: 0,
      total: approximateTotal,
    });
}

export function updateRequestProgress(requestUid, loaded, total) {
  const isApproximate = requests.getIn(['items', requestUid, 'isApproximate']);

  if (isApproximate) {
    requests = requests.update('prevTotal', (prevTotal) => prevTotal > total ? prevTotal : total);
  }

  requests = requests.mergeIn(['items', requestUid], {
    loaded,
    total,
    isApproximate: false,
  });
}

export function clearFinishedRequests() {
  requests = requests
    .updateIn(
      ['items'],
      (items) => items.filter((item) => item.get('total') !== item.get('loaded'))
    );
}

export function getTotalRequestsProgress() {
  const loaded = getRequestsLoadedAmount();
  const total = getRequestsTotalAmount();

  return total ? Math.round((100 * loaded) / total) : 100;
}

export function getRequestsLoadedAmount() {
  return requests.get('items').reduce((sum, item) => sum + item.get('loaded'), 0);
}

export function getRequestsTotalAmount() {
  return requests.get('items').reduce((sum, item) => sum + item.get('total'), 0);
}
