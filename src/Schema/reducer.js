/* @flow */

import {
  fromJS,
  List,
  Map,
} from 'immutable';
import _get from 'lodash/get';
import _find from 'lodash/find';
import {
  selectCurrentAnswers,
  selectCurrentIsQuestionMultiple,
  selectNextQuestion,
  makeSelectAnswersTextByIdForQuestion,
  makeSelectQuestionById,
  makeSelectQuestionByQid,
  makeSelectQuestionsRepliesUpToQid,
  makeSelectAnsweredQuestionsUpToQid,
  getQuestions,
  getQuestionReplies,
  getCurrentQuestion,
  getAnsweredQuestions,
  getChatLastQuestionReply,
  calculateVariablesPool,
} from './selectors';
import {
  makeSelectMessagesListUpToFeId,
} from '../Message/selectors';
import {
  addOutgoingMessage,
  addIncomingMessage,
} from '../Message/reducer';
import {
  parseSchema,
  createMessage,
  readQidFromFeId,
} from '../utils';
import type {
  ChatStateType,

  ChatThreadRollbackActionType,

  ChatLoadSchemaSuccessActionType,
  ChatLoadOnboardingSchemaSuccessActionType,

  ChatSelectAnswerActionType,
  ChatUnselectAnswerActionType,
  ChatSetAnswerActionType,
  ChatQuestionReplyType,

  ChatSendQuestionReplyActionType,
  ChatChangeQuestionActionType,
  ChatSchemaQueueFailedActionType,

  ChatAnswerType,
  ChatQuestionType,
  ChatMessageType,
} from '../types';

/* animations */

export const onChatQuestionAnimationStart = (state: ChatStateType, animation: string): ChatStateType => state.mergeDeep({
  animations: {
    question: `${animation}-started`,
  },
});

export const onChatQuestionAnimationEnd = (state: ChatStateType, animation: string): ChatStateType => state.mergeDeep({
  animations: {
    question: `${animation}-ended`,
  },
});

/* schema */

export const onSchemaLoad = (state: ChatStateType): ChatStateType => state.merge({
  loadingSchema: true,
});

export const onSchemaLoadSuccess = (state: ChatStateType, action: ChatLoadSchemaSuccessActionType | ChatLoadOnboardingSchemaSuccessActionType) => {
  const questions = parseSchema(action.payload);
  const {
    code: schemaCode,
    id: schemaId,
    type: schemaType,
  } = action.payload;
  const currentQuestion = { ...questions[0] };

  return state.merge({
    currentQuestion,
    loadingSchema: false,
    questions,
    schemaCode,
    schemaId,
    schemaType,
  });
};

export const onSchemaLoadFailed = (state: ChatStateType) => state.merge({
  loadingSchema: false,
});

export const onChatSchemaQueueStarted = (state: ChatStateType) => state.mergeIn(['schemaQueueStatus'], {
  failureReason: null,
  isEnded: false,
  isFailed: false,
  isStarted: true,
});

export const onChatSchemaQueueEnded = (state: ChatStateType) => state.merge({
  schemaQueueStatus: {
    failureReason: null,
    isEnded: true,
    isFailed: false,
    isStarted: false,
    requestsProgress: 0,
  },
});

export const onChatSchemaQueueFailed = (state: ChatStateType, action: ChatSchemaQueueFailedActionType) => state.merge({
  schemaQueueStatus: {
    failureReason: action.reason,
    isEnded: false,
    isFailed: true,
    isStarted: false,
    requestsProgress: 0,
  },
});

/* answers */

export const onAnswerSelect = (state: ChatStateType, action: ChatSelectAnswerActionType): ChatStateType => mergeCurrentQuestionAnswers(
  state,
  getAnswersWithUpdatedSelected(state, _get(action, 'answer.id'), true)
);

export const onAnswerUnselect = (state: ChatStateType, action: ChatUnselectAnswerActionType): ChatStateType => mergeCurrentQuestionAnswers(
  state,
  getAnswersWithUpdatedSelected(state, _get(action, 'answer.id'), false)
);

// export const onAnswerSet = (state: ChatStateType, action: ChatSetAnswerActionType): ChatStateType => mergeCurrentQuestionAnswers(
//   state,
//   [action.answer]
// );

export const onAnswerSet = (state: ChatStateType, action: ChatSetAnswerActionType): ChatStateType => {
  const currentQuestion = state.get('currentQuestion').toJS();
  const answer = _find(currentQuestion.answers, { type: 'text' });
  if (answer) {
    answer.text = action.answer.text;
    answer.meta = action.answer.meta;
  } else {
    currentQuestion.answers.push(action.answer);
  }

  return state.merge({
    currentQuestion,
  });
};

/* questions */

export const onSendQuestionReply = (state: ChatStateType, action: ChatSendQuestionReplyActionType): ChatStateType => {
  const questionsReplies = addQuestionReply(state, action);
  const newMessage: ChatMessageType = composeMessageFromQuestionReply(state, action.reply);

  const variablesPool: Object = calculateVariablesPool(action.reply.question, action.reply.answers)(state);
  const variablesPoolHistory: List = state.get('variablesPoolHistory').push(variablesPool);
  const currentQuestion = getCurrentQuestion(state);
  const answeredQuestions = getAnsweredQuestions(state)
    .push(currentQuestion);
  let results = {
    questionsReplies,
    answeredQuestions,
    variablesPool,
    variablesPoolHistory,
    showAnimations: true,
  };

  if (newMessage.type !== 'hidden' && _get(newMessage, 'meta.messageType') !== 'hidden') {
    results = {
      ...results,
      showAnimations: true,
      messages: addOutgoingMessage(state, newMessage),
    };
  }

  return state.merge(results);
};

export const addQuestionReply = (state: ChatStateType, action: ChatSendQuestionReplyActionType) => {
  let questionsReplies = getQuestionReplies(state);

  const modifiedAction = fromJS(action).get('reply').setIn(['meta', 'redux'], undefined);

  questionsReplies = questionsReplies.push(
    modifiedAction
  );

  return questionsReplies;
};

export const getNextQuestion = (state: ChatStateType) => {
  let nextQuestion = selectNextQuestion(state);

  if (nextQuestion && nextQuestion.get) {
    nextQuestion = nextQuestion.get(0);
  }

  return nextQuestion;
};

export const composeMessageFromQuestionReply = (state: ChatStateType, reply: ChatQuestionReplyType) => {
  const question = makeSelectQuestionById(reply.question)(state);
  const modifiedReply = fromJS(reply).setIn(['meta', 'redux'], undefined).toJS();
  const questionReply: ChatQuestionReplyType = {
    ...modifiedReply,
    isOwn: true,
    qid: question.get('qid'),
  };

  let answersText = [questionReply.text];

  // Answers in reply are only ID's but text is required to show it in chat immediately
  if (questionReply.answers && questionReply.answers.length) {
    answersText = makeSelectAnswersTextByIdForQuestion(
      questionReply.answers,
      questionReply.question
    )(state);
  }

  questionReply.text = answersText ? answersText.join(', ') : '';

  // convert reply to expected message format
  const message: ChatMessageType = createMessage(questionReply);

  return message;
};

/* current question */

export const onChangeQuestion = (state: ChatStateType, action: ChatChangeQuestionActionType) => state.merge({
  currentQuestion: action.question,
});

export const mergeCurrentQuestionAnswers = (state: ChatStateType, answers: Array<ChatAnswerType>): ChatStateType => state.mergeIn(
  ['currentQuestion', 'answers'],
  answers,
);

export const getAnswersWithUpdatedSelected = (state: ChatStateType, answerId: number, selected: boolean): Array<ChatAnswerType> => {
  const isMultiple = selectCurrentIsQuestionMultiple(state);
  let answers = selectCurrentAnswers(state) || [];

  answers = answers.map(
    (answer) => {
      if (answer.get('id') === answerId) {
        return answer.set('isSelected', selected);
      } else if (selected && (!isMultiple || answer.get('exclusive'))) {
        return answer.set('isSelected', false);
      }

      return answer;
    }
  );

  return answers;
};

/* next question */

export const onNextQuestionAction = (state: ChatStateType) => {
  const currentQuestionFromState = state.get('currentQuestion');
  const nextQuestion = getNextQuestion(state);

  // if current question is the last in the flow and already has been answered, clear it!
  if (currentQuestionFromState.get('nextQid') === -1) {
    const lastQuestionReply = getChatLastQuestionReply(state);
    // TODO: MM: probably if the last question has no answers, it should also pass
    if (lastQuestionReply.question === currentQuestionFromState.get('id')) {
      return state.merge({
        currentQuestion: {
          answers: [],
        },
      });
    }
  }

  // TODO: PP: figure out how is it possible for
  // currentQuestion to be undefined here
  if (!nextQuestion) {
    return state;
  }

  let results = {
    previousQuestion: currentQuestionFromState,
    currentQuestion: nextQuestion,
  };

  const newMessage: ChatMessageType = composeMessageFromQuestion(
    nextQuestion.toJS()
  );

  if (newMessage.type !== 'hidden' && _get(newMessage, 'meta.messageType') !== 'hidden') {
    results = {
      ...results,
      messages: addIncomingMessage(state, newMessage),
    };
  }

  return state.merge(results);
};

export const composeMessageFromQuestion = (question: ChatQuestionType): ChatMessageType => {
  const message = createMessage({
    ...question,
    question: question.id,
  });

  return message;
};

/* rollback question */

export const onThreadRollbackQuestionAction = (state: ChatStateType, action: ChatThreadRollbackActionType) => {
  const qid = readQidFromFeId(action.feId);
  // @TODO: !!! WARNING !!!
  // variables pool history works only one step back while this function genuinely
  // allow to rollback to any point in history
  // it is fine for current way we use it but this need to be fixed in the future
  // https://ydtechnology.atlassian.net/browse/ARD-474
  const variablesPoolHistory = state.get('variablesPoolHistory').pop();
  const variablesPool = variablesPoolHistory.last() || new Map();
  let messages = new List();
  let questionsReplies = new List();
  let answeredQuestions = new List();
  let currentQuestion;
  const previousQuestion = state.get('currentQuestion');

  if (action.feId) {
    messages = makeSelectMessagesListUpToFeId(action.feId)(state);
  }

  if (qid) {
    questionsReplies = makeSelectQuestionsRepliesUpToQid(qid)(state).pop();
    answeredQuestions = makeSelectAnsweredQuestionsUpToQid(qid)(state);
    currentQuestion = answeredQuestions.size ? answeredQuestions.last() : makeSelectQuestionByQid(qid)(state);
  } else {
    currentQuestion = getQuestions(state).get(0);
  }

  return state.merge({
    answeredQuestions: answeredQuestions.pop(),
    currentQuestion,
    messages,
    previousQuestion,
    questionsReplies,
    variablesPool,
    variablesPoolHistory,
  });
};

export const onChatSchemaQueueSetRequestsProgressAction = (state, action) => state.setIn(['schemaQueueStatus', 'requestsProgress'], action.requestsProgress);
