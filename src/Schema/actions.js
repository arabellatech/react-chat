/* @flow */

import {
  CHAT_SET_THREAD_ID_ACTION,

  CHAT_THREAD_ROLLBACK_ACTION,
  CHAT_THREAD_ROLLBACK_SUCCESS_ACTION,
  CHAT_THREAD_ROLLBACK_FAILED_ACTION,

  CHAT_LOAD_SCHEMA_ACTION,
  CHAT_LOAD_SCHEMA_SUCCESS_ACTION,
  CHAT_LOAD_SCHEMA_FAILED_ACTION,

  CHAT_LOAD_ONBOARDING_SCHEMA_ACTION,
  CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION,
  CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION,

  CHAT_SELECT_ANSWER_ACTION,
  CHAT_UNSELECT_ANSWER_ACTION,
  CHAT_SET_ANSWER_ACTION,

  CHAT_SEND_QUESTION_REPLY_ACTION,
  CHAT_SEND_QUESTION_REPLY_SUCCESS_ACTION,
  CHAT_SEND_QUESTION_REPLY_FAILED_ACTION,

  CHAT_CHANGE_QUESTION_ACTION,
  CHAT_NEXT_QUESTION_ACTION,

  CHAT_QUESTION_ANIMATION_START,
  CHAT_QUESTION_ANIMATION_END,

  CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION,
  CHAT_SCHEMA_QUEUE_STARTED_ACTION,
  CHAT_SCHEMA_QUEUE_ENDED_ACTION,
  CHAT_SCHEMA_QUEUE_FAILED_ACTION,
  CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION,
  CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION,
} from './constants';

import type {
  ChatSetThreadIdActionType,
  ChatThreadRollbackActionType,
  ChatThreadRollbackSuccessActionType,
  ChatThreadRollbackFailedActionType,

  ChatLoadSchemaActionType,
  ChatLoadSchemaSuccessActionType,
  ChatLoadSchemaFailedActionType,

  ChatLoadOnboardingSchemaActionType,
  ChatLoadOnboardingSchemaSuccessActionType,
  ChatLoadOnboardingSchemaFailedActionType,

  ChatSelectAnswerActionType,
  ChatUnselectAnswerActionType,
  ChatSetAnswerActionType,

  ChatSendQuestionReplyActionType,
  ChatSendQuestionReplySuccessActionType,
  ChatSendQuestionReplyFailedActionType,

  ChatChangeQuestionActionType,
  ChatNextQuestionActionType,

  ChatQuestionType,
  ChatQuestionReplyType,
  ChatAnswerType,

  ChatQuestionAnimationStartActionType,
  ChatQuestionAnimationEndActionType,

  ChatSchemaQueueStartedActionType,
  ChatSchemaQueueEndedActionType,
  ChatSchemaQueueFailedActionType,
  ChatSchemaQueueFailedReasonType,
} from '../types';

/* thread */

export const chatSetThreadIdAction = (id: number): ChatSetThreadIdActionType => ({
  type: CHAT_SET_THREAD_ID_ACTION,
  id,
});

export const chatThreadRollbackAction = (threadId: number, feId?: string, questionId?: number): ChatThreadRollbackActionType => ({
  type: CHAT_THREAD_ROLLBACK_ACTION,
  threadId,
  feId,
  questionId,
  requestUid: new Date().getTime(),
});

export const chatThreadRollbackSuccessAction = (payload: Object): ChatThreadRollbackSuccessActionType => ({
  type: CHAT_THREAD_ROLLBACK_SUCCESS_ACTION,
  payload,
});

export const chatThreadRollbackFailedAction = (payload: Object): ChatThreadRollbackFailedActionType => ({
  type: CHAT_THREAD_ROLLBACK_FAILED_ACTION,
  payload,
});

/* schema */

export const chatLoadSchemaAction = (id: number): ChatLoadSchemaActionType => ({
  type: CHAT_LOAD_SCHEMA_ACTION,
  id,
});

export const chatLoadSchemaSuccessAction = (payload: Object): ChatLoadSchemaSuccessActionType => ({
  type: CHAT_LOAD_SCHEMA_SUCCESS_ACTION,
  payload,
});

export const chatLoadSchemaFailedAction = (payload: Object): ChatLoadSchemaFailedActionType => ({
  type: CHAT_LOAD_SCHEMA_FAILED_ACTION,
  payload,
});

export const chatLoadOnboardingSchemaAction = (): ChatLoadOnboardingSchemaActionType => ({
  type: CHAT_LOAD_ONBOARDING_SCHEMA_ACTION,
});

export const chatLoadOnboardingSchemaSuccessAction = (payload: Object): ChatLoadOnboardingSchemaSuccessActionType => ({
  type: CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION,
  payload,
});

export const chatLoadOnboardingSchemaFailedAction = (payload: Object): ChatLoadOnboardingSchemaFailedActionType => ({
  type: CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION,
  payload,
});

export const chatSchemaPutRequestToQueue = (requestAction) => ({
  type: CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION,
  requestAction
});

export const chatSchemaQueueStartedAction = (): ChatSchemaQueueStartedActionType => ({
  type: CHAT_SCHEMA_QUEUE_STARTED_ACTION,
});

export const chatSchemaQueueEndedAction = (): ChatSchemaQueueEndedActionType => ({
  type: CHAT_SCHEMA_QUEUE_ENDED_ACTION,
});

export const chatSchemaQueueFailedAction = (reason: ChatSchemaQueueFailedReasonType, payload?: Object): ChatSchemaQueueFailedActionType => ({
  type: CHAT_SCHEMA_QUEUE_FAILED_ACTION,
  reason,
  payload,
});

export const chatSchemaQueueSetRequestsProgressAction = (requestsProgress) => ({
  type: CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION,
  requestsProgress,
});

export const chatSchemaQueueClearFinishedRequestsFromProgressAction = () => ({
  type: CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION,
});

/* answers */

export function chatSelectAnswerAction(answer: ChatAnswerType): ChatSelectAnswerActionType {
  return {
    type: CHAT_SELECT_ANSWER_ACTION,
    answer,
  };
}

export function chatUnselectAnswerAction(answer: ChatAnswerType): ChatUnselectAnswerActionType {
  return {
    type: CHAT_UNSELECT_ANSWER_ACTION,
    answer,
  };
}

export function chatSetAnswerAction(answer: ChatAnswerType): ChatSetAnswerActionType {
  return {
    type: CHAT_SET_ANSWER_ACTION,
    answer,
  };
}

/* current question */

export function chatSendQuestionReplyAction(reply: ChatQuestionReplyType): ChatSendQuestionReplyActionType {
  return {
    type: CHAT_SEND_QUESTION_REPLY_ACTION,
    reply,
    requestUid: new Date().getTime(),
  };
}

export function chatSendQuestionReplySuccessAction(payload: Object): ChatSendQuestionReplySuccessActionType {
  return {
    type: CHAT_SEND_QUESTION_REPLY_SUCCESS_ACTION,
    payload,
  };
}

export function chatSendQuestionReplyFailedAction(payload: Object): ChatSendQuestionReplyFailedActionType {
  return {
    type: CHAT_SEND_QUESTION_REPLY_FAILED_ACTION,
    payload,
  };
}

export function chatChangeQuestionAction(question: ChatQuestionType): ChatChangeQuestionActionType {
  return {
    type: CHAT_CHANGE_QUESTION_ACTION,
    question,
  };
}

export function chatNextQuestionAction(): ChatNextQuestionActionType {
  return {
    type: CHAT_NEXT_QUESTION_ACTION,
  };
}

/* animations */

export const chatQuestionAnimationStartAction = (animation: string): ChatQuestionAnimationStartActionType => ({
  type: CHAT_QUESTION_ANIMATION_START,
  animation,
});

export const chatQuestionAnimationEndAction = (animation: string): ChatQuestionAnimationEndActionType => ({
  type: CHAT_QUESTION_ANIMATION_END,
  animation,
});
