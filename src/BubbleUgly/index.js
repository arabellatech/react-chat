import React, {
  Component,
} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _bindAll from 'lodash/bindAll';
import _isFunction from 'lodash/isFunction';
import fecha from 'fecha';
import EditBtnDesktop from './EditBtnDesktop';
import EditBtnMobile from './EditBtnMobile';
import MarchingAnts from '../MarchingAnts';
import {
  chatBubbleAnimationStartAction,
  chatBubbleAnimationEndAction,
} from '../Message/actions';

const collapseTimeNormal = 400;
const collapseMediaTimeNormal = 500;
const loadingTimeNormal = 1200;
const timeMultiplier = 0.9;
const collapseTime = timeMultiplier * collapseTimeNormal;
const collapseMediaTime = timeMultiplier * collapseMediaTimeNormal;
const loadingTime = timeMultiplier * loadingTimeNormal;

const mapDispatchToProps = {
  animationStart: chatBubbleAnimationStartAction,
  animationEnd: chatBubbleAnimationEndAction,
};

@connect(null, mapDispatchToProps)
export default class BubbleUgly extends Component {
  static propTypes = {
    animationEnd: PropTypes.func.isRequired,
    animationStart: PropTypes.func.isRequired,
    children: PropTypes.any,
    feId: PropTypes.string.isRequired,
    mediaUrl: PropTypes.any,
    message: PropTypes.object.isRequired,
    messagesGroup: PropTypes.object,
    onEditClick: PropTypes.func,
    questionId: PropTypes.number,
    renderAdditionalMessageContent: PropTypes.func,
    renderMessageFooter: PropTypes.func.isRequired,
    renderTimestamp: PropTypes.func,
    showAnimations: PropTypes.bool,
    showMarchingAnts: PropTypes.bool,
    time: PropTypes.string,
    timeFormat: PropTypes.string,
  };

  static defaultProps = {
    children: undefined,
    feId: '',
    mediaUrl: undefined,
    messagesGroup: {},
    onEditClick: null,
    questionId: null,
    renderAdditionalMessageContent: undefined,
    renderTimestamp: undefined,
    showAnimations: false,
    showMarchingAnts: undefined,
    time: null,
    timeFormat: null,
  };

  constructor(props) {
    super(props);

    this.animationsTimers = {};
  }

  state = {
    loading: false,
    collapse: false,
    collapsePhase: '',
    showEditBtn: false,
  };

  componentDidMount() {
    if (this.props.showAnimations) {
      this.props.animationStart();

      // CASE 1 - no typing indicator
      this.answerAnimation();

      // CASE 2 - typing indicator
      this.botAnimation();

      // CASE 3 - media animation
      this.mediaAnimation();
    }
  }

  componentWillUnmount() {
    Object
      .values(this.animationsTimers)
      .map(clearTimeout);
  }

  onLastAnimationStart(duration = 1000) {
    const { animationEnd } = this.props;

    this.animationsTimers.animation1 = setTimeout(
      animationEnd,
      duration
    );
  }

  handleEditClick = (event) => {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    const {
      onEditClick,
      feId,
      questionId,
    } = this.props;

    onEditClick(feId, questionId);
  }

  handleBubbleClick = () => {
    this.toggleShowEditBtn();
  }

  setBubbleHeight(callback) {
    this.bubbleWrap.style.position = 'absolute';
    this.bubbleWrap.style.visibility = 'hidden';
    const bubbleHeight = this.bubble.offsetHeight;
    this.bubble.style.height = `${bubbleHeight}px`;
    this.bubbleWrap.style.position = 'relative';
    this.bubbleWrap.style.visibility = 'visible';
    if (callback) {
      callback();
    }
  }

  restoreDefaultBubbleHeight() {
    if (this.bubble) {
      this.bubble.style.height = 'auto';
    }
  }

  mediaAnimation() {
    if (this.props.mediaUrl) {
      // First phase - animate height
      this.setState({
        collapse: true,
      });

      // Second phase - show the media bubble
      this.animationsTimers.animation2 = setTimeout(() => {
        this.setState({
          collapse: false,
        });

        this.onLastAnimationStart();
      }, collapseMediaTime);
    }
  }

  answerAnimation() {
    if (!this.props.showMarchingAnts && !this.props.mediaUrl) {
      // Set bubble height needed for the animation
      this.setBubbleHeight(() => {
        // Start the animation
        this.setState({
          collapse: true,
          collapsePhase: 'third',
        });

        // finish the animation
        this.animationsTimers.animation3 = setTimeout(() => {
          this.setState({
            collapse: false,
          });
          this.restoreDefaultBubbleHeight();
          this.onLastAnimationStart();
        }, collapseTime);
      });
    }
  }

  botAnimation() {
    if (this.props.showMarchingAnts && !this.props.mediaUrl) {
      this.setState({
        collapse: true,
        collapsePhase: 'first',
      });

      // Second phase - show the loading animation for $loadingTime
      this.animationsTimers.animation4 = setTimeout(() => {
        // But first set the container height to minimum 48px(loading bubble height) to prevent jumpy animation
        this.bubbleOuterWrap.style.minHeight = '48px';
        this.setState({
          loading: true,
          collapse: false,
          collapsePhase: '',
        });
      }, collapseTime);

      // Third phase - hide the loading animation and toggle height to text bubble size
      this.animationsTimers.animation5 = setTimeout(() => {
        // hide loading animation
        this.setState({
          loading: false,
        });
        // toggle height to text bubble size
        this.setBubbleHeight(() => {
          this.setState({
            collapse: true,
            collapsePhase: 'second',
          });
        });
      }, collapseTime + loadingTime);
      // Fifth phase - display text bubble
      this.animationsTimers.animation6 = setTimeout(() => {
        this.setState({
          collapse: false,
          collapsePhase: '',
        });
        this.restoreDefaultBubbleHeight();
        this.onLastAnimationStart();
      }, (collapseTime * 2) + loadingTime);
    }
  }

  toggleShowEditBtn() {
    const showEditBtn = this.state.showEditBtn;

    this.setState({
      showEditBtn: !showEditBtn,
    });
  }

  renderTimestamp() {
    const {
      time,
      timeFormat,
      renderTimestamp,
      messagesGroup,
    } = this.props;

    if (renderTimestamp) {
      const timestampProps = {
        time,
        timeFormat,
        messagesGroup,
      };

      return renderTimestamp(timestampProps);
    }

    if (!time || !timeFormat) {
      return null;
    }

    const timeFormatted = fecha.format(time, timeFormat);

    return (
      <div className="chat-msg-timestamp">
        {timeFormatted}
      </div>
    );
  }

  renderBubble() {
    const {
      children,
      message,
      messagesGroup,
      renderAdditionalMessageContent,
      renderMessageFooter,
      showMarchingAnts,
    } = this.props;
    const {
      collapse,
      collapsePhase,
      loading,
    } = this.state;
    const editButtons = this.renderEditButtons();
    const isLoading = loading && !collapse;
    const isLoaded = ((showMarchingAnts && !loading) || !showMarchingAnts) && !collapse;
    const collapseClass = collapse ? `chat-msg-bubble--collapse chat-msg-bubble--collapse_${collapsePhase}` : '';
    let content;

    if ((loading && showMarchingAnts) || collapsePhase === 'first') {
      content = <MarchingAnts />;
    } else {
      content = (
        <div className="chat-msg-text">
          <span dangerouslySetInnerHTML={{ __html: children }}></span>
          {renderAdditionalMessageContent && renderAdditionalMessageContent(message)}
        </div>
      );
    }

    return (
      <div
        className="chat-msg-bubble-outer_wrap"
        ref={(bubbleOuterWrap) => { this.bubbleOuterWrap = bubbleOuterWrap; }}
      >
        <div
          className="chat-msg-bubble-wrap"
          ref={(bubbleWrap) => { this.bubbleWrap = bubbleWrap; }}
        >
          {editButtons}
          <div
            className={`chat-msg-bubble ${isLoading ? 'chat-msg-bubble--loading' : ''} ${isLoaded ? 'chat-msg-bubble--loaded' : ''} ${collapseClass}`}
            ref={(bubble) => { this.bubble = bubble; }}
            onClick={this.handleBubbleClick}
            role="link"
            tabIndex={0}
          >
            <div className="chat-msg-bubble-inner">
              <div className="chat-msg-bubble-bg"></div>
              {content}
            </div>
          </div>
          {renderMessageFooter()}
          {this.renderTimestamp()}
        </div>
      </div>
    );
  }

  renderEditButtons() {
    const {
      onEditClick,
    } = this.props;
    const canEdit = _isFunction(onEditClick);

    if (!canEdit) {
      return null;
    }

    return [
      <EditBtnDesktop
        show
        onEditClick={this.handleEditClick}
      />,
      <EditBtnMobile
        show={this.state.showEditBtn}
        onEditClick={this.handleEditClick}
      />,
    ];
  }

  renderMediaBubble() {
    const isLoading = this.props.showAnimations && this.state.loading && !this.state.collapse;
    const isLoaded = (!this.props.showAnimations || (this.props.showMarchingAnts && !this.state.loading) || !this.props.showMarchingAnts) && !this.state.collapse;
    const collapseClass = this.state.collapse ? 'chat-msg-media--collapse' : '';

    return (
      <div className={`chat-msg-media ${isLoading ? 'chat-msg-media--loading' : ''} ${isLoaded ? 'chat-msg-media--loaded' : ''} ${collapseClass}`}>
        <div className="chat-msg-media-thumb">
          <img src={this.props.mediaUrl} alt="" className="chat-msg-media-thumb-img" />
        </div>
        <div className="chat-msg-media-bubble">
          <div className="chat-msg-media-bubble-bg"></div>
          <div className="chat-msg-text">
            <div className="chat-msg-text">{this.props.children}</div>
          </div>
          {this.renderTimestamp()}
        </div>
      </div>
    );
  }

  render() {
    const bubble = this.props.mediaUrl ? this.renderMediaBubble() : this.renderBubble();

    return bubble;
  }
}
