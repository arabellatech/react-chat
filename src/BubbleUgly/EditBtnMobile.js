/* @flow */

import React from 'react';

type EditBtnMobileProps = {
  onEditClick: () => void,
  show: boolean,
}

const EditBtnMobile = (props: EditBtnMobileProps) => {
  if (!props.show) {
    return null;
  }

  return (
    <button
      className="chat-change_answer chat-change_answer--visible hidden-desktop"
      onClick={props.onEditClick}
    >
      Change answer?
    </button>
  );
};

export default EditBtnMobile;
