/* @flow */

import React from 'react';

type EditBtnDesktopProps = {
  onEditClick: () => void,
  show: boolean,
}

const EditBtnDesktop = (props: EditBtnDesktopProps) => {
  if (!props.show) {
    return null;
  }

  return (
    <button
      className="chat-edit"
      onClick={props.onEditClick}
    >
      <span className="chat-edit-icon">
        <i className="icon-edit"></i>
      </span>
      <span className="chat-edit-text">EDIT</span>
    </button>
  );
};

export default EditBtnDesktop;
