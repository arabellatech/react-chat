/* @flow */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import _bindAll from 'lodash/bindAll';
import {
  CHAT_MESSAGE_TYPE_TEXT,
} from '../Message/constants';
import {
  chatSendMessageAndOpenThreadAction,
  chatSendMessageAndWaitForResponseAction,
  chatSendMessageAndCloseThreadAction,
  chatSendMessageAction,
} from '../Message/actions';
import {
  chatThreadCloseAction,
  chatThreadOpenAction,
  chatThreadWaitForResponseAction,
} from '../Thread/actions';
import type {
  ChatMessageFormProps,
  ChatMessageType,
} from '../types';

type ChatMessageFormStateType = {
  text: string,
}

type SendMessageOptionsType = {
  closeThread: boolean,
}

class MessageForm extends PureComponent {
  constructor(props) {
    super(props);

    const { threadId } = props;

    if (!threadId) {
      throw Error('props.threadId is required!');
    }

    this.state = {
      text: '',
    };

    _bindAll(this, [
      'onSubmit',
      'setText',
    ]);
  }

  state: ChatMessageFormStateType

  onSubmit(event: Event, options?: SendMessageOptionsType) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    if (this.state.text.length > 0) {
      this.sendMessage(options);
      this.setText('');
    }

    if (!this.state.text.length && options) {
      this.updateThreadStatus(options);
    }
  }

  setText(text: string) {
    this.setState({
      text,
    });
  }

  updateThreadStatus(options?: SendMessageOptionsType) {
    const {
      waitForResponse,
      openThread,
      closeThread,
      threadId,
    } = this.props;

    if (options && options.waitForResponse) {
      waitForResponse(threadId);
    } else if (options && options.openThread) {
      openThread(threadId);
    } else if (options && options.closeThread) {
      closeThread(threadId);
    }
  }

  sendMessage(options?: SendMessageOptionsType) {
    const message: ChatMessageType = this.composeMessage();
    const {
      sendMessage,
      sendMessageAndWaitForResponse,
      sendMessageAndOpenThread,
      sendMessageAndCloseThread,
    } = this.props;

    if (options && options.waitForResponse) {
      sendMessageAndWaitForResponse(message);
    } else if (options && options.openThread) {
      sendMessageAndOpenThread(message);
    } else if (options && options.closeThread) {
      sendMessageAndCloseThread(message);
    } else {
      sendMessage(message);
    }
  }

  composeMessage() {
    const { threadId } = this.props;
    const message = {
      threadId,
      text: this.state.text,
      type: CHAT_MESSAGE_TYPE_TEXT,
    };

    return message;
  }

  composeProps() {
    const {
      threadId,
      ...parentProps
    } = this.props;
    const props = {
      ...parentProps,
      onSubmit: this.onSubmit,
      setText: this.setText,
      text: this.state.text,
    };

    return props;
  }

  props: ChatMessageFormProps;

  render() {
    const props = this.composeProps();
    const Component = this.props.component;

    return (
      <Component {...props} />
    );
  }
}

// $FlowFix - flow doesn't like null in mapStateToProps
const mapStateToProps = () => ({});

const mapDispatchToProps = {
  sendMessage: chatSendMessageAction,
  closeThread: chatThreadCloseAction,
  openThread: chatThreadOpenAction,
  waitForResponse: chatThreadWaitForResponseAction,
  sendMessageAndCloseThread: chatSendMessageAndCloseThreadAction,
  sendMessageAndOpenThread: chatSendMessageAndOpenThreadAction,
  sendMessageAndWaitForResponse: chatSendMessageAndWaitForResponseAction,
};

// override state and dispatch props with own props
const mergeProps = (stateProps?, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(MessageForm);
