/* @flow */

import React from 'react';
import {
  mountWithStore,
} from 'react-unit-testing-utils';
import {
  CHAT_MESSAGE_TYPE_TEXT,
} from '../Message/constants';
import MessageForm from './';

describe('MessageForm', () => {
  let root;
  let wrapper;
  const ExampleComponent = () => <form id="exampleComponent"></form>;
  const defaultProps = {
    threadId: 1,
    component: ExampleComponent,
  };
  const shallowWithProps = (props) => {
    root = mountWithStore(
      <MessageForm {...props} />,
      {}
    );
    wrapper = root.find(ExampleComponent);
  };
  const getProps = () => wrapper.props();

  describe('passed props', () => {
    it('should throw error when props.threadId is not defined', () => {
      expect(() => {
        shallowWithProps({});
      }).toThrowError();
    });

    // TODO figure out how to check if sendMessage triggers chatSendMessageReplyAction
    it('should pass sendMessage in props', () => {
      shallowWithProps(defaultProps);

      expect(getProps().sendMessage).toBeDefined();
    });

    it('should pass state.text as props.text', () => {
      shallowWithProps(defaultProps);

      expect(getProps().text).toEqual('');
    });
  });

  describe('props.onSubmit', () => {
    const sendMessage = jest.fn();

    beforeEach(() => {
      sendMessage.mockReset();

      shallowWithProps({
        ...defaultProps,
        sendMessage,
      });

      getProps().setText('ANSWER_TEXT');
    });

    it('should call sendMessage with {text:"ANSWER_TEXT", threadId:1, type:message}', () => {
      const expected = {
        text: 'ANSWER_TEXT',
        threadId: 1,
        type: CHAT_MESSAGE_TYPE_TEXT,
      };

      getProps().onSubmit();

      expect(sendMessage).toHaveBeenCalledWith(expected);
    });

    it('should clear props.text', () => {
      expect(getProps().text).toEqual('ANSWER_TEXT');

      getProps().onSubmit();

      expect(getProps().text).toEqual('');
    });
  });
});
