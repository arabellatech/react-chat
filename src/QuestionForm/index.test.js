/* @flow */

import React from 'react';
import {
  mountWithStore,
} from 'react-unit-testing-utils';
import type {
  ChatQuestionType,
  ChatAnswerType,
} from '../types';
import QuestionForm from './';

describe('QuestionForm', () => {
  let wrapper;
  const answer110: ChatAnswerType = {
    id: 110,
    nextQid: 40,
    text: 'ANSWER_ONE_TEXT',
    value: 'ANSWER_ONE_VALUE',
  };
  const answer110Selected: ChatAnswerType = {
    ...answer110,
    isSelected: true,
  };
  const answer120: ChatAnswerType = {
    id: 120,
    nextQid: 50,
    text: 'ANSWER_TWO_TEXT',
    value: 'ANSWER_TWO_VALUE',
  };
  const answer120Selected: ChatAnswerType = {
    ...answer120,
    isSelected: true,
  };
  const answer130: ChatAnswerType = {
    id: 130,
    nextQid: 60,
    text: 'ANSWER_THREE_TEXT',
    value: 'ANSWER_THREE_VALUE',
  };
  const currentQuestion: ChatQuestionType = {
    qid: 10,
    id: 20,
    nextQid: 30,
    text: 'QUESTION_TEXT',
    answers: [
      answer110,
      answer120,
    ],
    type: 'message',
  };
  const ExampleComponent = () => <form id="exampleComponent"></form>;
  const defaultProps = {
    component: ExampleComponent,
    type: 'message',
  };
  const getStateWithCurrentQuestion = (question: ChatQuestionType = currentQuestion) => ({
    chat: {
      currentQuestion: question,
      answeredQuestions: [],
      animations: {},
    },
  });
  const shallowWithProps = (props, state) => {
    wrapper = mountWithStore(
      <QuestionForm {...props} />,
      state,
    ).find(ExampleComponent);
  };
  const getProps = () => wrapper.props();

  describe('rendering', () => {
    it('should throw errors when props.type is not defined', () => {
      expect(() => {
        shallowWithProps(
          {
            component: ExampleComponent,
          },
          getStateWithCurrentQuestion()
        );
      }).toThrowError('props.type must be defined!');
    });

    it('should throw errors when props.component is not defined', () => {
      expect(() => {
        shallowWithProps(
          {
            type: 'message',
          },
          getStateWithCurrentQuestion()
        );
      }).toThrowError('props.component must be defined!');
    });

    it('should render component if props.type match props.getType', () => {
      shallowWithProps(
        {
          ...defaultProps,
          getType: () => 'choice',
          type: 'choice',
        },
        getStateWithCurrentQuestion()
      );

      expect(wrapper.length).toBe(1);
    });

    it('should not render component if props.type does not match props.getType', () => {
      shallowWithProps(
        {
          ...defaultProps,
          getType: () => 'message',
          type: 'choice',
        },
        getStateWithCurrentQuestion()
      );

      expect(wrapper.length).toBe(0);
    });

    it('should render component if props.type match currentQuestion meta type', () => {
      shallowWithProps(
        {
          ...defaultProps,
          type: 'choice',
        },
        getStateWithCurrentQuestion({
          ...currentQuestion,
          meta: {
            type: 'choice',
          },
        })
      );

      expect(wrapper.length).toBe(1);
    });

    it('should not render component if props.type does not match currentQuestion meta type', () => {
      shallowWithProps(
        {
          ...defaultProps,
          type: 'choice',
        },
        getStateWithCurrentQuestion({
          ...currentQuestion,
          meta: {
            type: 'message',
          },
        })
      );

      expect(wrapper.length).toBe(0);
    });

    it('should render component if props.type match currentQuestion type', () => {
      shallowWithProps(
        {
          ...defaultProps,
          type: 'choice',
        },
        getStateWithCurrentQuestion({
          ...currentQuestion,
          type: 'choice',
        })
      );

      expect(wrapper.length).toBe(1);
    });

    it('should not render component if props.type does not match currentQuestion type', () => {
      shallowWithProps(
        {
          ...defaultProps,
          type: 'choice',
        },
        getStateWithCurrentQuestion({
          ...currentQuestion,
          type: 'message',
        })
      );

      expect(wrapper.length).toBe(0);
    });
  });

  describe('passed props', () => {
    beforeEach(() => {
      shallowWithProps(defaultProps, getStateWithCurrentQuestion());
    });

    it('should have question in props', () => {
      expect(getProps().question).toEqual(currentQuestion);
    });

    it('should have onSubmit in props', () => {
      expect(getProps().onSubmit).toBeDefined();
    });

    // TODO figure out how to check if selectAnswer triggers chatSelectAnswerAction
    it('should pass selectAnswer in props', () => {
      expect(getProps().selectAnswer).toBeDefined();
    });

    // TODO figure out how to check if unelectAnswer triggers chatUnselectAnswerAction
    it('should pass unselectAnswer in props', () => {
      expect(getProps().unselectAnswer).toBeDefined();
    });

    // TODO figure out how to check if setAnswer triggers chatSetAnswerAction
    it('should pass setAnswer in props', () => {
      expect(getProps().setAnswer).toBeDefined();
    });

    // TODO figure out how to check if sendQuestionReply triggers chatSendQuestionReplyAction
    it('should pass sendQuestionReply in props', () => {
      expect(getProps().sendQuestionReply).toBeDefined();
    });

    it('should pass answeredQuestions in props', () => {
      expect(getProps().answeredQuestions).toBeDefined();
    });
  });

  describe('props.getAnswerById', () => {
    const getAnswerById = (id?: number) => getProps().getAnswerById(id);

    beforeEach(() => {
      shallowWithProps(defaultProps, getStateWithCurrentQuestion());
    });

    it('should return null when called with non existing id', () => {
      expect(getAnswerById(65)).toBe(null);
    });

    it('should return answer mathing passed id', () => {
      expect(getAnswerById(110)).toEqual(answer110);
      expect(getAnswerById(120)).toEqual(answer120);
    });
  });

  describe('props.getSelectedAnswers', () => {
    const getSelectedAnswers = () => getProps().getSelectedAnswers();

    it('should return empty array when no answer is selected', () => {
      shallowWithProps(defaultProps, getStateWithCurrentQuestion());

      expect(getSelectedAnswers()).toEqual([]);
    });

    it('should return answer110 when it has isSelected set to true', () => {
      const question: ChatQuestionType = {
        ...currentQuestion,
        answers: [
          answer110Selected,
          answer120,
        ],
      };
      shallowWithProps(defaultProps, getStateWithCurrentQuestion(question));

      expect(getSelectedAnswers()).toEqual([answer110Selected]);
    });
  });

  describe('props.onSubmit', () => {
    const sendQuestionReply = jest.fn();
    const getPropsForType = (type) => ({
      ...defaultProps,
      sendQuestionReply,
      threadId: 1,
      type,
    });

    beforeEach(() => {
      sendQuestionReply.mockReset();
    });

    it('should call sendQuestionReply with {answers:[110], question:20, threadId:1, type, meta} when question type:choice', () => {
      const question: ChatQuestionType = {
        ...currentQuestion,
        type: 'choice',
        answers: [
          answer110Selected,
          answer120,
        ],
      };
      const expected = {
        question: 20,
        answers: [110],
        threadId: 1,
        type: 'message',
        meta: {},
      };

      shallowWithProps(getPropsForType('choice'), getStateWithCurrentQuestion(question));

      getProps().onSubmit();

      expect(sendQuestionReply).toHaveBeenCalledWith(expected);
    });

    it('should call sendQuestionReply with {answers:[110, 120], question:20, threadId:1, type, meta} when question type:multichoice', () => {
      const question: ChatQuestionType = {
        ...currentQuestion,
        type: 'multichoice',
        answers: [
          answer110Selected,
          answer120Selected,
        ],
      };
      const expected = {
        question: 20,
        answers: [110, 120],
        threadId: 1,
        type: 'message',
        meta: {},
      };

      shallowWithProps(getPropsForType('multichoice'), getStateWithCurrentQuestion(question));

      getProps().onSubmit();

      expect(sendQuestionReply).toHaveBeenCalledWith(expected);
    });

    it('should call sendQuestionReply with {text:"ANSWER_THREE_TEXT", question:20, threadId:1, type, meta} when question type:text', () => {
      const question: ChatQuestionType = {
        ...currentQuestion,
        type: 'text',
        answers: [
          answer130,
        ],
      };
      const expected = {
        question: 20,
        text: 'ANSWER_THREE_TEXT',
        threadId: 1,
        type: 'message',
        meta: {},
      };

      shallowWithProps(getPropsForType('text'), getStateWithCurrentQuestion(question));

      getProps().onSubmit();

      expect(sendQuestionReply).toHaveBeenCalledWith(expected);
    });

    it('should call sendQuestionReply with {text:"ANSWER_THREE_TEXT", question:20, threadId:1, type, meta} when question type:message', () => {
      const question: ChatQuestionType = {
        ...currentQuestion,
        type: 'message',
        answers: [
          answer130,
        ],
      };
      const expected = {
        question: 20,
        text: 'ANSWER_THREE_TEXT',
        threadId: 1,
        type: 'message',
        meta: {},
      };

      shallowWithProps(getPropsForType('message'), getStateWithCurrentQuestion(question));

      getProps().onSubmit();

      expect(sendQuestionReply).toHaveBeenCalledWith(expected);
    });
  });
});
