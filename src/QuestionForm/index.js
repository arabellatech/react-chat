/* @flow */

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import _bindAll from 'lodash/bindAll';
import _isFunction from 'lodash/isFunction';
import _delay from 'lodash/delay';
import {
  chatSelectAnswerAction,
  chatUnselectAnswerAction,
  chatSetAnswerAction,
  chatSendQuestionReplyAction,
  chatChangeQuestionAction,
  chatQuestionAnimationStartAction,
  chatQuestionAnimationEndAction,
} from '../Schema/actions';
import {
  chatSetCustomVariablesAction,
} from '../VariablesPool/actions';
import {
  selectChatCurrentQuestion,
  selectChatCurrentReply,
  selectChatAnsweredQuestions,
  selectAnswers,
  selectSelectedAnswers,
} from '../Schema/selectors';
import {
  selectChatAnimations,
} from '../Chat/selectors';
import {
  selectChatThreadId,
} from '../Message/selectors';
import {
  selectChatCustomVariables,
} from '../VariablesPool/selectors';

import type {
  ChatQuestionFormProps,
} from '../types';

const mapStateToProps = (state) => ({
  currentQuestion: selectChatCurrentQuestion(state),
  currentQuestionReply: selectChatCurrentReply(state),
  answeredQuestions: selectChatAnsweredQuestions(state),
  customVariables: selectChatCustomVariables(state),
  animations: selectChatAnimations(state),
  threadId: selectChatThreadId(state),
});

const mapDispatchToProps = {
  selectAnswer: chatSelectAnswerAction,
  unselectAnswer: chatUnselectAnswerAction,
  setAnswer: chatSetAnswerAction,
  sendQuestionReply: chatSendQuestionReplyAction,
  setCustomVariables: chatSetCustomVariablesAction,
  changeQuestion: chatChangeQuestionAction,
  animationStart: chatQuestionAnimationStartAction,
  animationEnd: chatQuestionAnimationEndAction,
};

class QuestionForm extends PureComponent {
  constructor(props) {
    super(props);

    if (!props.type) {
      throw Error('props.type must be defined!');
    }

    if (!props.component) {
      throw Error('props.component must be defined!');
    }

    _bindAll(this, [
      'animateCSSClass',
      'ensureSingleSubmit',
      'getAnswerById',
      'getSelectedAnswers',
      'handleSubmit',
      'startAnimation',
    ]);
  }

  state = {
    submitting: false,
  };

  getAnswerById(id: number) {
    // TODO fix type
    const foundAnswer = selectAnswers(this.props.currentQuestion)
      .find((answer) => answer.get('id') === id);

    return foundAnswer ? foundAnswer.toJS() : null;
  }

  getSelectedAnswers() {
    // TODO fix type
    const foundAnswers = selectSelectedAnswers(this.props.currentQuestion);

    return foundAnswers.toJS();
  }

  getQuestionType() {
    const {
      currentQuestion,
      getType,
    } = this.props;
    let type;

    if (getType && _isFunction(getType)) {
      type = getType(currentQuestion);
    } else {
      const metaType = currentQuestion.getIn([
        'meta',
        'type',
      ]);
      const questionType = currentQuestion.get('type');

      type = metaType || questionType;
    }

    return type;
  }

  handleSubmit(event: Event) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    const {
      currentQuestionReply,
      sendQuestionReply,
      threadId,
    } = this.props;
    const questionReply = {
      ...currentQuestionReply,
      threadId,
    };

    // $FlowFixMe
    sendQuestionReply(questionReply);
  }

  startAnimation({
    type,
    duration,
    }: {
    type: 'IN' | 'OUT',
    duration: number,
  }) {
    if (type === 'IN') {
      this.animateIn(duration);
    } else if (type === 'OUT') {
      this.animateOut(duration);
    }
  }

  animateOut(duration: number) {
    const {
      animationStart,
      animationEnd,
    } = this.props;

    animationStart('out');

    _delay(
      () => {
        animationEnd('out');
      },
      duration
    );
  }

  animateIn(duration: number) {
    const {
      animationStart,
      animationEnd,
    } = this.props;

    animationStart('in');

    _delay(
      () => {
        animationEnd('in');
      },
      duration
    );
  }

  animateCSSClass(baseClassName: string) {
    const { animations } = this.props;
    const classNames = [baseClassName];
    const questionAnimation = animations.get('question');

    if (questionAnimation === 'out-started') {
      classNames.push(`${baseClassName}--out`);
    } else if (questionAnimation !== 'out-ended') {
      classNames.push(`${baseClassName}--in`);
    }

    return classNames.join(' ');
  }

  isMatchingType() {
    const expectedType = this.props.type;
    const currentType = this.getQuestionType();
    const isMatchingType = expectedType === currentType;

    return isMatchingType;
  }

  ensureSingleSubmit(callback) {
    if (!this.submitting) {
      this.setSubmitting(true);

      callback();

      setTimeout(() => {
        this.setSubmitting(false);
      }, 2000);
    }
  }

  setSubmitting(submitting) {
    this.submitting = submitting;

    this.setState({
      submitting,
    });
  }

  composeProps() {
    const {
      currentQuestion,
      animations,
      animationStart,
      animationEnd,
      ...parentProps
    } = this.props;
    const props = {
      ...parentProps,
      animateCSSClass: this.animateCSSClass,
      onSubmit: this.handleSubmit,
      startAnimation: this.startAnimation,
      getAnswerById: this.getAnswerById,
      getSelectedAnswers: this.getSelectedAnswers,
      submitting: this.state.submitting,
      ensureSingleSubmit: this.ensureSingleSubmit,
      // TODO http://redux.js.org/docs/recipes/UsingImmutableJS.html#use-a-higher-order-component-to-convert-your-smart-components-immutablejs-props-to-your-dumb-components-javascript-props
      // convert from immutable to simple object
      // for use in dumb components
      // TODO fix type
      question: currentQuestion.toJS(),
    };

    return props;
  }

  props: ChatQuestionFormProps;

  render() {
    if (!this.isMatchingType()) {
      return null;
    }

    const props = this.composeProps();
    const Component = this.props.component;

    return (
      <Component {...props} />
    );
  }
}

// override state and dispatch props with own props
const mergeProps = (stateProps?, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
});

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(QuestionForm);
