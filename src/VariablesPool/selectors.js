/* @flow */

import { createSelector } from 'reselect';
import {
  getChatDomain,
} from '../Chat/selectors';
import type {
  ChatStateType,
} from '../types';

export const getVariablesPool = (state: ChatStateType) => state.get('variablesPool');
export const getCustomVariables = (state: ChatStateType) => state.get('customVariables');

export const selectVariablesPool = createSelector(
  getChatDomain,
  getVariablesPool,
);

export const selectChatCustomVariables = createSelector(
  getChatDomain,
  getCustomVariables,
);
