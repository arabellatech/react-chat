/* @flow */

import type {
  ChatStateType,
} from '../types';
import type {
  ChatSetVariablesPoolActionType,
  ChatSetCustomVariablesActionType,
} from '../VariablesPool/types';

export const onChatSetCustomVariablesAction = (state: ChatStateType, action: ChatSetCustomVariablesActionType) => state.mergeIn(['customVariables'], action.variables || {});
export const onChatSetVariablesPoolAction = (state: ChatStateType, action: ChatSetVariablesPoolActionType) => state.mergeIn(['variablesPool'], action.variablesPool || {});
