/* @flow */

/* actions */

export const CHAT_SET_VARIABLES_POOL_ACTION = 'app/Chat/SET_VARIABLES_POOL';
export const CHAT_SET_CUSTOM_VARIABLES_ACTION = 'app/Chat/SET_CUSTOM_VARIABLES';
