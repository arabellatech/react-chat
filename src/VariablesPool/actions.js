/* @flow */

import {
  CHAT_SET_VARIABLES_POOL_ACTION,
  CHAT_SET_CUSTOM_VARIABLES_ACTION,
} from './constants';

import type {
  ChatSetVariablesPoolActionType,
  ChatSetCustomVariablesActionType,
} from './types';

export const chatSetVariablesPoolAction = (variablesPool: Object): ChatSetVariablesPoolActionType => ({
  type: CHAT_SET_VARIABLES_POOL_ACTION,
  variablesPool,
});

export const chatSetCustomVariablesAction = (variables: Object): ChatSetCustomVariablesActionType => ({
  type: CHAT_SET_CUSTOM_VARIABLES_ACTION,
  variables,
});
