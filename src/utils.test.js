/* @flow */

import {
  getForkedQid,
  parseSchema,
  parseSchemaQuestion,
  createMessageId,
  readQidFromFeId,
} from './utils';

import type {
  ChatQuestionType,
} from './types';

describe('utils', () => {
  describe('parseSchemaQuestion', () => {
    const question = {
      schema: 2,
      rangeTo: null,
      nextQid: null,
      statesInitedWith: [],
      required: false,
      hasAttachments: false,
      qid: 10,
      choiceOpen: false,
      type: 'choice',
      group: 1,
      id: 3,
      text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
      rangeFrom: null,
      position: 10,
      states_with: [],
      answers: [
        {
          question: 3,
          nextQid: 20,
          id: 1,
          text: 'START ONLINE VISIT',
          position: 1,
        },
        {
          question: 3,
          nextQid: 30,
          id: 2,
          text: 'START ONLINE VISIT',
          position: 2,
        },
      ],
      attachments: [],
    };
    const expected: ChatQuestionType = {
      nextQid: null,
      qid: 10,
      choiceOpen: false,
      type: 'choice',
      id: 3,
      text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
      answers: [
        {
          question: 3,
          nextQid: 20,
          id: 1,
          text: 'START ONLINE VISIT',
          position: 1,
        },
        {
          question: 3,
          nextQid: 30,
          id: 2,
          text: 'START ONLINE VISIT',
          position: 2,
        },
      ],
      attachments: [],
    };

    it('should return question with only selected attributes', () => {
      expect(parseSchemaQuestion(question)).toEqual(expected);
    });
  });

  describe('parseSchema', () => {
    const schema = {
      name: 'Onboarding',
      published: false,
      version: 1,
      type: 'onboarding',
      deprecated: false,
      id: 2,
      groups: [
        {
          id: 1,
          schema: 2,
          position: 1,
          name: 'M: Customer initial page',
          questions: [
            {
              schema: 2,
              rangeTo: null,
              nextQid: null,
              statesInitedWith: [],
              required: false,
              hasAttachments: false,
              qid: 10,
              choiceOpen: false,
              type: 'choice',
              group: 1,
              id: 3,
              text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
              rangeFrom: null,
              position: 10,
              states_with: [],
              answers: [
                {
                  question: 3,
                  nextQid: 20,
                  id: 1,
                  text: 'START ONLINE VISIT',
                  position: 1,
                },
                {
                  question: 3,
                  nextQid: 30,
                  id: 2,
                  text: 'START ONLINE VISIT',
                  position: 2,
                },
              ],
              attachments: [],
            },
          ],
        },
        {
          id: 2,
          schema: 2,
          position: 2,
          name: 'Q: Not available zip code',
          questions: [
            {
              schema: 2,
              rangeTo: null,
              nextQid: 21,
              statesInitedWith: [],
              required: false,
              hasAttachments: false,
              qid: 20,
              choiceOpen: false,
              type: 'message',
              group: 2,
              id: 4,
              text: 'Thank you. Im afraid were not available in your area yet.',
              rangeFrom: null,
              position: 20,
              states_with: [],
              answers: [],
              attachments: [],
            },
            {
              schema: 2,
              rangeTo: null,
              nextQid: -1,
              statesInitedWith: [],
              required: false,
              hasAttachments: false,
              qid: 21,
              choiceOpen: false,
              type: 'text',
              group: 2,
              id: 5,
              text: 'Well notify you as soon as we launch in {{cityname}}. Enter your e-mail address to get $50 off when we launch.',
              rangeFrom: null,
              position: 21,
              states_with: [],
              answers: [
                {
                  question: 5,
                  nextQid: -1,
                  id: 3,
                  text: 'YOUR EMAIL ADDRESS',
                  position: 1,
                },
              ],
              attachments: [],
            },
          ],
        },
      ],
    };
    const expected = [
      {
        nextQid: null,
        qid: 10,
        choiceOpen: false,
        type: 'choice',
        id: 3,
        text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
        answers: [
          {
            question: 3,
            nextQid: 20,
            id: 1,
            text: 'START ONLINE VISIT',
            position: 1,
          },
          {
            question: 3,
            nextQid: 30,
            id: 2,
            text: 'START ONLINE VISIT',
            position: 2,
          },
        ],
        attachments: [],
      },
      {
        nextQid: 21,
        qid: 20,
        choiceOpen: false,
        type: 'message',
        id: 4,
        text: 'Thank you. Im afraid were not available in your area yet.',
        answers: [],
        attachments: [],
      },
      {
        nextQid: -1,
        qid: 21,
        choiceOpen: false,
        type: 'text',
        id: 5,
        text: 'Well notify you as soon as we launch in {{cityname}}. Enter your e-mail address to get $50 off when we launch.',
        answers: [
          {
            question: 5,
            nextQid: -1,
            id: 3,
            text: 'YOUR EMAIL ADDRESS',
            position: 1,
          },
        ],
        attachments: [],
      },
    ];

    it('should return questions array', () => {
      expect(parseSchema(schema)).toEqual(expected);
    });
  });

  describe('createMessageId', () => {
    it('should return SQR:1 for schema question reply', () => {
      const received = createMessageId({
        qid: 1,
        isOwn: true,
      });

      expect(received).toEqual('SQR:1');
    });

    it('should return SQM:2 for schema question message', () => {
      const received = createMessageId({
        qid: 2,
        isOwn: false,
      });

      expect(received).toEqual('SQM:2');
    });

    it('should return UOM:someUniqueID for outgoing message', () => {
      const received = createMessageId({
        isOwn: true,
      });

      expect(received).toEqual('UOM:someUniqueID');
    });

    it('should return SIM:4 for incoming message', () => {
      const received = createMessageId({
        id: 4,
        isOwn: false,
      });

      expect(received).toEqual('UIM:4');
    });
  });

  describe('readQidFromFeId', () => {
    it('should return null when called with false value', () => {
      expect(readQidFromFeId()).toBe(null);
      expect(readQidFromFeId('')).toBe(null);
      expect(readQidFromFeId(undefined)).toBe(null);
    });

    it('should return 10 when called with SQR:10', () => {
      expect(readQidFromFeId('SQR:10')).toBe(10);
    });

    it('should return 20 when called with SQM:20', () => {
      expect(readQidFromFeId('SQM:20')).toBe(20);
    });

    it('should return null when called with UIM:30', () => {
      expect(readQidFromFeId('UIM:30')).toBe(null);
    });

    it('should return null when called with UOM:40', () => {
      expect(readQidFromFeId('UIM:40')).toBe(null);
    });
  });

  describe('forks', () => {
    describe('getForkedQid', () => {
      describe('single rule', () => {
        const ruleVariableEq2 = [
          "10",
          "ruleVariable == 2",
        ];
        const ruleVariableEq5 = [
          "20",
          "ruleVariable == 5",
        ];

        it('should return null when missing variable for rule', () => {
          const received = getForkedQid(
            [
              ruleVariableEq2,
              ruleVariableEq5,
            ],
            {
              otherRuleVariable: 2,
            },
          );

          expect(received).toBeNull();
        });

        it('should return null when no matching rule', () => {
          const received = getForkedQid(
            [
              ruleVariableEq2,
              ruleVariableEq5,
            ],
            {
              ruleVariable: 1,
              otherRuleVariable: 2,
            },
          );

          expect(received).toBeNull();
        });

        it('should return rule[0] when matching rule found (match for first rule)', () => {
          const received = getForkedQid(
            [
              ruleVariableEq2,
              ruleVariableEq5,
            ],
            {
              ruleVariable: 2,
              otherRuleVariable: 5,
            },
          );

          expect(received).toEqual(10);
        });

        it('should return rule[0] when matching rule found (match for second rule)', () => {
          const received = getForkedQid(
            [
              ruleVariableEq2,
              ruleVariableEq5,
            ],
            {
              ruleVariable: 5,
              otherRuleVariable: 2,
            },
          );

          expect(received).toEqual(20);
        });
      });

      describe('two rules with "and"', () => {
        const ruleVariableAEq3AndVariableBEq4 = [
          "10",
          "ruleVariableA == 3 and ruleVariableB == 4",
        ];
        const ruleVariableAEq1AndVariableBEq2 = [
          "20",
          "ruleVariableA == 1 and ruleVariableB == 2",
        ];

        it('should return null when missing variable for all equations', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3AndVariableBEq4,
              ruleVariableAEq1AndVariableBEq2,
            ],
            {},
          );

          expect(received).toBeNull();
        });

        it('should return null when missing variable for single equation', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3AndVariableBEq4,
              ruleVariableAEq1AndVariableBEq2,
            ],
            {
              ruleVariableA: 3,
            },
          );

          expect(received).toBeNull();
        });

        it('should return rule[0] when matching rule found (match for first rule)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3AndVariableBEq4,
              ruleVariableAEq1AndVariableBEq2,
            ],
            {
              ruleVariableA: 3,
              ruleVariableB: 4,
            },
          );

          expect(received).toEqual(10);
        });

        it('should return rule[0] when matching rule found (match for second rule)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3AndVariableBEq4,
              ruleVariableAEq1AndVariableBEq2,
            ],
            {
              ruleVariableA: 1,
              ruleVariableB: 2,
            },
          );

          expect(received).toEqual(20);
        });
      });

      describe('two rules with "or"', () => {
        const ruleVariableAEq3OrVariableBEq4 = [
          "10",
          "ruleVariableA == 3 or ruleVariableB == 4",
        ];
        const ruleVariableAEq1OrVariableBEq2 = [
          "20",
          "ruleVariableA == 1 or ruleVariableB == 2",
        ];

        it('should return null when missing variable for all equations', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3OrVariableBEq4,
              ruleVariableAEq1OrVariableBEq2,
            ],
            {},
          );

          expect(received).toBeNull();
        });

        it('should return rule[0] when matching rule found (match for first rule & first equation)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3OrVariableBEq4,
              ruleVariableAEq1OrVariableBEq2,
            ],
            {
              ruleVariableA: 3,
            },
          );

          expect(received).toEqual(10);
        });

        it('should return rule[0] when matching rule found (match for first rule & second equation)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3OrVariableBEq4,
              ruleVariableAEq1OrVariableBEq2,
            ],
            {
              ruleVariableB: 4,
            },
          );

          expect(received).toEqual(10);
        });

        it('should return rule[0] when matching rule found (match for second rule & first equation)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3OrVariableBEq4,
              ruleVariableAEq1OrVariableBEq2,
            ],
            {
              ruleVariableA: 1,
            },
          );

          expect(received).toEqual(20);
        });

        it('should return rule[0] when matching rule found (match for second rule & second equation)', () => {
          const received = getForkedQid(
            [
              ruleVariableAEq3OrVariableBEq4,
              ruleVariableAEq1OrVariableBEq2,
            ],
            {
              ruleVariableB: 2,
            },
          );

          expect(received).toEqual(20);
        });
      });
    });
  });
});
