/* @flow */

import { WEBSOCKET_MESSAGE } from '@giantmachines/redux-websocket';
// $FlowWTF
import type { Action } from '@giantmachines/redux-websocket';
import {
  CHAT_SET_THREAD_ID_ACTION,
  CHAT_THREAD_ROLLBACK_ACTION,

  CHAT_LOAD_SCHEMA_ACTION,
  CHAT_LOAD_SCHEMA_SUCCESS_ACTION,
  CHAT_LOAD_SCHEMA_FAILED_ACTION,

  CHAT_LOAD_ONBOARDING_SCHEMA_ACTION,
  CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION,
  CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION,

  CHAT_SELECT_ANSWER_ACTION,
  CHAT_UNSELECT_ANSWER_ACTION,
  CHAT_SET_ANSWER_ACTION,

  CHAT_SEND_QUESTION_REPLY_ACTION,
  CHAT_CHANGE_QUESTION_ACTION,
  CHAT_NEXT_QUESTION_ACTION,

  CHAT_QUESTION_ANIMATION_START,
  CHAT_QUESTION_ANIMATION_END,

  CHAT_SCHEMA_QUEUE_STARTED_ACTION,
  CHAT_SCHEMA_QUEUE_ENDED_ACTION,
  CHAT_SCHEMA_QUEUE_FAILED_ACTION,
  CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION,
} from './Schema/constants';
import {
  CHAT_SET_VARIABLES_POOL_ACTION,
  CHAT_SET_CUSTOM_VARIABLES_ACTION,
} from './VariablesPool/constants';
import {
  CHAT_RESET_MESSAGES_ACTION,
  CHAT_LOAD_MESSAGES_ACTION,
  CHAT_LOAD_MESSAGES_SUCCESS_ACTION,
  CHAT_SEND_MESSAGE_ACTION,

  CHAT_BUBBLE_ANIMATION_START,
  CHAT_BUBBLE_ANIMATION_END,

  CHAT_SET_SENDER_ID_ACTION,
} from './Message/constants';
import {
  onSetThreadIdAction,
  onResetMessagesAction,
  onLoadMessagesAction,
  onLoadMessagesSuccessAction,
  onSendMessageAction,
  onReceiveWebsocketMessageAction,
  onChatBubbleAnimationStart,
  onChatBubbleAnimationEnd,
  onChatSetSenderId,
} from './Message/reducer';
import {
  onChatQuestionAnimationStart,
  onChatQuestionAnimationEnd,
  onSchemaLoad,
  onSchemaLoadSuccess,
  onSchemaLoadFailed,
  onAnswerSelect,
  onAnswerUnselect,
  onAnswerSet,
  onSendQuestionReply,
  onChangeQuestion,
  onNextQuestionAction,
  onThreadRollbackQuestionAction,
  onChatSchemaQueueStarted,
  onChatSchemaQueueEnded,
  onChatSchemaQueueFailed,
  onChatSchemaQueueSetRequestsProgressAction,
} from './Schema/reducer';
import {
  onChatSetCustomVariablesAction,
  onChatSetVariablesPoolAction,
} from './VariablesPool/reducer';
import initialState from './initialState';

import type {
  ChatStateType,
  ChatActionType,
} from './types';

function chatReducer(state: ChatStateType = initialState, action: ChatActionType | Action): ChatStateType {
  switch (action.type) {
    case CHAT_SET_THREAD_ID_ACTION:
      return onSetThreadIdAction(state, action);

    case CHAT_RESET_MESSAGES_ACTION:
      return onResetMessagesAction(state, action);

    case CHAT_LOAD_MESSAGES_ACTION:
      return onLoadMessagesAction(state, action);
    case CHAT_LOAD_MESSAGES_SUCCESS_ACTION:
      return onLoadMessagesSuccessAction(state, action);
    case CHAT_SEND_MESSAGE_ACTION:
      return onSendMessageAction(state, action);
    case WEBSOCKET_MESSAGE:
      return onReceiveWebsocketMessageAction(state, action);

    case CHAT_LOAD_SCHEMA_ACTION:
    case CHAT_LOAD_ONBOARDING_SCHEMA_ACTION:
      return onSchemaLoad(state, action);
    case CHAT_LOAD_SCHEMA_SUCCESS_ACTION:
    case CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION:
      return onSchemaLoadSuccess(state, action);
    case CHAT_LOAD_SCHEMA_FAILED_ACTION:
    case CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION:
      return onSchemaLoadFailed(state, action);

    case CHAT_SCHEMA_QUEUE_STARTED_ACTION:
      return onChatSchemaQueueStarted(state);
    case CHAT_SCHEMA_QUEUE_ENDED_ACTION:
      return onChatSchemaQueueEnded(state);
    case CHAT_SCHEMA_QUEUE_FAILED_ACTION:
      return onChatSchemaQueueFailed(state, action);

    case CHAT_SELECT_ANSWER_ACTION:
      return onAnswerSelect(state, action);
    case CHAT_UNSELECT_ANSWER_ACTION:
      return onAnswerUnselect(state, action);
    case CHAT_SET_ANSWER_ACTION:
      return onAnswerSet(state, action);

    case CHAT_SEND_QUESTION_REPLY_ACTION:
      return onSendQuestionReply(state, action);
    case CHAT_CHANGE_QUESTION_ACTION:
      return onChangeQuestion(state, action);
    case CHAT_NEXT_QUESTION_ACTION:
      return onNextQuestionAction(state);
    case CHAT_THREAD_ROLLBACK_ACTION:
      return onThreadRollbackQuestionAction(state, action);

    case CHAT_QUESTION_ANIMATION_START:
      return onChatQuestionAnimationStart(state, action.animation);
    case CHAT_QUESTION_ANIMATION_END:
      return onChatQuestionAnimationEnd(state, action.animation);

    case CHAT_BUBBLE_ANIMATION_START:
      return onChatBubbleAnimationStart(state);
    case CHAT_BUBBLE_ANIMATION_END:
      return onChatBubbleAnimationEnd(state);

    case CHAT_SET_SENDER_ID_ACTION:
      return onChatSetSenderId(state, action);

    case CHAT_SET_VARIABLES_POOL_ACTION:
      return onChatSetVariablesPoolAction(state, action);

    case CHAT_SET_CUSTOM_VARIABLES_ACTION:
      return onChatSetCustomVariablesAction(state, action);

    case CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION:
      return onChatSchemaQueueSetRequestsProgressAction(state, action);

    default:
      return state;
  }
}

export default chatReducer;
