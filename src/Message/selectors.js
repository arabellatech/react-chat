/* @flow */

import {
  List,
  Map,
  OrderedMap,
  fromJS,
} from 'immutable';
import { createSelector } from 'reselect';
import {
  getChatDomain,
} from '../Chat/selectors';
import type {
  ChatStateType,
} from '../types';

/* getters */

export const getMessagesList = (state: ChatStateType) => state.get('messages');
export const getLoadingMessagesList = (state: ChatStateType) => state.get('loadingMessages');
export const getChatShowAnimations = (state: ChatStateType) => state.get('showAnimations');
export const getChatThreadId = (state: ChatStateType) => state.get('threadId');
export const getMessagesFromGroup = (state: ChatStateType) => state.get('items');
export const getMessageDate = (state: ChatStateType) => state
  .get('created')
  .substr(0, 10);
export const getFirstMessageFeId = (state: ChatStateType) => state
  .get('items')
  .get(0)
  .get('feId');

/* chat level selectors (for use in chat reducers) */

/* messages group related selectors */

export const selectLastGroupFromMessagesList = createSelector(
  getMessagesList,
  (messages) => messages.last(),
);

export const makeSelectMessagesListUpToFeId = (feId: string) => createSelector(
  getMessagesList,
  (messagesGroups) => {
    const size = messagesGroups.size;
    let trimmedMessages = new List();

    for (let i = 0; i < size; i += 1) {
      const currentGroup = messagesGroups.get(i);

      if (getFirstMessageFeId(currentGroup) === feId) {
        break;
      }

      trimmedMessages = trimmedMessages.push(currentGroup);
    }

    return trimmedMessages;
  }
);

export const selectMessagesGrouppedByDate = createSelector(
  getMessagesList,
  (messagesGroups) => {
    let messagesGrouppedByDate = OrderedMap();

    const addGroupForDate = (messagesGroup, date) => {
      let groupsForDate = messagesGrouppedByDate.get(date);

      if (groupsForDate) {
        groupsForDate = groupsForDate.push(messagesGroup);
      } else {
        groupsForDate = fromJS([messagesGroup]);
      }

      messagesGrouppedByDate = messagesGrouppedByDate.merge({
        [date]: groupsForDate,
      });
    };

    const mapGroupToDates = (messagesGroup) => {
      const messagesByDate = selectMessagesByDate(messagesGroup);

      if (!messagesByDate) {
        return;
      }

      messagesByDate.forEach(
        (messages, date) => {
          const groupForDate = messagesGroup.set('items', messages);

          addGroupForDate(groupForDate, date);
        }
      );
    };

    if (messagesGroups) {
      messagesGroups.forEach(mapGroupToDates);
    }

    return messagesGrouppedByDate;
  }
);

export const selectMessagesByDate = (messagesGroup: Map<string, *>) => {
  const messages = getMessagesFromGroup(messagesGroup);
  let messagesByDate = Map();

  messages.forEach(
    (message) => {
      const date = getMessageDate(message);
      let messagesForDate = messagesByDate.get(date);

      if (!messagesForDate) {
        messagesForDate = fromJS([message]);
      } else {
        messagesForDate = messagesForDate.push(message);
      }

      messagesByDate = messagesByDate.merge({
        [date]: messagesForDate,
      });
    },
  );

  return messagesByDate;
};

/* top level selectors (for use in mapStateToProps or global reducers) */

export const selectChatMessagesList = createSelector(
  getChatDomain,
  getMessagesList,
);

export const selectChatMessagesGrouppedByDate = createSelector(
  getChatDomain,
  selectMessagesGrouppedByDate,
);

export const selectChatLoadingMessagesList = createSelector(
  getChatDomain,
  getLoadingMessagesList,
);

export const selectChatShowAnimations = createSelector(
  getChatDomain,
  getChatShowAnimations,
);

export const selectChatThreadId = createSelector(
  getChatDomain,
  getChatThreadId,
);
