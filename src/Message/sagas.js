// TODO: use flow

import {
  call,
  put,
  takeEvery,
} from 'redux-saga/effects';
import {
  chatLoadMessagesSuccessAction,
  chatLoadMessagesFailedAction,
  chatSendMessageAction,
  chatSendMessageSuccessAction,
  chatSendMessageFailedAction,
} from './actions';
import {
  CHAT_LOAD_MESSAGES_ACTION,
  CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,
  CHAT_SEND_MESSAGE_ACTION,
} from './constants';
import {
  loadMessages,
  sendMessage,
} from '../api';
/*
import type {
  ChatLoadMessagesActionType,
  ChatSendMessageActionType,
  ChatThreadRollbackActionType,
} from './types';
*/

export function* watchChatLoadMessagesSaga() {
  yield takeEvery(CHAT_LOAD_MESSAGES_ACTION, chatLoadMessagesSaga);
}

export function* chatLoadMessagesSaga(action/* : ChatLoadMessagesActionType */) {
  try {
    const response = yield call(loadMessages, action.threadId);

    yield put(chatLoadMessagesSuccessAction(response.data));
  } catch (error) {
    yield put(chatLoadMessagesFailedAction(error));
  }
}

export function* watchChatSendMessageAndUpdateThreadSaga() {
  yield takeEvery([
    CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
    CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
    CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,
  ], chatTriggerSendMessageSaga);
}

export function* chatTriggerSendMessageSaga(action) {
  yield put(chatSendMessageAction(action.payload));
}

export function* watchChatSendMessageSaga() {
  yield takeEvery(CHAT_SEND_MESSAGE_ACTION, chatSendMessageSaga);
}

export function* chatSendMessageSaga(action/* : ChatSendMessageActionType */) {
  try {
    const response = yield call(sendMessage, action.payload);

    yield put(chatSendMessageSuccessAction(response.data));
  } catch (error) {
    yield put(chatSendMessageFailedAction(error));
  }
}

// All sagas to be loaded
export default [
  watchChatLoadMessagesSaga,
  watchChatSendMessageAndUpdateThreadSaga,
  watchChatSendMessageSaga,
];
