/* @flow */

import {
  CHAT_MESSAGE_TYPE_TEXT,
} from '../Message/constants';
import chatReducer from '../reducer';
import {
  addIncomingMessage,
  addOutgoingMessage,
} from './reducer';
import type { Action } from '@giantmachines/redux-websocket';
import type {
  ChatSendMessageActionType,
} from '../types';

describe('chatReducer', () => {
  let initialState;
  const getState = (state?: Object = {}) => initialState.merge(state);


  beforeEach(() => {
    // $FlowExpectedError
    Date.now = jest.fn(() => 1487076708000); // 2017-02-14T13:51:48+01:00

    initialState = chatReducer(undefined, {});
  });


  describe('CHAT_SET_THREAD_ID', () => {
    it('should set threadId', () => {
      const state = initialState.merge({
        threadId: null,
      });
      const received = chatReducer(
        state,
        {
          type: 'app/Chat/SET_THREAD_ID',
          id: 1,
        }
      );

      expect(received.get('threadId')).toBe(1);
    });
  });

  describe('CHAT_RESET_MESSAGES', () => {
    it('should reset state to initial values', () => {
      const state = getState({
        loadingSchema: true,
        loadingMessages: true,
        showAnimations: true,
        questions: [{
          id: 1,
        }],
        currentQuestion: {
          answers: [{
            id: 2,
          }],
        },
        questionsReplies: [{
          id: 3,
        }],
        schemaId: 4,
        messages: [{
          id: 5,
        }],
        thread: 6,
        threadId: 7,
        animations: {
          bubble: 'ended',
        },
        customVariables: {
          var: 1,
        },
        variablesPool: {
          var: 2,
        },
      });
      const received = chatReducer(
        state,
        {
          type: 'app/Chat/RESET_MESSAGES',
        }
      );
      const expected = initialState.toJS();

      expect(received.toJS()).toEqual(expected);
    });
  });

  describe('loading messages', () => {
    const senderIncoming = {
      senderData: {
        id: 1,
      },
    };
    const senderOutgoing = {
      senderData: {
        id: 2,
      },
    };
    const messageIncoming1 = {
      text: 'INCOMING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      isOwn: false,
      ...senderIncoming,
    };
    const messageIncoming2 = {
      text: 'INCOMING_2',
      type: CHAT_MESSAGE_TYPE_TEXT,
      isOwn: false,
      ...senderIncoming,
    };
    const messageOutgoing1 = {
      text: 'OUTGOING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      isOwn: true,
      ...senderOutgoing,
    };


    describe('CHAT_LOAD_MESSAGES', () => {
      it('should set loadingMessages to true', () => {
        const received = chatReducer(
          initialState,
          {
            type: 'app/Chat/LOAD_MESSAGES',
          }
        );

        expect(received.get('loadingMessages')).toBe(true);
      });
    });

    describe('CHAT_LOAD_MESSAGES_SUCCESS', () => {
      it('should create message groups when state.messages is empty', () => {
        const received = chatReducer(
          initialState,
          {
            type: 'app/Chat/LOAD_MESSAGES_SUCCESS',
            payload: {
              results: [
                messageIncoming1,
                messageIncoming2,
                messageOutgoing1,
              ],
            },
          },
        );

        expect(
          received.get('messages')
            .toJS()
        ).toMatchSnapshot();
      });

      it('should update last group if it matches new message', () => {
        const state = getState({
          messages: [{
            items: [],
            sender: {
              id: 2,
            },
            type: CHAT_MESSAGE_TYPE_TEXT,
            showOnRight: true,
          }],
        });
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_MESSAGES_SUCCESS',
            payload: {
              results: [
                messageOutgoing1,
              ],
            },
          },
        );

        expect(
          received.get('messages')
            .toJS()
        ).toMatchSnapshot();
      });

      it('should create new group if new message doesn\'t match last group', () => {
        const state = getState({
          messages: [{
            items: [],
            sender: {
              id: 2,
            },
            type: CHAT_MESSAGE_TYPE_TEXT,
            showOnRight: true,
          }],
        });
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/LOAD_MESSAGES_SUCCESS',
            payload: {
              results: [
                messageIncoming1,
              ],
            },
          },
        );

        expect(
          received.get('messages')
            .toJS()
        ).toMatchSnapshot();
      });
    });
  });

  describe('CHAT_SEND_MESSAGE', () => {
    const messageIncoming1 = {
      text: 'INCOMING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      threadId: 100,
    };
    const messageIncoming2 = {
      text: 'INCOMING_2',
      type: CHAT_MESSAGE_TYPE_TEXT,
      threadId: 100,
    };
    const messageOutgoing1 = {
      text: 'OUTGOING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      threadId: 100,
    };

    it('should create new messages group if none exist', () => {
      const action: ChatSendMessageActionType = {
        type: 'app/Chat/SEND_MESSAGE',
        payload: {
          ...messageIncoming1,
        },
      };
      const received = chatReducer(
        initialState,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toMatchSnapshot();
    });

    it('should create new messages group if last group is with incoming messages', () => {
      const state = getState({
        messages: [{
          items: [
            messageIncoming1,
          ],
          isOwn: false,
          type: CHAT_MESSAGE_TYPE_TEXT,
          sender: 1,
        }],
      });
      const action: ChatSendMessageActionType = {
        type: 'app/Chat/SEND_MESSAGE',
        payload: {
          ...messageOutgoing1,
        },
      };
      const received = chatReducer(
        state,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toMatchSnapshot();
    });

    it('should add to last messages group if new message matches', () => {
      const state = getState({
        messages: [{
          items: [
            messageIncoming1,
          ],
          isOwn: false,
          type: CHAT_MESSAGE_TYPE_TEXT,
        }],
      });
      const action: ChatSendMessageActionType = {
        type: 'app/Chat/SEND_MESSAGE',
        payload: {
          ...messageIncoming2,
        },
      };
      const received = chatReducer(
        state,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toMatchSnapshot();
    });
  });

  describe('WEBSOCKET_MESSAGE', () => {
    const messageIncoming1 = {
      text: 'INCOMING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      showOnRight: false,
    };
    const messageOutgoing1 = {
      text: 'OUTGOING_1',
      type: CHAT_MESSAGE_TYPE_TEXT,
      showOnRight: true,
    };

    it('should create new messages group if none exist', () => {
      const action: Action = {
        type: 'WEBSOCKET:MESSAGE',
        payload: {
          message: 'message_created_or_updated',
          data: {
            ...messageIncoming1,
          },
        },
      };
      const received = chatReducer(
        initialState,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toMatchSnapshot();
    });

    it('should create new messages group if last group is with outgoing messages', () => {
      const state = getState({
        messages: [{
          items: [
            messageOutgoing1,
          ],
          isOwn: true,
          type: CHAT_MESSAGE_TYPE_TEXT,
        }],
      });
      const action: Action = {
        type: 'WEBSOCKET:MESSAGE',
        payload: {
          message: 'message_created_or_updated',
          data: {
            ...messageIncoming1,
          },
        },
      };
      const received = chatReducer(
        state,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toMatchSnapshot();
    });

    it('should do nothing if message is from current user', () => {
      const action: Action = {
        type: 'WEBSOCKET:MESSAGE',
        payload: {
          message: 'message_created_or_updated',
          data: {
            ...messageOutgoing1,
          },
        },
      };
      const received = chatReducer(
        initialState,
        action,
      );

      expect(
        received.get('messages')
          .toJS()
      ).toEqual(
        initialState.get('messages')
          .toJS()
      );
    });
  });

  describe('animations', () => {
    describe('CHAT_BUBBLE_ANIMATION_START', () => {
      it('should add bubble:started to animations', () => {
        const state = getState();
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/BUBBLE_ANIMATION_START',
          }
        );
        const expected = {
          bubble: 'started',
        };

        expect(
          received.get('animations')
            .toJS()
        ).toEqual(expected);
      });
    });

    describe('CHAT_BUBBLE_ANIMATION_END', () => {
      it('should add bubble:ended to animations', () => {
        const state = getState();
        const received = chatReducer(
          state,
          {
            type: 'app/Chat/BUBBLE_ANIMATION_END',
          }
        );
        const expected = {
          bubble: 'ended',
        };

        expect(
          received.get('animations')
            .toJS()
        ).toEqual(expected);
      });
    });
  });

  describe('add message helpers', () => {
    let state;
    const incomingGroup = {
      showOnRight: false,
      sender: null,
      items: [],
      type: CHAT_MESSAGE_TYPE_TEXT,
    };
    const outgoingGroup = {
      showOnRight: true,
      items: [],
      type: CHAT_MESSAGE_TYPE_TEXT,
    };

    describe('addIncomingMessage', () => {
      it('should create new messages group if there are none yet', () => {
        state = getState();
        const result = addIncomingMessage(
          state,
          {
            text: 'NEW_INCOMING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [{
          ...incomingGroup,
          items: [{
            text: 'NEW_INCOMING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }],
          type: CHAT_MESSAGE_TYPE_TEXT,
        }];

        expect(result).toEqual(expected);
      });

      it('should create new messages group if last one contains outgoing messages', () => {
        state = getState({
          messages: [{
            ...outgoingGroup,
          }],
        });
        const result = addIncomingMessage(
          state,
          {
            text: 'NEW_INCOMING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [
          outgoingGroup,
          {
            ...incomingGroup,
            items: [{
              text: 'NEW_INCOMING_MSG',
              threadId: 100,
              type: CHAT_MESSAGE_TYPE_TEXT,
            }],
            type: CHAT_MESSAGE_TYPE_TEXT,
          },
        ];

        expect(result).toEqual(expected);
      });

      it('should add to last messages group if it contains incoming messages', () => {
        state = getState({
          messages: [{
            ...incomingGroup,
          }],
        });
        const result = addIncomingMessage(
          state,
          {
            text: 'NEW_INCOMING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [{
          ...incomingGroup,
          items: [{
            text: 'NEW_INCOMING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }],
          type: CHAT_MESSAGE_TYPE_TEXT,
        }];

        expect(result).toEqual(expected);
      });
    });

    describe('addOutgoingMessage', () => {
      it('should create new messages group if there are none yet', () => {
        state = getState();
        const result = addOutgoingMessage(
          state,
          {
            text: 'NEW_OUTGOING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [{
          ...outgoingGroup,
          items: [{
            text: 'NEW_OUTGOING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }],
          sender: null,
          type: CHAT_MESSAGE_TYPE_TEXT,
        }];

        expect(result).toEqual(expected);
      });

      it('should create new messages group if last one contains incoming messages', () => {
        state = getState({
          messages: [{
            ...incomingGroup,
          }],
        });
        const result = addOutgoingMessage(
          state,
          {
            text: 'NEW_OUTGOING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [
          incomingGroup,
          {
            ...outgoingGroup,
            items: [{
              text: 'NEW_OUTGOING_MSG',
              threadId: 100,
              type: CHAT_MESSAGE_TYPE_TEXT,
            }],
            sender: null,
            type: CHAT_MESSAGE_TYPE_TEXT,
          },
        ];

        expect(result).toEqual(expected);
      });

      it('should add to last messages group if it contains outgoing messages', () => {
        state = getState({
          messages: [{
            ...outgoingGroup,
          }],
        });
        const result = addOutgoingMessage(
          state,
          {
            text: 'NEW_OUTGOING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }
        ).toJS();
        const expected = [{
          ...outgoingGroup,
          items: [{
            text: 'NEW_OUTGOING_MSG',
            threadId: 100,
            type: CHAT_MESSAGE_TYPE_TEXT,
          }],
        }];

        expect(result).toEqual(expected);
      });
    });
  });
});
