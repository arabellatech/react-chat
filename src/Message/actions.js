/* @flow */

import {
  CHAT_RESET_MESSAGES_ACTION,

  CHAT_LOAD_MESSAGES_ACTION,
  CHAT_LOAD_MESSAGES_SUCCESS_ACTION,
  CHAT_LOAD_MESSAGES_FAILED_ACTION,

  CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
  CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,

  CHAT_SEND_MESSAGE_ACTION,
  CHAT_SEND_MESSAGE_SUCCESS_ACTION,
  CHAT_SEND_MESSAGE_FAILED_ACTION,

  CHAT_BUBBLE_ANIMATION_START,
  CHAT_BUBBLE_ANIMATION_END,

  CHAT_SET_SENDER_ID_ACTION,
} from './constants';

import type {
  ChatMessageType,
  ChatMessageListSerializerType,

  ChatLoadMessagesActionType,
  ChatLoadMessagesSuccessActionType,
  ChatLoadMessagesFailedActionType,

  ChatSendMessageAndCloseThreadActionType,

  ChatSendMessageActionType,
  ChatSendMessageSuccessActionType,
  ChatSendMessageFailedActionType,

  // ChatUpdateMessageStatusActionType,

  ChatBubbleAnimationStartActionType,
  ChatBubbleAnimationEndActionType,

  ChatResetMessagesActionType,
} from '../types';

export function chatLoadMessagesAction(threadId: number): ChatLoadMessagesActionType {
  return {
    type: CHAT_LOAD_MESSAGES_ACTION,
    threadId,
  };
}

export function chatLoadMessagesSuccessAction(payload: ChatMessageListSerializerType): ChatLoadMessagesSuccessActionType {
  return {
    type: CHAT_LOAD_MESSAGES_SUCCESS_ACTION,
    payload,
  };
}

export function chatLoadMessagesFailedAction(payload: Object): ChatLoadMessagesFailedActionType {
  return {
    type: CHAT_LOAD_MESSAGES_FAILED_ACTION,
    payload,
  };
}

export function chatSendMessageAndCloseThreadAction(payload: ChatMessageType): ChatSendMessageAndCloseThreadActionType {
  return {
    type: CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
    payload,
  };
}

export function chatSendMessageAndOpenThreadAction(payload: ChatMessageType): ChatSendMessageAndOpenThreadActionType {
  return {
    type: CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
    payload,
  };
}

export function chatSendMessageAndWaitForResponseAction(payload: ChatMessageType): ChatSendMessageAndWaitForResponseActionType {
  return {
    type: CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,
    payload,
  };
}

export function chatSendMessageAction(payload: ChatMessageType): ChatSendMessageActionType {
  return {
    type: CHAT_SEND_MESSAGE_ACTION,
    payload,
  };
}

export function chatSendMessageSuccessAction(payload: ChatMessageType): ChatSendMessageSuccessActionType {
  return {
    type: CHAT_SEND_MESSAGE_SUCCESS_ACTION,
    payload,
  };
}

export function chatSendMessageFailedAction(payload: Object): ChatSendMessageFailedActionType {
  return {
    type: CHAT_SEND_MESSAGE_FAILED_ACTION,
    payload,
  };
}

export function chatBubbleAnimationStartAction(): ChatBubbleAnimationStartActionType {
  return {
    type: CHAT_BUBBLE_ANIMATION_START,
  };
}

export function chatBubbleAnimationEndAction(): ChatBubbleAnimationEndActionType {
  return {
    type: CHAT_BUBBLE_ANIMATION_END,
  };
}

export function chatResetMessagesAction(): ChatResetMessagesActionType {
  return {
    type: CHAT_RESET_MESSAGES_ACTION,
  };
}

export function chatSetSenderIdAction(senderId) {
  return {
    type: CHAT_SET_SENDER_ID_ACTION,
    senderId,
  };
}
