/* @flow */

import _pick from 'lodash/pick';
import _get from 'lodash/get';

import type {
  ChatMessageType,
  ChatSenderType,
  ChatMessagesListGroupType,
  ChatAccountSerializerType,
  ChatMessageSerializerType,
} from '../types';

export function parseMessage(item: ChatMessageSerializerType): ChatMessageType {
  let delay = 0;
  if (_get(item, 'meta.delay')) {
    delay = parseInt(item.meta.delay, 10);
  }

  return {
    id: item.id,
    feId: _get(item, 'meta.feId'),
    threadId: item.thread,
    created: item.created,
    editable: false,
    status: 'loading',
    type: item.type,
    text: item.text,
    delay,
    meta: _get(item, 'meta'),
    attachments: item.attachmentsData,
  };
}

// TODO: decide whether or not we should remove this function
export function parseSender(item: ChatAccountSerializerType): ChatSenderType {
  return item;
  // return _pick(
  //   item,
  //   [
  //     'id',
  //     'photo',
  //     'firstName',
  //     'lastName',
  //     'role',
  //   ]
  // );
}

export function createMessagesListGroup(
  showOnRight: boolean = false,
  sender: ?ChatSenderType = undefined,
  items: Array<ChatMessageType> = [],
  type: string = ''
): ChatMessagesListGroupType {
  return {
    showOnRight,
    sender,
    items,
    type,
  };
}
