/* @flow */

import { fromJS } from 'immutable';
import {
  selectMessagesGrouppedByDate,
} from './selectors';

describe('messages selectors:', () => {
  const message1 = {
    id: 1,
    created: '2017-06-06T15:15:49.970873Z',
    showOnRight: false,
    text: 'THEIR_FIRST_MSG',
  };
  const message2 = {
    id: 2,
    created: '2017-06-06T15:28:49.970873Z',
    showOnRight: false,
    text: 'THEIR_SECOND_MSG',
  };
  const message3 = {
    id: 3,
    created: '2017-06-06T16:15:49.970873Z',
    showOnRight: true,
    text: 'MY_FIRST_MSG',
  };
  const message4 = {
    id: 4,
    created: '2017-06-07T10:28:49.970873Z',
    showOnRight: false,
    text: 'THEIR_THIRD_MSG',
  };
  const message5 = {
    id: 5,
    created: '2017-06-07T12:00:49.970873Z',
    showOnRight: true,
    text: 'MY_SECOND_MSG',
  };
  const message6 = {
    id: 6,
    created: '2017-06-07T13:10:49.970873Z',
    showOnRight: true,
    text: 'MY_THIRD_MSG',
  };
  const group1 = {
    showOnRight: false,
    items: [
      message1,
      message2,
    ],
  };
  const group2 = {
    showOnRight: true,
    items: [
      message3,
    ],
  };
  const group3 = {
    showOnRight: false,
    items: [
      message4,
    ],
  };
  const group4 = {
    showOnRight: true,
    items: [
      message5,
      message6,
    ],
  };
  const defaultMessages = [
    group1,
    group2,
    group3,
    group4,
  ];

  const getState = (messages = defaultMessages) => fromJS({
    messages,
  });


  describe('selectMessagesGrouppedByDate', () => {
    describe('incoming messages', () => {
      it('should split single messages groups by date', () => {
        const chatState = getState([{
          showOnRight: false,
          items: [
            message1,
            message2,
            message4,
          ],
        }]);
        const expected = {
          '2017-06-06': [
            group1,
          ],
          '2017-06-07': [
            group3,
          ],
        };
        const received = selectMessagesGrouppedByDate(chatState).toJS();

        expect(received).toEqual(expected);
      });

      it('should split two messages groups by date', () => {
        const chatState = getState([
          group1,
          group3,
        ]);
        const expected = {
          '2017-06-06': [
            group1,
          ],
          '2017-06-07': [
            group3,
          ],
        };
        const received = selectMessagesGrouppedByDate(chatState).toJS();

        expect(received).toEqual(expected);
      });
    });

    describe('incoming and outgoing messages', () => {
      it('should group messages by date', () => {
        const chatState = getState();
        const expected = {
          '2017-06-06': [
            group1,
            group2,
          ],
          '2017-06-07': [
            group3,
            group4,
          ],
        };
        const received = selectMessagesGrouppedByDate(chatState).toJS();

        expect(received).toEqual(expected);
      });
    });
  });
});
