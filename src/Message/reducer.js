/* @flow */

import { fromJS } from 'immutable';
import _last from 'lodash/last';
import _get from 'lodash/get';
// $FlowWTF
import type { Action } from '@giantmachines/redux-websocket';
import {
  getMessagesList,
} from './selectors';
import {
  createMessage,
} from '../utils';
import {
  parseMessage,
  parseSender,
  createMessagesListGroup,
} from './formatter';
import initialState from '../initialState';

import type {
  ChatStateType,
  ChatSetThreadIdActionType,

  ChatMessageType,
  ChatMessagesListGroupType,
  ChatGroupedMessagesListType,
  ChatMessageSerializerType,
  ChatLoadMessagesSuccessActionType,
  ChatSendMessageActionType,
} from '../types';

/* messages */

export const onSetThreadIdAction = (state: ChatStateType, action: ChatSetThreadIdActionType): ChatStateType => state.merge({
  threadId: action.id,
});

export const onResetMessagesAction = (state: ChatStateType): ChatStateType => state.merge(initialState);

export const onLoadMessagesAction = (state: ChatStateType): ChatStateType => state.merge({
  loadingMessages: true,
});

export const onLoadMessagesSuccessAction = (state: ChatStateType, action: ChatLoadMessagesSuccessActionType): ChatStateType => {
  const groupedMessages: ChatGroupedMessagesListType = getMessagesList(state).toJS();

  // TODO: MM: _.last will return undefined if groupedMessages is empty array but FlowType doesn't complain :(
  let lastGroup: ChatMessagesListGroupType = _last(groupedMessages);

  // TODO: PP: create a separate function to use in forEach
  action.payload.results.forEach((apiItem: ChatMessageSerializerType) => {
    // TODO: MM: find better name for functions that convert from one format to another
    // PP: use convertToXXX
    const formattedItem: ChatMessageType = parseMessage(apiItem);

    if (!lastGroup || (lastGroup.sender && apiItem.senderData && lastGroup.sender.id !== apiItem.senderData.id) || lastGroup.type !== apiItem.type || !apiItem.senderData || (apiItem.senderData && !lastGroup.sender)) {
      lastGroup = createMessagesListGroup();
      groupedMessages.push(lastGroup);
      lastGroup.sender = parseSender(apiItem.senderData);
      lastGroup.showOnRight = apiItem.isOwn;
      lastGroup.type = apiItem.type;
    }
    lastGroup.items.push(formattedItem);
  });

  return state.merge({
    loadingMessages: false,
    messages: groupedMessages,
  });
};

export const onSendMessageAction = (state: ChatStateType, action: ChatSendMessageActionType): ChatStateType => {
  const groupedMessages: ChatGroupedMessagesListType = getMessagesList(state).toJS();
  let lastGroup: ChatMessagesListGroupType = _last(groupedMessages);
  const formattedItem: ChatMessageType = createMessage(action.payload);

  if (!lastGroup || lastGroup.sender || !lastGroup.showOnRight) {
    lastGroup = createMessagesListGroup();
    lastGroup.showOnRight = true;
    lastGroup.type = formattedItem.type;
    groupedMessages.push(lastGroup);
  }
  lastGroup.items.push(formattedItem);

  return state.merge({
    showAnimations: true,
    messages: groupedMessages,
  });
};

export const onReceiveWebsocketMessageAction = (state: ChatStateType, action: Action): ChatStateType => {
  const { data: message } = action.payload;
  const websocketType = action.payload.message;

  switch (websocketType) {
    // Single case for now, should go back to it after beforementioned chat updates
    case 'message_created_or_updated': {
      if (state.get('senderId') === _get(message, 'senderData.id')) {
        return state;
      }

      const messages = addIncomingMessage(state, message);

      return state.merge({
        messages,
      });
    }
    default:
      return state;
  }
};

export const addIncomingMessage = (state: ChatStateType, message: ChatMessageType) => addMessage(state, message, false);
export const addOutgoingMessage = (state: ChatStateType, message: ChatMessageType) => addMessage(state, message, true);

export const addMessage = (state: ChatStateType, message: ChatMessageType, showOnRight: boolean): ChatStateType => {
  let groupedMessages = getMessagesList(state);
  let lastGroup = groupedMessages.last();
  const immutableMessage = fromJS(message);

  // if there is no group or last group doesn't match
  // expected one, create new group
  if (!lastGroup || lastGroup.get('showOnRight') !== showOnRight || lastGroup.get('type') !== message.type) {
    lastGroup = createMessagesListGroup(
      showOnRight,
      null,
      [immutableMessage],
      message.type,
    );
  } else {
    let items = lastGroup.get('items');

    items = items.push(immutableMessage);

    lastGroup = lastGroup.merge({
      items,
    });

    groupedMessages = groupedMessages.pop();
  }

  return groupedMessages.push(
    fromJS(lastGroup)
  );
};

/* animations */

export const onChatBubbleAnimationStart = (state: ChatStateType): ChatStateType => state.mergeDeep({
  animations: {
    bubble: 'started',
  },
});

export const onChatBubbleAnimationEnd = (state: ChatStateType): ChatStateType => state.mergeDeep({
  animations: {
    bubble: 'ended',
  },
});

export const onChatSetSenderId = (state, action) => state.set('senderId', action.senderId);
