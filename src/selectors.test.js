/* @flow */

import _mapKeys from 'lodash/mapKeys';

import * as chatSelectors from './Chat/selectors';
import * as messageSelectors from './Message/selectors';
import * as schemaSelectors from './Schema/selectors';
import * as variablesPoolSelectors from './VariablesPool/selectors';
import * as allSelectors from './selectors';

describe('export: selectors', () => {
  it('should export all chat selectors', () => {
    expect(Object.keys(chatSelectors)).toMatchSnapshot();

    _mapKeys(
      chatSelectors,
      (selector, selectorName) => expect(allSelectors[selectorName]).toBe(chatSelectors[selectorName])
    );
  });

  it('should export all message selectors', () => {
    expect(Object.keys(messageSelectors)).toMatchSnapshot();

    _mapKeys(
      messageSelectors,
      (selector, selectorName) => expect(allSelectors[selectorName]).toBe(messageSelectors[selectorName])
    );
  });

  it('should export all schema selectors', () => {
    expect(Object.keys(schemaSelectors)).toMatchSnapshot();

    _mapKeys(
      schemaSelectors,
      (selector, selectorName) => expect(allSelectors[selectorName]).toBe(schemaSelectors[selectorName])
    );
  });

  it('should export all variables pool selectors', () => {
    expect(Object.keys(variablesPoolSelectors)).toMatchSnapshot();

    _mapKeys(
      variablesPoolSelectors,
      (selector, selectorName) => expect(allSelectors[selectorName]).toBe(variablesPoolSelectors[selectorName])
    );
  });
});
