/* @flow */

import apiClient from 'api-client';
import MockAdapter from 'axios-mock-adapter';

import {
  getChatSchema,
  sendQuestionReply,
  sendMessage,
} from './';

import { CHAT_MESSAGE_TYPE_TEXT } from '../Message/constants';

import type {
  ChatQuestionReplyType,
  ChatMessageType,
} from '../types';

describe('react-chat API', () => {
  let mock;

  beforeEach(() => {
    mock = new MockAdapter(apiClient);
  });

  describe('getChatSchema', () => {
    const getChatSchemaSuccessResponse = {
      name: 'Onboarding',
      published: false,
      version: 1,
      type: 'onboarding',
      deprecated: false,
      id: 2,
      groups: [
        {
          id: 1,
          schema: 2,
          position: 1,
          name: 'M: Customer initial page',
          questions: [
            {
              schema: 2,
              rangeTo: null,
              nextQid: null,
              meta: null,
              statesInitedWith: [],
              required: false,
              hasAttachments: false,
              qid: 10,
              choiceOpen: false,
              type: 'choice',
              group: 1,
              id: 3,
              text: 'Hey there! Thanks for visiting Roman. Before we get started, we need some basic information in order to proceed. No names or emails yet.',
              rangeFrom: null,
              position: 10,
              statesWith: [],
              answers: [
                {
                  question: 3,
                  nextQid: 20,
                  meta: null,
                  id: 1,
                  text: 'START ONLINE VISIT',
                  position: 1,
                },
                {
                  question: 3,
                  nextQid: 30,
                  meta: null,
                  id: 2,
                  text: 'START ONLINE VISIT',
                  position: 2,
                },
              ],
              attachments: [],
            },
          ],
        },
      ],
      questions: [],
    };

    it('should return promise and resolve it with getChatSchemaSuccessResponse', (done) => {
      const id = 1;

      mock.onGet(`/schemas/${id}`).reply(200, getChatSchemaSuccessResponse);

      getChatSchema(id)
        .then((response) => {
          expect(response.data).toEqual(getChatSchemaSuccessResponse);
          done();
        });
    });
  });

  describe('sendQuestionReply', () => {
    it('should return promise and resolve it with request response data', (done) => {
      const reply: ChatQuestionReplyType = {
        type: CHAT_MESSAGE_TYPE_TEXT,
        threadId: 10,
        question: 20,
        answers: [1, 2],
      };
      const successResponse = {
        created: 'RESPONSE_CREATED',
        isRead: false,
        hasAttachments: false,
        thread: 10,
        sender: 'RESPONSE_SENDER_TEXT',
        id: 100,
        text: 'RESPONSE_TEXT',
        type: 'RESPONSE_TYPE',
        senderData: {},
        attachmentsData: null,
        isOwn: false,
      };

      mock.onPost(`/threads/${reply.threadId}/messages`).reply(200, successResponse);

      sendQuestionReply(reply)
        .then((response) => {
          expect(response.data).toEqual(successResponse);
          done();
        });
    });
  });

  describe('sendMessage', () => {
    it('should return promise and resolve it with request response data', (done) => {
      const message: ChatMessageType = {
        threadId: 10,
        text: 'RESPONSE_TEXT',
        type: 'RESPONSE_TYPE',
      };
      const successResponse = {
        created: 'RESPONSE_CREATED',
        isRead: false,
        hasAttachments: false,
        thread: 10,
        sender: 'RESPONSE_SENDER_TEXT',
        id: 100,
        text: 'RESPONSE_TEXT',
        type: 'RESPONSE_TYPE',
        senderData: {},
        attachmentsData: null,
        isOwn: false,
      };

      mock.onPost(`/threads/${message.threadId}/messages`).reply(200, successResponse);

      sendMessage(message)
        .then((response) => {
          expect(response.data).toEqual(successResponse);
          done();
        });
    });
  });
});
