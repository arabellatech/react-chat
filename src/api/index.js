/* @flow */

import apiClient from 'api-client';
import {
  OPEN,
  CLOSED,
  WAITING_FOR_RESPONSE,
} from '../Thread/constants';
import type {
  ChatMessageType,
  ChatQuestionReplyType,
} from '../types';

export const getChatSchema = (id: number) => apiClient.get(`/schemas/${id}`);

export const getChatOnboardingSchema = () => apiClient.get('/public/onboarding-schema');

export const sendQuestionReply = (reply: ChatQuestionReplyType, requestConfig: object) => apiClient.post(`/threads/${reply.threadId}/messages`, reply, requestConfig);

export const sendMessage = (message: ChatMessageType) => apiClient.post(`/threads/${message.threadId}/messages`, message);

export function loadMessages(threadId: number, params: Object = { limit: 9999, filtering: 'type__in:message', ordering: 'id' }) {
  return apiClient.get(`/threads/${threadId}/messages`, { params });
}

export const threadOpen = (threadId: number) => apiClient.patch(`/threads/${threadId}`, {
  state: OPEN,
});

export const threadClose = (threadId: number) => apiClient.patch(`/threads/${threadId}`, {
  state: CLOSED,
});

export const threadWaitForResponse = (threadId: number) => apiClient.patch(`/threads/${threadId}`, {
  state: WAITING_FOR_RESPONSE,
});

export const threadRollback = (threadId: number, questionId: number, requestConfig: object) => apiClient.post(`/threads/${threadId}/rollback`, { threadId, question: questionId }, requestConfig);
