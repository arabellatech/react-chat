/* @flow */

import _flatten from 'lodash/fp/flatten';
import _flow from 'lodash/fp/flow';
import _get from 'lodash/fp/get';
import _isString from 'lodash/fp/isString';
import _map from 'lodash/fp/map';
import _pick from 'lodash/fp/pick';
import _camelCase from 'lodash/camelCase';
import _isNumber from 'lodash/isNumber';
import {
  CHAT_MESSAGE_TYPE_TEXT,
} from './Message/constants';

import type {
  ChatMessageType,
  ChatQuestionType,
} from './types';

const equationParamRegexp = /([a-z_]+)/gi;
const equationOperatorAndRegexp = /\band\b/gi;
const equationOperatorOrRegexp = /\bor\b/gi;


export const parseSchemaQuestion = (question: Object): ChatQuestionType => _pick([
  'id',
  'qid',
  'nextQid',
  'type',
  'showAsType',
  'choiceOpen',
  'text',
  'attachments',
  'answers', // TODO: MM: type needed to know what to expect
  'meta',
  'forkingRules',
  'variableModifications',
])(question);

export const parseSchema = _flow(
  _get('groups'),
  _map(
    _get('questions')
  ),
  _flatten,
  _map(
    parseSchemaQuestion
  ),
);

export const createMessage = (data: Object): ChatMessageType => ({
  feId: createMessageId(data), // TODO: MM: it uses the isOwn from the message to calculate the feId - to consider since we changed isOwn in groups to showOnRight
  threadId: data.threadId,
  created: new Date().toISOString(),
  editable: false,
  status: 'loading',
  type: data.showAsType || CHAT_MESSAGE_TYPE_TEXT,
  text: data.text,
  meta: data.meta,
  questionId: data.question || null,
  attachments: [],
});

export const getForkedQid = (forkingRules: Array<*>, variablesPool: Object) => {
  const matchingRule = forkingRules.find(
    ([qid, equation]) => calculateEquation(equation, variablesPool)
  );

  if (matchingRule) {
    return parseInt(matchingRule[0], 10);
  }

  return null;
};

const calculateEquation = (equation: string, variablesPool: Object = {}) => {
  const equationFunction = createEquationFunction(equation);
  let result;

  try {
    result =  equationFunction(variablesPool);
  } catch(e) {
    throw new Error(`Fork function error: ${equationFunction.toString()}`);
  }

  return result;
}

const createEquationFunction = (equation: string) => {
  const equationWithOperators = replaceStringOperators(equation);
  // call changeParamsToObject after replaceStringOperators,
  // this way we won't end up with equationParams.and and equationParams.or
  const equationWithPrefixedParams = changeParamsToObject(equationWithOperators);
  const functionBody = `return (${equationWithPrefixedParams})`;
  const equationFunction = new Function('equationParams', functionBody);

  return equationFunction;
}

// replace string operators with && || to
// make them work inside Function
// from:  forkVariable == 1 and forkOtherVariable == 2
// to:    forkVariable == 1 && forkOtherVariable == 2
// from:  forkVariable == 1 or forkOtherVariable == 2
// to:    forkVariable == 1 || forkOtherVariable == 2
const replaceStringOperators = (equation: string): string => equation
  .replace(equationOperatorAndRegexp, '&&')
  .replace(equationOperatorOrRegexp, '||');

// make all params object properties to
// avoid having undefined variable error
// from:  forkVariable == 1
// to:    equationParams.forkVariable == 1
const changeParamsToObject = (equation: string) => equation
  .replace(equationParamRegexp, 'equationParams.$1');

export const createMessageId = (data: Object): string => {
  const {
    id,
    isOwn,
    qid,
  } = data;

  if (qid) {
    // SQR - schema question reply
    // SQM - schema question message
    return isOwn ? `SQR:${qid}` : `SQM:${qid}`;
  }

  // UOM - user outgoing message
  // UIM - user incoming message
  // TODO: set some unique id
  return isOwn ? 'UOM:someUniqueID' : `UIM:${id}`;
};

export const readQidFromFeId = (feId?: string) => {
  if (!feId || !_isString(feId)) {
    return null;
  }

  const qid = feId.match(/SQ[MR]:([0-9]+)/);

  if (qid) {
    return Number(qid[1]);
  }

  return null;
};
