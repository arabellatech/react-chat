/* @flow */

import chatReducer from './reducer';

describe('chatReducer', () => {
  let initialState;


  beforeEach(() => {
    // $FlowExpectedError
    Date.now = jest.fn(() => 1487076708000); // 2017-02-14T13:51:48+01:00

    initialState = chatReducer(undefined, {});
  });


  describe('unsupported action', () => {
    it('should return the initial state when called with { type: UNSUPPORTED_TYPE }', () => {
      expect(chatReducer(undefined, { type: 'UNSUPPORTED_TYPE' })).toEqual(initialState);
      expect(chatReducer(initialState, { type: 'UNSUPPORTED_TYPE' })).toEqual(initialState);
    });
  });
});
