/* @flow */

import React, { PureComponent } from 'react';
import _get from 'lodash/get';
import fecha from 'fecha';
import linkify from 'html-linkify';
import { AllHtmlEntities as Entities } from 'html-entities';
import nl2br from 'nl2br';
import BubbleUgly from '../BubbleUgly';
import Avatar from '../Avatar';
import type {
  ChatMessageType,
  ChatMessagesGroupProps,
} from '../types';

const entities = new Entities();

export default class MessagesGroup extends PureComponent {
  props: ChatMessagesGroupProps;

  getGroupClassName() {
    const {
      messagesGroup,
    } = this.props;
    const classNames = ['chat-msg'];

    if (messagesGroup.showOnRight) {
      classNames.push('chat-msg--answer');
    }

    if (messagesGroup.sender && messagesGroup.sender.role) {
      classNames.push(`chat-msg--${messagesGroup.sender.role}`);
    }

    return classNames.join(' ');
  }

  parseText(message: ChatMessageType) {
    if (!message.text) {
      return '';
    }

    const { textProcessor } = this.props;
    let { customVariables } = this.props;
    let results = message.text;

    results = linkify(results, {
      attributes: { target: '_blank' },
      nl2br: true,
    });
    // Yes, we need to decode entities two times.
    // We get encoded entities from backend
    // and then linkify encodes them the second time.
    // E.g. backend encode space to "&nbsp;" and linkify "&nbsp;" to "&amp;nbsp"
    results = entities.decode(entities.decode(results));
    results = nl2br(results);

    if (textProcessor) {
      results = textProcessor(results, message);
    }

    if (!customVariables || !message.meta || !message.meta.customVariables) {
      return results;
    }

    customVariables = customVariables.toJS();

    // $FlowFixMe
    message.meta.customVariables.forEach((variable) => {
      if (customVariables[variable]) {
        results = results.replace(`{{${variable}}}`, customVariables[variable]);
      }
    });

    return results;
  }

  props: ChatMessagesGroupProps;

  getSenderData() {
    const {
      messagesGroup,
      incomingSender,
      outgoingSender,
    } = this.props;

    let senderData = null;

    if (messagesGroup.sender) {
      senderData = messagesGroup.sender;
    } else {
      senderData = messagesGroup.showOnRight ? outgoingSender : incomingSender;
    }

    return senderData;
  }

  renderAvatar() {
    const {
      messagesGroup,
      showAvatarsOnRight,
      renderAvatar,
      renderNoSenderAvatar,
    } = this.props;

    const senderData = this.getSenderData();

    if (!senderData && renderNoSenderAvatar) {
      return renderNoSenderAvatar();
    }

    if (!senderData || (messagesGroup.showOnRight && !showAvatarsOnRight)) {
      return null;
    }

    if (renderAvatar) {
      return renderAvatar(senderData);
    }

    return (
      <Avatar
        alt={senderData.firstName}
        src={senderData.photo}
      />
    );
  }

  renderMessageFooter = () => {
    const { renderMessageFooter } = this.props;

    if (!renderMessageFooter) {
      return null;
    }

    const senderData = this.getSenderData();

    return renderMessageFooter(senderData);
  }

  renderBubbles() {
    const {
      messagesGroup: {
        items,
      },
    } = this.props;

    return items.map(this.renderBubble);
  }

  renderBubble = (message: ChatMessageType, key: number) => {
    const {
      showAnimations,
      messagesGroup,
      onEditClick,
      messageTimeFormat,
      renderTimestamp,
      renderAdditionalMessageContent,
    } = this.props;

    const mediaUrl = _get(message, 'meta.thumbnail');
    const handleEditClick = messagesGroup.showOnRight ? onEditClick : null;

    return (
      <BubbleUgly
        feId={message.feId}
        key={key}
        mediaUrl={mediaUrl}
        message={message}
        messagesGroup={messagesGroup}
        onEditClick={handleEditClick}
        questionId={message.questionId}
        renderAdditionalMessageContent={renderAdditionalMessageContent}
        renderMessageFooter={this.renderMessageFooter}
        renderTimestamp={renderTimestamp}
        showAnimations={showAnimations}
        showMarchingAnts={!messagesGroup.showOnRight}
        time={message.created}
        timeFormat={messageTimeFormat}
      >
        {this.parseText(message)}
      </BubbleUgly>
    );
  }

  renderDate(timestamp: ?string) {
    const {
      messageDateFormat,
    } = this.props;

    if (!messageDateFormat || !timestamp) {
      return null;
    }

    const dateFormatted = fecha.format(timestamp, messageDateFormat);

    return (
      <div className="chat-msg-date" key={dateFormatted}>
        {dateFormatted}
      </div>
    );
  }

  render() {
    const className = this.getGroupClassName();
    const bubbles = this.renderBubbles();
    const avatar = this.renderAvatar();

    return (
      <div className={className}>
        {avatar}
        <div className="chat-msg-main">
          {bubbles}
        </div>
      </div>
    );
  }
}
