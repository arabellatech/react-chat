'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _sagas = require('./Message/sagas');

var _sagas2 = _interopRequireDefault(_sagas);

var _sagas3 = require('./Schema/sagas');

var _sagas4 = _interopRequireDefault(_sagas3);

var _sagas5 = require('./Thread/sagas');

var _sagas6 = _interopRequireDefault(_sagas5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

exports.default = [].concat(_toConsumableArray(_sagas2.default), _toConsumableArray(_sagas4.default), _toConsumableArray(_sagas6.default));