'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _bindAll2 = require('lodash/bindAll');

var _bindAll3 = _interopRequireDefault(_bindAll2);

var _constants = require('../Message/constants');

var _actions = require('../Message/actions');

var _actions2 = require('../Thread/actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MessageForm = function (_PureComponent) {
  _inherits(MessageForm, _PureComponent);

  function MessageForm(props) {
    _classCallCheck(this, MessageForm);

    var _this = _possibleConstructorReturn(this, (MessageForm.__proto__ || Object.getPrototypeOf(MessageForm)).call(this, props));

    var threadId = props.threadId;


    if (!threadId) {
      throw Error('props.threadId is required!');
    }

    _this.state = {
      text: ''
    };

    (0, _bindAll3.default)(_this, ['onSubmit', 'setText']);
    return _this;
  }

  _createClass(MessageForm, [{
    key: 'onSubmit',
    value: function onSubmit(event, options) {
      if (event && event.preventDefault) {
        event.preventDefault();
      }

      if (this.state.text.length > 0) {
        this.sendMessage(options);
        this.setText('');
      }

      if (!this.state.text.length && options) {
        this.updateThreadStatus(options);
      }
    }
  }, {
    key: 'setText',
    value: function setText(text) {
      this.setState({
        text: text
      });
    }
  }, {
    key: 'updateThreadStatus',
    value: function updateThreadStatus(options) {
      var _props = this.props,
          waitForResponse = _props.waitForResponse,
          openThread = _props.openThread,
          closeThread = _props.closeThread,
          threadId = _props.threadId;


      if (options && options.waitForResponse) {
        waitForResponse(threadId);
      } else if (options && options.openThread) {
        openThread(threadId);
      } else if (options && options.closeThread) {
        closeThread(threadId);
      }
    }
  }, {
    key: 'sendMessage',
    value: function sendMessage(options) {
      var message = this.composeMessage();
      var _props2 = this.props,
          sendMessage = _props2.sendMessage,
          sendMessageAndWaitForResponse = _props2.sendMessageAndWaitForResponse,
          sendMessageAndOpenThread = _props2.sendMessageAndOpenThread,
          sendMessageAndCloseThread = _props2.sendMessageAndCloseThread;


      if (options && options.waitForResponse) {
        sendMessageAndWaitForResponse(message);
      } else if (options && options.openThread) {
        sendMessageAndOpenThread(message);
      } else if (options && options.closeThread) {
        sendMessageAndCloseThread(message);
      } else {
        sendMessage(message);
      }
    }
  }, {
    key: 'composeMessage',
    value: function composeMessage() {
      var threadId = this.props.threadId;

      var message = {
        threadId: threadId,
        text: this.state.text,
        type: _constants.CHAT_MESSAGE_TYPE_TEXT
      };

      return message;
    }
  }, {
    key: 'composeProps',
    value: function composeProps() {
      var _props3 = this.props,
          threadId = _props3.threadId,
          parentProps = _objectWithoutProperties(_props3, ['threadId']);

      var props = _extends({}, parentProps, {
        onSubmit: this.onSubmit,
        setText: this.setText,
        text: this.state.text
      });

      return props;
    }
  }, {
    key: 'render',
    value: function render() {
      var props = this.composeProps();
      var Component = this.props.component;

      return _react2.default.createElement(Component, props);
    }
  }]);

  return MessageForm;
}(_react.PureComponent);

// $FlowFix - flow doesn't like null in mapStateToProps


var mapStateToProps = function mapStateToProps() {
  return {};
};

var mapDispatchToProps = {
  sendMessage: _actions.chatSendMessageAction,
  closeThread: _actions2.chatThreadCloseAction,
  openThread: _actions2.chatThreadOpenAction,
  waitForResponse: _actions2.chatThreadWaitForResponseAction,
  sendMessageAndCloseThread: _actions.chatSendMessageAndCloseThreadAction,
  sendMessageAndOpenThread: _actions.chatSendMessageAndOpenThreadAction,
  sendMessageAndWaitForResponse: _actions.chatSendMessageAndWaitForResponseAction
};

// override state and dispatch props with own props
var mergeProps = function mergeProps(stateProps, dispatchProps, ownProps) {
  return _extends({}, stateProps, dispatchProps, ownProps);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, mergeProps)(MessageForm);