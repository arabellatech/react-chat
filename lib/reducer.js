'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _reduxWebsocket = require('@giantmachines/redux-websocket');

var _constants = require('./Schema/constants');

var _constants2 = require('./VariablesPool/constants');

var _constants3 = require('./Message/constants');

var _reducer = require('./Message/reducer');

var _reducer2 = require('./Schema/reducer');

var _reducer3 = require('./VariablesPool/reducer');

var _initialState = require('./initialState');

var _initialState2 = _interopRequireDefault(_initialState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function chatReducer() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _initialState2.default;
  var action = arguments[1];

  switch (action.type) {
    case _constants.CHAT_SET_THREAD_ID_ACTION:
      return (0, _reducer.onSetThreadIdAction)(state, action);

    case _constants3.CHAT_RESET_MESSAGES_ACTION:
      return (0, _reducer.onResetMessagesAction)(state, action);

    case _constants3.CHAT_LOAD_MESSAGES_ACTION:
      return (0, _reducer.onLoadMessagesAction)(state, action);
    case _constants3.CHAT_LOAD_MESSAGES_SUCCESS_ACTION:
      return (0, _reducer.onLoadMessagesSuccessAction)(state, action);
    case _constants3.CHAT_SEND_MESSAGE_ACTION:
      return (0, _reducer.onSendMessageAction)(state, action);
    case _reduxWebsocket.WEBSOCKET_MESSAGE:
      return (0, _reducer.onReceiveWebsocketMessageAction)(state, action);

    case _constants.CHAT_LOAD_SCHEMA_ACTION:
    case _constants.CHAT_LOAD_ONBOARDING_SCHEMA_ACTION:
      return (0, _reducer2.onSchemaLoad)(state, action);
    case _constants.CHAT_LOAD_SCHEMA_SUCCESS_ACTION:
    case _constants.CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION:
      return (0, _reducer2.onSchemaLoadSuccess)(state, action);
    case _constants.CHAT_LOAD_SCHEMA_FAILED_ACTION:
    case _constants.CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION:
      return (0, _reducer2.onSchemaLoadFailed)(state, action);

    case _constants.CHAT_SCHEMA_QUEUE_STARTED_ACTION:
      return (0, _reducer2.onChatSchemaQueueStarted)(state);
    case _constants.CHAT_SCHEMA_QUEUE_ENDED_ACTION:
      return (0, _reducer2.onChatSchemaQueueEnded)(state);
    case _constants.CHAT_SCHEMA_QUEUE_FAILED_ACTION:
      return (0, _reducer2.onChatSchemaQueueFailed)(state, action);

    case _constants.CHAT_SELECT_ANSWER_ACTION:
      return (0, _reducer2.onAnswerSelect)(state, action);
    case _constants.CHAT_UNSELECT_ANSWER_ACTION:
      return (0, _reducer2.onAnswerUnselect)(state, action);
    case _constants.CHAT_SET_ANSWER_ACTION:
      return (0, _reducer2.onAnswerSet)(state, action);

    case _constants.CHAT_SEND_QUESTION_REPLY_ACTION:
      return (0, _reducer2.onSendQuestionReply)(state, action);
    case _constants.CHAT_CHANGE_QUESTION_ACTION:
      return (0, _reducer2.onChangeQuestion)(state, action);
    case _constants.CHAT_NEXT_QUESTION_ACTION:
      return (0, _reducer2.onNextQuestionAction)(state);
    case _constants.CHAT_THREAD_ROLLBACK_ACTION:
      return (0, _reducer2.onThreadRollbackQuestionAction)(state, action);

    case _constants.CHAT_QUESTION_ANIMATION_START:
      return (0, _reducer2.onChatQuestionAnimationStart)(state, action.animation);
    case _constants.CHAT_QUESTION_ANIMATION_END:
      return (0, _reducer2.onChatQuestionAnimationEnd)(state, action.animation);

    case _constants3.CHAT_BUBBLE_ANIMATION_START:
      return (0, _reducer.onChatBubbleAnimationStart)(state);
    case _constants3.CHAT_BUBBLE_ANIMATION_END:
      return (0, _reducer.onChatBubbleAnimationEnd)(state);

    case _constants3.CHAT_SET_SENDER_ID_ACTION:
      return (0, _reducer.onChatSetSenderId)(state, action);

    case _constants2.CHAT_SET_VARIABLES_POOL_ACTION:
      return (0, _reducer3.onChatSetVariablesPoolAction)(state, action);

    case _constants2.CHAT_SET_CUSTOM_VARIABLES_ACTION:
      return (0, _reducer3.onChatSetCustomVariablesAction)(state, action);

    case _constants.CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION:
      return (0, _reducer2.onChatSchemaQueueSetRequestsProgressAction)(state, action);

    default:
      return state;
  }
}
// $FlowWTF
exports.default = chatReducer;