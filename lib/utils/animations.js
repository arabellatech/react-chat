'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStartedAnimations = exports.stickToBottom = undefined;

var _immutable = require('immutable');

var stickToBottom = exports.stickToBottom = function stickToBottom(target) {
  var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2000;

  var targetCopy = target;
  var canPlay = false;
  var lastTop = 0;

  canPlay = time > 0;

  function tick() {
    if (target) {
      var destScrollPos = target.scrollHeight - target.clientHeight;

      if (lastTop < destScrollPos) {
        lastTop = destScrollPos;
        targetCopy.scrollTop = destScrollPos;
      }
    }

    if (canPlay) {
      requestAnimationFrame(tick);
    }
  }

  tick();

  setTimeout(function () {
    canPlay = false;
  }, time);
};

var getStartedAnimations = exports.getStartedAnimations = function getStartedAnimations(animations, nextAnimations) {
  var startedAnimations = nextAnimations.filter(function () {
    var animationProgress = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
    var animationType = arguments[1];
    return animations.get(animationType) !== animationProgress && animationProgress.indexOf('started') > -1;
  });

  return startedAnimations;
};