'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _fecha = require('fecha');

var _fecha2 = _interopRequireDefault(_fecha);

var _htmlLinkify = require('html-linkify');

var _htmlLinkify2 = _interopRequireDefault(_htmlLinkify);

var _htmlEntities = require('html-entities');

var _nl2br = require('nl2br');

var _nl2br2 = _interopRequireDefault(_nl2br);

var _BubbleUgly = require('../BubbleUgly');

var _BubbleUgly2 = _interopRequireDefault(_BubbleUgly);

var _Avatar = require('../Avatar');

var _Avatar2 = _interopRequireDefault(_Avatar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var entities = new _htmlEntities.AllHtmlEntities();

var MessagesGroup = function (_PureComponent) {
  _inherits(MessagesGroup, _PureComponent);

  function MessagesGroup() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, MessagesGroup);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = MessagesGroup.__proto__ || Object.getPrototypeOf(MessagesGroup)).call.apply(_ref, [this].concat(args))), _this), _this.renderMessageFooter = function () {
      var renderMessageFooter = _this.props.renderMessageFooter;


      if (!renderMessageFooter) {
        return null;
      }

      var senderData = _this.getSenderData();

      return renderMessageFooter(senderData);
    }, _this.renderBubble = function (message, key) {
      var _this$props = _this.props,
          showAnimations = _this$props.showAnimations,
          messagesGroup = _this$props.messagesGroup,
          onEditClick = _this$props.onEditClick,
          messageTimeFormat = _this$props.messageTimeFormat,
          renderTimestamp = _this$props.renderTimestamp,
          renderAdditionalMessageContent = _this$props.renderAdditionalMessageContent;


      var mediaUrl = (0, _get3.default)(message, 'meta.thumbnail');
      var handleEditClick = messagesGroup.showOnRight ? onEditClick : null;

      return _react2.default.createElement(
        _BubbleUgly2.default,
        {
          feId: message.feId,
          key: key,
          mediaUrl: mediaUrl,
          message: message,
          messagesGroup: messagesGroup,
          onEditClick: handleEditClick,
          questionId: message.questionId,
          renderAdditionalMessageContent: renderAdditionalMessageContent,
          renderMessageFooter: _this.renderMessageFooter,
          renderTimestamp: renderTimestamp,
          showAnimations: showAnimations,
          showMarchingAnts: !messagesGroup.showOnRight,
          time: message.created,
          timeFormat: messageTimeFormat
        },
        _this.parseText(message)
      );
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(MessagesGroup, [{
    key: 'getGroupClassName',
    value: function getGroupClassName() {
      var messagesGroup = this.props.messagesGroup;

      var classNames = ['chat-msg'];

      if (messagesGroup.showOnRight) {
        classNames.push('chat-msg--answer');
      }

      if (messagesGroup.sender && messagesGroup.sender.role) {
        classNames.push('chat-msg--' + messagesGroup.sender.role);
      }

      return classNames.join(' ');
    }
  }, {
    key: 'parseText',
    value: function parseText(message) {
      if (!message.text) {
        return '';
      }

      var textProcessor = this.props.textProcessor;
      var customVariables = this.props.customVariables;

      var results = message.text;

      results = (0, _htmlLinkify2.default)(results, {
        attributes: { target: '_blank' },
        nl2br: true
      });
      // Yes, we need to decode entities two times.
      // We get encoded entities from backend
      // and then linkify encodes them the second time.
      // E.g. backend encode space to "&nbsp;" and linkify "&nbsp;" to "&amp;nbsp"
      results = entities.decode(entities.decode(results));
      results = (0, _nl2br2.default)(results);

      if (textProcessor) {
        results = textProcessor(results, message);
      }

      if (!customVariables || !message.meta || !message.meta.customVariables) {
        return results;
      }

      customVariables = customVariables.toJS();

      // $FlowFixMe
      message.meta.customVariables.forEach(function (variable) {
        if (customVariables[variable]) {
          results = results.replace('{{' + variable + '}}', customVariables[variable]);
        }
      });

      return results;
    }
  }, {
    key: 'getSenderData',
    value: function getSenderData() {
      var _props = this.props,
          messagesGroup = _props.messagesGroup,
          incomingSender = _props.incomingSender,
          outgoingSender = _props.outgoingSender;


      var senderData = null;

      if (messagesGroup.sender) {
        senderData = messagesGroup.sender;
      } else {
        senderData = messagesGroup.showOnRight ? outgoingSender : incomingSender;
      }

      return senderData;
    }
  }, {
    key: 'renderAvatar',
    value: function renderAvatar() {
      var _props2 = this.props,
          messagesGroup = _props2.messagesGroup,
          showAvatarsOnRight = _props2.showAvatarsOnRight,
          renderAvatar = _props2.renderAvatar,
          renderNoSenderAvatar = _props2.renderNoSenderAvatar;


      var senderData = this.getSenderData();

      if (!senderData && renderNoSenderAvatar) {
        return renderNoSenderAvatar();
      }

      if (!senderData || messagesGroup.showOnRight && !showAvatarsOnRight) {
        return null;
      }

      if (renderAvatar) {
        return renderAvatar(senderData);
      }

      return _react2.default.createElement(_Avatar2.default, {
        alt: senderData.firstName,
        src: senderData.photo
      });
    }
  }, {
    key: 'renderBubbles',
    value: function renderBubbles() {
      var items = this.props.messagesGroup.items;


      return items.map(this.renderBubble);
    }
  }, {
    key: 'renderDate',
    value: function renderDate(timestamp) {
      var messageDateFormat = this.props.messageDateFormat;


      if (!messageDateFormat || !timestamp) {
        return null;
      }

      var dateFormatted = _fecha2.default.format(timestamp, messageDateFormat);

      return _react2.default.createElement(
        'div',
        { className: 'chat-msg-date', key: dateFormatted },
        dateFormatted
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var className = this.getGroupClassName();
      var bubbles = this.renderBubbles();
      var avatar = this.renderAvatar();

      return _react2.default.createElement(
        'div',
        { className: className },
        avatar,
        _react2.default.createElement(
          'div',
          { className: 'chat-msg-main' },
          bubbles
        )
      );
    }
  }]);

  return MessagesGroup;
}(_react.PureComponent);

exports.default = MessagesGroup;