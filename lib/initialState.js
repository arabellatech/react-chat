'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _immutable = require('immutable');

exports.default = (0, _immutable.fromJS)({
  loadingSchema: false,
  loadingMessages: false,
  showAnimations: false,
  questions: [],
  currentQuestion: {
    answers: []
  },
  answeredQuestions: [],
  questionsReplies: [],
  schemaQueueStatus: {
    isStarted: false,
    isEnded: false,
    isFailed: false,
    failureReason: null,
    progress: 0
  },
  schemaId: null,
  messages: [],
  thread: null,
  threadId: null,
  animations: {},
  customVariables: {},
  variablesPool: {},
  variablesPoolHistory: []
});