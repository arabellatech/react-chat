'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectChatAnimations = exports.listPick = exports.filterByMultiple = exports.filterBy = exports.findBy = exports.getAnimations = exports.getChatDomain = undefined;

var _immutable = require('immutable');

var _reselect = require('reselect');

var _indexOf2 = require('lodash/indexOf');

var _indexOf3 = _interopRequireDefault(_indexOf2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* get */

var getChatDomain = exports.getChatDomain = function getChatDomain(state) {
  return state.get('chat');
};

var getAnimations = exports.getAnimations = function getAnimations(state) {
  return state.get('animations');
};

/* helpers */

var findBy = exports.findBy = function findBy(list, paramName, paramValue) {
  return list.find(function (item) {
    return item.get(paramName) === paramValue;
  });
};
var filterBy = exports.filterBy = function filterBy(list, paramName, paramValue) {
  return list.filter(function (item) {
    return item.get(paramName) === paramValue;
  });
};

var filterByMultiple = exports.filterByMultiple = function filterByMultiple(list, paramName, paramValues) {
  return list.filter(function (item) {
    return (0, _indexOf3.default)(paramValues, item.get(paramName)) > -1;
  });
};

var listPick = exports.listPick = function listPick(list, paramName) {
  return list.map(function (item) {
    return item.get(paramName);
  });
};

/* top level selectors (for use in mapStateToProps or global reducers) */

var selectChatAnimations = exports.selectChatAnimations = (0, _reselect.createSelector)(getChatDomain, getAnimations);