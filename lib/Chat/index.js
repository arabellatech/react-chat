'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _class, _temp2;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactResizeDetector = require('react-resize-detector');

var _reactResizeDetector2 = _interopRequireDefault(_reactResizeDetector);

var _immutable = require('immutable');

var _fecha = require('fecha');

var _fecha2 = _interopRequireDefault(_fecha);

var _isEmpty2 = require('lodash/isEmpty');

var _isEmpty3 = _interopRequireDefault(_isEmpty2);

var _map2 = require('lodash/map');

var _map3 = _interopRequireDefault(_map2);

var _MessagesGroup = require('../MessagesGroup');

var _MessagesGroup2 = _interopRequireDefault(_MessagesGroup);

var _actions = require('../Schema/actions');

var _actions2 = require('../Message/actions');

var _selectors = require('./selectors');

var _selectors2 = require('../VariablesPool/selectors');

var _selectors3 = require('../Message/selectors');

var _animations = require('../utils/animations');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    showAnimations: (0, _selectors3.selectChatShowAnimations)(state),
    loadingMessages: (0, _selectors3.selectChatLoadingMessagesList)(state),
    messages: (0, _selectors3.selectChatMessagesGrouppedByDate)(state),
    animations: (0, _selectors.selectChatAnimations)(state),
    customVariables: (0, _selectors2.selectChatCustomVariables)(state)
  };
};

var mapDispatchToProps = {
  loadMessages: _actions2.chatLoadMessagesAction,
  resetMessages: _actions2.chatResetMessagesAction,
  rollbackThread: _actions.chatThreadRollbackAction,
  setSenderId: _actions2.chatSetSenderIdAction
};

var Chat = (_temp2 = _class = function (_PureComponent) {
  _inherits(Chat, _PureComponent);

  function Chat() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Chat);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Chat.__proto__ || Object.getPrototypeOf(Chat)).call.apply(_ref, [this].concat(args))), _this), _this.chatWindowResize = function (width, height) {
      if (height < _this.currentChatWindowHeight) {
        _this.stickToBottom(0, true);
      }
      _this.currentChatWindowHeight = height;
    }, _this.stickToBottom = function () {
      var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
      var once = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      setTimeout(function () {
        (0, _animations.stickToBottom)(_this.chatWindow, time, once);
      }, 0);
    }, _this.setChatWindowRef = function (chatWindow) {
      _this.chatWindow = chatWindow;

      if (chatWindow) {
        _this.currentChatWindowHeight = chatWindow.offsetHeight;
      }
    }, _this.renderMessagesGroupsWithDate = function (messagesGroups, date) {
      var datePartial = _this.renderDate(date);
      var groupsPartial = messagesGroups.map(_this.renderMessagesGroup);

      return _react2.default.createElement(
        _react.Fragment,
        { key: date },
        datePartial,
        groupsPartial
      );
    }, _this.renderMessagesGroup = function (messagesGroup, key) {
      var _this$props = _this.props,
          incomingSender = _this$props.incomingSender,
          outgoingSender = _this$props.outgoingSender,
          showAnimations = _this$props.showAnimations,
          renderMessagesGroup = _this$props.renderMessagesGroup,
          customVariables = _this$props.customVariables,
          messageDateFormat = _this$props.messageDateFormat,
          messageTimeFormat = _this$props.messageTimeFormat,
          showAvatarsOnRight = _this$props.showAvatarsOnRight,
          renderAvatar = _this$props.renderAvatar,
          renderNoSenderAvatar = _this$props.renderNoSenderAvatar,
          renderTimestamp = _this$props.renderTimestamp,
          renderMessageFooter = _this$props.renderMessageFooter;


      var groupProps = {
        incomingSender: incomingSender,
        outgoingSender: outgoingSender,
        showAnimations: showAnimations,
        customVariables: customVariables,
        key: key,
        messageDateFormat: messageDateFormat,
        messageTimeFormat: messageTimeFormat,
        messagesGroup: messagesGroup,
        showAvatarsOnRight: showAvatarsOnRight,
        renderAvatar: renderAvatar,
        renderNoSenderAvatar: renderNoSenderAvatar,
        renderTimestamp: renderTimestamp,
        renderMessageFooter: renderMessageFooter
      };
      var editClickHandler = _this.getOnEditClickHandler();

      if (editClickHandler) {
        groupProps.onEditClick = editClickHandler;
      }

      if (renderMessagesGroup) {
        return renderMessagesGroup(groupProps);
      }

      return _react2.default.createElement(_MessagesGroup2.default, groupProps);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Chat, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          loadMessages = _props.loadMessages,
          resetMessages = _props.resetMessages,
          senderId = _props.senderId,
          setSenderId = _props.setSenderId,
          threadId = _props.threadId;


      resetMessages();

      if (threadId) {
        loadMessages(threadId);
        setSenderId(senderId);
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var startedAnimations = (0, _animations.getStartedAnimations)(this.props.animations, nextProps.animations);

      if (this.props.senderId !== nextProps.senderId) {
        this.props.setSenderId(nextProps.senderId);
      }

      if (this.props.threadId && this.props.threadId !== nextProps.threadId) {
        this.props.resetMessages();
      }

      if (!(0, _isEmpty3.default)(startedAnimations) || this.props.messages.size === 0 && nextProps.messages.size > 0) {
        this.stickToBottom(1000);
      }
    }
  }, {
    key: 'getOnEditClickHandler',
    value: function getOnEditClickHandler() {
      var _props2 = this.props,
          onEditClick = _props2.onEditClick,
          threadId = _props2.threadId,
          rollbackThread = _props2.rollbackThread;


      if (!onEditClick) {
        return null;
      }

      var editHandler = function editHandler(feId, questionId) {
        return onEditClick(threadId, feId, questionId, rollbackThread);
      };

      return editHandler;
    }
  }, {
    key: 'renderEmptyList',
    value: function renderEmptyList() {
      var renderEmptyList = this.props.renderEmptyList;


      if (renderEmptyList) {
        return renderEmptyList();
      }

      return null;
    }
  }, {
    key: 'renderMessages',
    value: function renderMessages() {
      var messages = this.props.messages;


      if (!messages.size) {
        return this.renderEmptyList();
      }

      var messagesGroups = (0, _map3.default)(messages.toJS(), this.renderMessagesGroupsWithDate);

      return _react2.default.createElement(
        'div',
        { key: 'chatMessages', className: 'chat-window-wrap', ref: this.setChatWindowRef },
        _react2.default.createElement(
          'div',
          { className: 'chat-window' },
          _react2.default.createElement(
            'div',
            { className: 'chat-window-inner' },
            messagesGroups
          )
        ),
        _react2.default.createElement(_reactResizeDetector2.default, { handleHeight: true, onResize: this.chatWindowResize })
      );
    }
  }, {
    key: 'renderDate',
    value: function renderDate(date) {
      var messageDateFormat = this.props.messageDateFormat;


      if (!messageDateFormat || !date || date === 'NO_DATE') {
        return null;
      }

      var dateFormatted = _fecha2.default.format(date, messageDateFormat);

      return _react2.default.createElement(
        'div',
        { className: 'chat-msg-date' },
        dateFormatted
      );
    }
  }, {
    key: 'renderMessageForm',
    value: function renderMessageForm() {
      var _props3 = this.props,
          threadId = _props3.threadId,
          renderMessageForm = _props3.renderMessageForm;


      if (!renderMessageForm) {
        return null;
      }

      return renderMessageForm(threadId);
    }
  }, {
    key: 'render',
    value: function render() {
      if (this.props.loadingMessages) {
        return this.renderEmptyList();
      }

      var messagesList = this.renderMessages();
      var messageForm = this.renderMessageForm();

      return _react2.default.createElement(
        'div',
        { className: 'chat' },
        messagesList,
        messageForm
      );
    }
  }]);

  return Chat;
}(_react.PureComponent), _class.defaultProps = {
  messages: (0, _immutable.Map)(),
  threadId: null,
  loadingMessages: false,
  messageDateFormat: null,
  messageTimeFormat: null,
  showAvatarsOnRight: false
}, _temp2);

// override state and dispatch props with own props

var mergeProps = function mergeProps(stateProps, dispatchProps, ownProps) {
  return _extends({}, stateProps, dispatchProps, ownProps);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, mergeProps)(Chat);