'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchChatLoadMessagesSaga = watchChatLoadMessagesSaga;
exports.chatLoadMessagesSaga = chatLoadMessagesSaga;
exports.watchChatSendMessageAndUpdateThreadSaga = watchChatSendMessageAndUpdateThreadSaga;
exports.chatTriggerSendMessageSaga = chatTriggerSendMessageSaga;
exports.watchChatSendMessageSaga = watchChatSendMessageSaga;
exports.chatSendMessageSaga = chatSendMessageSaga;

var _effects = require('redux-saga/effects');

var _actions = require('./actions');

var _constants = require('./constants');

var _api = require('../api');

var _marked = /*#__PURE__*/regeneratorRuntime.mark(watchChatLoadMessagesSaga),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(chatLoadMessagesSaga),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSendMessageAndUpdateThreadSaga),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(chatTriggerSendMessageSaga),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSendMessageSaga),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(chatSendMessageSaga); // TODO: use flow

/*
import type {
  ChatLoadMessagesActionType,
  ChatSendMessageActionType,
  ChatThreadRollbackActionType,
} from './types';
*/

function watchChatLoadMessagesSaga() {
  return regeneratorRuntime.wrap(function watchChatLoadMessagesSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_LOAD_MESSAGES_ACTION, chatLoadMessagesSaga);

        case 2:
        case 'end':
          return _context.stop();
      }
    }
  }, _marked, this);
}

function chatLoadMessagesSaga(action /* : ChatLoadMessagesActionType */) {
  var response;
  return regeneratorRuntime.wrap(function chatLoadMessagesSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(_api.loadMessages, action.threadId);

        case 3:
          response = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)((0, _actions.chatLoadMessagesSuccessAction)(response.data));

        case 6:
          _context2.next = 12;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2['catch'](0);
          _context2.next = 12;
          return (0, _effects.put)((0, _actions.chatLoadMessagesFailedAction)(_context2.t0));

        case 12:
        case 'end':
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 8]]);
}

function watchChatSendMessageAndUpdateThreadSaga() {
  return regeneratorRuntime.wrap(function watchChatSendMessageAndUpdateThreadSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.takeEvery)([_constants.CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION, _constants.CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION, _constants.CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION], chatTriggerSendMessageSaga);

        case 2:
        case 'end':
          return _context3.stop();
      }
    }
  }, _marked3, this);
}

function chatTriggerSendMessageSaga(action) {
  return regeneratorRuntime.wrap(function chatTriggerSendMessageSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.put)((0, _actions.chatSendMessageAction)(action.payload));

        case 2:
        case 'end':
          return _context4.stop();
      }
    }
  }, _marked4, this);
}

function watchChatSendMessageSaga() {
  return regeneratorRuntime.wrap(function watchChatSendMessageSaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_SEND_MESSAGE_ACTION, chatSendMessageSaga);

        case 2:
        case 'end':
          return _context5.stop();
      }
    }
  }, _marked5, this);
}

function chatSendMessageSaga(action /* : ChatSendMessageActionType */) {
  var response;
  return regeneratorRuntime.wrap(function chatSendMessageSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.call)(_api.sendMessage, action.payload);

        case 3:
          response = _context6.sent;
          _context6.next = 6;
          return (0, _effects.put)((0, _actions.chatSendMessageSuccessAction)(response.data));

        case 6:
          _context6.next = 12;
          break;

        case 8:
          _context6.prev = 8;
          _context6.t0 = _context6['catch'](0);
          _context6.next = 12;
          return (0, _effects.put)((0, _actions.chatSendMessageFailedAction)(_context6.t0));

        case 12:
        case 'end':
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 8]]);
}

// All sagas to be loaded
exports.default = [watchChatLoadMessagesSaga, watchChatSendMessageAndUpdateThreadSaga, watchChatSendMessageSaga];