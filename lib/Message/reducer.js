'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.onChatSetSenderId = exports.onChatBubbleAnimationEnd = exports.onChatBubbleAnimationStart = exports.addMessage = exports.addOutgoingMessage = exports.addIncomingMessage = exports.onReceiveWebsocketMessageAction = exports.onSendMessageAction = exports.onLoadMessagesSuccessAction = exports.onLoadMessagesAction = exports.onResetMessagesAction = exports.onSetThreadIdAction = undefined;

var _immutable = require('immutable');

var _last2 = require('lodash/last');

var _last3 = _interopRequireDefault(_last2);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _selectors = require('./selectors');

var _utils = require('../utils');

var _formatter = require('./formatter');

var _initialState = require('../initialState');

var _initialState2 = _interopRequireDefault(_initialState);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* messages */

// $FlowWTF
var onSetThreadIdAction = exports.onSetThreadIdAction = function onSetThreadIdAction(state, action) {
  return state.merge({
    threadId: action.id
  });
};

var onResetMessagesAction = exports.onResetMessagesAction = function onResetMessagesAction(state) {
  return state.merge(_initialState2.default);
};

var onLoadMessagesAction = exports.onLoadMessagesAction = function onLoadMessagesAction(state) {
  return state.merge({
    loadingMessages: true
  });
};

var onLoadMessagesSuccessAction = exports.onLoadMessagesSuccessAction = function onLoadMessagesSuccessAction(state, action) {
  var groupedMessages = (0, _selectors.getMessagesList)(state).toJS();

  // TODO: MM: _.last will return undefined if groupedMessages is empty array but FlowType doesn't complain :(
  var lastGroup = (0, _last3.default)(groupedMessages);

  // TODO: PP: create a separate function to use in forEach
  action.payload.results.forEach(function (apiItem) {
    // TODO: MM: find better name for functions that convert from one format to another
    var formattedItem = (0, _formatter.parseMessage)(apiItem);

    if (!lastGroup || lastGroup.sender && apiItem.senderData && lastGroup.sender.id !== apiItem.senderData.id || lastGroup.type !== apiItem.type || !apiItem.senderData || apiItem.senderData && !lastGroup.sender) {
      lastGroup = (0, _formatter.createMessagesListGroup)();
      groupedMessages.push(lastGroup);
      lastGroup.sender = (0, _formatter.parseSender)(apiItem.senderData);
      lastGroup.showOnRight = apiItem.isOwn;
      lastGroup.type = apiItem.type;
    }
    lastGroup.items.push(formattedItem);
  });

  return state.merge({
    loadingMessages: false,
    messages: groupedMessages
  });
};

var onSendMessageAction = exports.onSendMessageAction = function onSendMessageAction(state, action) {
  var groupedMessages = (0, _selectors.getMessagesList)(state).toJS();
  var lastGroup = (0, _last3.default)(groupedMessages);
  var formattedItem = (0, _utils.createMessage)(action.payload);

  if (!lastGroup || lastGroup.sender || !lastGroup.showOnRight) {
    lastGroup = (0, _formatter.createMessagesListGroup)();
    lastGroup.showOnRight = true;
    lastGroup.type = formattedItem.type;
    groupedMessages.push(lastGroup);
  }
  lastGroup.items.push(formattedItem);

  return state.merge({
    showAnimations: true,
    messages: groupedMessages
  });
};

var onReceiveWebsocketMessageAction = exports.onReceiveWebsocketMessageAction = function onReceiveWebsocketMessageAction(state, action) {
  var message = action.payload.data;

  var websocketType = action.payload.message;

  switch (websocketType) {
    // Single case for now, should go back to it after beforementioned chat updates
    case 'message_created_or_updated':
      {
        if (state.get('senderId') === (0, _get3.default)(message, 'senderData.id')) {
          return state;
        }

        var messages = addIncomingMessage(state, message);

        return state.merge({
          messages: messages
        });
      }
    default:
      return state;
  }
};

var addIncomingMessage = exports.addIncomingMessage = function addIncomingMessage(state, message) {
  return addMessage(state, message, false);
};
var addOutgoingMessage = exports.addOutgoingMessage = function addOutgoingMessage(state, message) {
  return addMessage(state, message, true);
};

var addMessage = exports.addMessage = function addMessage(state, message, showOnRight) {
  var groupedMessages = (0, _selectors.getMessagesList)(state);
  var lastGroup = groupedMessages.last();
  var immutableMessage = (0, _immutable.fromJS)(message);

  // if there is no group or last group doesn't match
  // expected one, create new group
  if (!lastGroup || lastGroup.get('showOnRight') !== showOnRight || lastGroup.get('type') !== message.type) {
    lastGroup = (0, _formatter.createMessagesListGroup)(showOnRight, null, [immutableMessage], message.type);
  } else {
    var items = lastGroup.get('items');

    items = items.push(immutableMessage);

    lastGroup = lastGroup.merge({
      items: items
    });

    groupedMessages = groupedMessages.pop();
  }

  return groupedMessages.push((0, _immutable.fromJS)(lastGroup));
};

/* animations */

var onChatBubbleAnimationStart = exports.onChatBubbleAnimationStart = function onChatBubbleAnimationStart(state) {
  return state.mergeDeep({
    animations: {
      bubble: 'started'
    }
  });
};

var onChatBubbleAnimationEnd = exports.onChatBubbleAnimationEnd = function onChatBubbleAnimationEnd(state) {
  return state.mergeDeep({
    animations: {
      bubble: 'ended'
    }
  });
};

var onChatSetSenderId = exports.onChatSetSenderId = function onChatSetSenderId(state, action) {
  return state.set('senderId', action.senderId);
};