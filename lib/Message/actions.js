'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chatLoadMessagesAction = chatLoadMessagesAction;
exports.chatLoadMessagesSuccessAction = chatLoadMessagesSuccessAction;
exports.chatLoadMessagesFailedAction = chatLoadMessagesFailedAction;
exports.chatSendMessageAndCloseThreadAction = chatSendMessageAndCloseThreadAction;
exports.chatSendMessageAndOpenThreadAction = chatSendMessageAndOpenThreadAction;
exports.chatSendMessageAndWaitForResponseAction = chatSendMessageAndWaitForResponseAction;
exports.chatSendMessageAction = chatSendMessageAction;
exports.chatSendMessageSuccessAction = chatSendMessageSuccessAction;
exports.chatSendMessageFailedAction = chatSendMessageFailedAction;
exports.chatBubbleAnimationStartAction = chatBubbleAnimationStartAction;
exports.chatBubbleAnimationEndAction = chatBubbleAnimationEndAction;
exports.chatResetMessagesAction = chatResetMessagesAction;
exports.chatSetSenderIdAction = chatSetSenderIdAction;

var _constants = require('./constants');

function chatLoadMessagesAction(threadId) {
  return {
    type: _constants.CHAT_LOAD_MESSAGES_ACTION,
    threadId: threadId
  };
}

function chatLoadMessagesSuccessAction(payload) {
  return {
    type: _constants.CHAT_LOAD_MESSAGES_SUCCESS_ACTION,
    payload: payload
  };
}

function chatLoadMessagesFailedAction(payload) {
  return {
    type: _constants.CHAT_LOAD_MESSAGES_FAILED_ACTION,
    payload: payload
  };
}

function chatSendMessageAndCloseThreadAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION,
    payload: payload
  };
}

function chatSendMessageAndOpenThreadAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION,
    payload: payload
  };
}

function chatSendMessageAndWaitForResponseAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION,
    payload: payload
  };
}

function chatSendMessageAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_ACTION,
    payload: payload
  };
}

function chatSendMessageSuccessAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_SUCCESS_ACTION,
    payload: payload
  };
}

function chatSendMessageFailedAction(payload) {
  return {
    type: _constants.CHAT_SEND_MESSAGE_FAILED_ACTION,
    payload: payload
  };
}

function chatBubbleAnimationStartAction() {
  return {
    type: _constants.CHAT_BUBBLE_ANIMATION_START
  };
}

function chatBubbleAnimationEndAction() {
  return {
    type: _constants.CHAT_BUBBLE_ANIMATION_END
  };
}

function chatResetMessagesAction() {
  return {
    type: _constants.CHAT_RESET_MESSAGES_ACTION
  };
}

function chatSetSenderIdAction(senderId) {
  return {
    type: _constants.CHAT_SET_SENDER_ID_ACTION,
    senderId: senderId
  };
}