'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseMessage = parseMessage;
exports.parseSender = parseSender;
exports.createMessagesListGroup = createMessagesListGroup;

var _pick2 = require('lodash/pick');

var _pick3 = _interopRequireDefault(_pick2);

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function parseMessage(item) {
  var delay = 0;
  if ((0, _get3.default)(item, 'meta.delay')) {
    delay = parseInt(item.meta.delay, 10);
  }

  return {
    id: item.id,
    feId: (0, _get3.default)(item, 'meta.feId'),
    threadId: item.thread,
    created: item.created,
    editable: false,
    status: 'loading',
    type: item.type,
    text: item.text,
    delay: delay,
    meta: (0, _get3.default)(item, 'meta'),
    attachments: item.attachmentsData
  };
}

// TODO: decide whether or not we should remove this function
function parseSender(item) {
  return item;
  // return _pick(
  //   item,
  //   [
  //     'id',
  //     'photo',
  //     'firstName',
  //     'lastName',
  //     'role',
  //   ]
  // );
}

function createMessagesListGroup() {
  var showOnRight = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var sender = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : undefined;
  var items = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];
  var type = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : '';

  return {
    showOnRight: showOnRight,
    sender: sender,
    items: items,
    type: type
  };
}