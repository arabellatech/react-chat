'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectChatThreadId = exports.selectChatShowAnimations = exports.selectChatLoadingMessagesList = exports.selectChatMessagesGrouppedByDate = exports.selectChatMessagesList = exports.selectMessagesByDate = exports.selectMessagesGrouppedByDate = exports.makeSelectMessagesListUpToFeId = exports.selectLastGroupFromMessagesList = exports.getFirstMessageFeId = exports.getMessageDate = exports.getMessagesFromGroup = exports.getChatThreadId = exports.getChatShowAnimations = exports.getLoadingMessagesList = exports.getMessagesList = undefined;

var _immutable = require('immutable');

var _reselect = require('reselect');

var _selectors = require('../Chat/selectors');

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* getters */

var getMessagesList = exports.getMessagesList = function getMessagesList(state) {
  return state.get('messages');
};
var getLoadingMessagesList = exports.getLoadingMessagesList = function getLoadingMessagesList(state) {
  return state.get('loadingMessages');
};
var getChatShowAnimations = exports.getChatShowAnimations = function getChatShowAnimations(state) {
  return state.get('showAnimations');
};
var getChatThreadId = exports.getChatThreadId = function getChatThreadId(state) {
  return state.get('threadId');
};
var getMessagesFromGroup = exports.getMessagesFromGroup = function getMessagesFromGroup(state) {
  return state.get('items');
};
var getMessageDate = exports.getMessageDate = function getMessageDate(state) {
  return state.get('created').substr(0, 10);
};
var getFirstMessageFeId = exports.getFirstMessageFeId = function getFirstMessageFeId(state) {
  return state.get('items').get(0).get('feId');
};

/* chat level selectors (for use in chat reducers) */

/* messages group related selectors */

var selectLastGroupFromMessagesList = exports.selectLastGroupFromMessagesList = (0, _reselect.createSelector)(getMessagesList, function (messages) {
  return messages.last();
});

var makeSelectMessagesListUpToFeId = exports.makeSelectMessagesListUpToFeId = function makeSelectMessagesListUpToFeId(feId) {
  return (0, _reselect.createSelector)(getMessagesList, function (messagesGroups) {
    var size = messagesGroups.size;
    var trimmedMessages = new _immutable.List();

    for (var i = 0; i < size; i += 1) {
      var currentGroup = messagesGroups.get(i);

      if (getFirstMessageFeId(currentGroup) === feId) {
        break;
      }

      trimmedMessages = trimmedMessages.push(currentGroup);
    }

    return trimmedMessages;
  });
};

var selectMessagesGrouppedByDate = exports.selectMessagesGrouppedByDate = (0, _reselect.createSelector)(getMessagesList, function (messagesGroups) {
  var messagesGrouppedByDate = (0, _immutable.OrderedMap)();

  var addGroupForDate = function addGroupForDate(messagesGroup, date) {
    var groupsForDate = messagesGrouppedByDate.get(date);

    if (groupsForDate) {
      groupsForDate = groupsForDate.push(messagesGroup);
    } else {
      groupsForDate = (0, _immutable.fromJS)([messagesGroup]);
    }

    messagesGrouppedByDate = messagesGrouppedByDate.merge(_defineProperty({}, date, groupsForDate));
  };

  var mapGroupToDates = function mapGroupToDates(messagesGroup) {
    var messagesByDate = selectMessagesByDate(messagesGroup);

    if (!messagesByDate) {
      return;
    }

    messagesByDate.forEach(function (messages, date) {
      var groupForDate = messagesGroup.set('items', messages);

      addGroupForDate(groupForDate, date);
    });
  };

  if (messagesGroups) {
    messagesGroups.forEach(mapGroupToDates);
  }

  return messagesGrouppedByDate;
});

var selectMessagesByDate = exports.selectMessagesByDate = function selectMessagesByDate(messagesGroup) {
  var messages = getMessagesFromGroup(messagesGroup);
  var messagesByDate = (0, _immutable.Map)();

  messages.forEach(function (message) {
    var date = getMessageDate(message);
    var messagesForDate = messagesByDate.get(date);

    if (!messagesForDate) {
      messagesForDate = (0, _immutable.fromJS)([message]);
    } else {
      messagesForDate = messagesForDate.push(message);
    }

    messagesByDate = messagesByDate.merge(_defineProperty({}, date, messagesForDate));
  });

  return messagesByDate;
};

/* top level selectors (for use in mapStateToProps or global reducers) */

var selectChatMessagesList = exports.selectChatMessagesList = (0, _reselect.createSelector)(_selectors.getChatDomain, getMessagesList);

var selectChatMessagesGrouppedByDate = exports.selectChatMessagesGrouppedByDate = (0, _reselect.createSelector)(_selectors.getChatDomain, selectMessagesGrouppedByDate);

var selectChatLoadingMessagesList = exports.selectChatLoadingMessagesList = (0, _reselect.createSelector)(_selectors.getChatDomain, getLoadingMessagesList);

var selectChatShowAnimations = exports.selectChatShowAnimations = (0, _reselect.createSelector)(_selectors.getChatDomain, getChatShowAnimations);

var selectChatThreadId = exports.selectChatThreadId = (0, _reselect.createSelector)(_selectors.getChatDomain, getChatThreadId);