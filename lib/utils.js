'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.readQidFromFeId = exports.createMessageId = exports.getForkedQid = exports.createMessage = exports.parseSchema = exports.parseSchemaQuestion = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _flatten2 = require('lodash/fp/flatten');

var _flatten3 = _interopRequireDefault(_flatten2);

var _flow2 = require('lodash/fp/flow');

var _flow3 = _interopRequireDefault(_flow2);

var _get2 = require('lodash/fp/get');

var _get3 = _interopRequireDefault(_get2);

var _isString2 = require('lodash/fp/isString');

var _isString3 = _interopRequireDefault(_isString2);

var _map2 = require('lodash/fp/map');

var _map3 = _interopRequireDefault(_map2);

var _pick2 = require('lodash/fp/pick');

var _pick3 = _interopRequireDefault(_pick2);

var _camelCase2 = require('lodash/camelCase');

var _camelCase3 = _interopRequireDefault(_camelCase2);

var _isNumber2 = require('lodash/isNumber');

var _isNumber3 = _interopRequireDefault(_isNumber2);

var _constants = require('./Message/constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var equationParamRegexp = /([a-z_]+)/gi;
var equationOperatorAndRegexp = /\band\b/gi;
var equationOperatorOrRegexp = /\bor\b/gi;

var parseSchemaQuestion = exports.parseSchemaQuestion = function parseSchemaQuestion(question) {
  return (0, _pick3.default)(['id', 'qid', 'nextQid', 'type', 'showAsType', 'choiceOpen', 'text', 'attachments', 'answers', // TODO: MM: type needed to know what to expect
  'meta', 'forkingRules', 'variableModifications'])(question);
};

var parseSchema = exports.parseSchema = (0, _flow3.default)((0, _get3.default)('groups'), (0, _map3.default)((0, _get3.default)('questions')), _flatten3.default, (0, _map3.default)(parseSchemaQuestion));

var createMessage = exports.createMessage = function createMessage(data) {
  return {
    feId: createMessageId(data), // TODO: MM: it uses the isOwn from the message to calculate the feId - to consider since we changed isOwn in groups to showOnRight
    threadId: data.threadId,
    created: new Date().toISOString(),
    editable: false,
    status: 'loading',
    type: data.showAsType || _constants.CHAT_MESSAGE_TYPE_TEXT,
    text: data.text,
    meta: data.meta,
    questionId: data.question || null,
    attachments: []
  };
};

var getForkedQid = exports.getForkedQid = function getForkedQid(forkingRules, variablesPool) {
  var matchingRule = forkingRules.find(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        qid = _ref2[0],
        equation = _ref2[1];

    return calculateEquation(equation, variablesPool);
  });

  if (matchingRule) {
    return parseInt(matchingRule[0], 10);
  }

  return null;
};

var calculateEquation = function calculateEquation(equation) {
  var variablesPool = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var equationFunction = createEquationFunction(equation);
  var result = void 0;

  try {
    result = equationFunction(variablesPool);
  } catch (e) {
    throw new Error('Fork function error: ' + equationFunction.toString());
  }

  return result;
};

var createEquationFunction = function createEquationFunction(equation) {
  var equationWithOperators = replaceStringOperators(equation);
  // call changeParamsToObject after replaceStringOperators,
  // this way we won't end up with equationParams.and and equationParams.or
  var equationWithPrefixedParams = changeParamsToObject(equationWithOperators);
  var functionBody = 'return (' + equationWithPrefixedParams + ')';
  var equationFunction = new Function('equationParams', functionBody);

  return equationFunction;
};

// replace string operators with && || to
// make them work inside Function
// from:  forkVariable == 1 and forkOtherVariable == 2
// to:    forkVariable == 1 && forkOtherVariable == 2
// from:  forkVariable == 1 or forkOtherVariable == 2
// to:    forkVariable == 1 || forkOtherVariable == 2
var replaceStringOperators = function replaceStringOperators(equation) {
  return equation.replace(equationOperatorAndRegexp, '&&').replace(equationOperatorOrRegexp, '||');
};

// make all params object properties to
// avoid having undefined variable error
// from:  forkVariable == 1
// to:    equationParams.forkVariable == 1
var changeParamsToObject = function changeParamsToObject(equation) {
  return equation.replace(equationParamRegexp, 'equationParams.$1');
};

var createMessageId = exports.createMessageId = function createMessageId(data) {
  var id = data.id,
      isOwn = data.isOwn,
      qid = data.qid;


  if (qid) {
    // SQR - schema question reply
    // SQM - schema question message
    return isOwn ? 'SQR:' + qid : 'SQM:' + qid;
  }

  // UOM - user outgoing message
  // UIM - user incoming message
  // TODO: set some unique id
  return isOwn ? 'UOM:someUniqueID' : 'UIM:' + id;
};

var readQidFromFeId = exports.readQidFromFeId = function readQidFromFeId(feId) {
  if (!feId || !(0, _isString3.default)(feId)) {
    return null;
  }

  var qid = feId.match(/SQ[MR]:([0-9]+)/);

  if (qid) {
    return Number(qid[1]);
  }

  return null;
};