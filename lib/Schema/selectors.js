'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculateVariablesPool = exports.selectQuestionsRepliesProgress = exports.selectMinNumberOfStepsToEnd = exports.selectChatFinishQid = exports.selectChatQuestionsGroupByQid = exports.selectChatCurrentQuestionId = exports.selectChatNrOfStepsReplies = exports.selectChatQuestions = exports.selectChatSchemaQueueStatusIsFailed = exports.selectChatSchemaQueueStatusIsEnded = exports.selectSchemaQueueRequestsProgress = exports.selectChatSchemaQueueStatus = exports.selectLoadingSchema = exports.selectSchemaCode = exports.selectSchemaType = exports.selectSchemaId = exports.selectChatLastQuestionReply = exports.getChatLastQuestionReply = exports.selectChatQuestionsReplies = exports.selectChatNextQuestionQid = exports.selectChatCurrentReply = exports.selectChatCurrentSelectedAnswers = exports.selectChatCurrentAnswers = exports.selectChatAnsweredQuestions = exports.selectChatPreviousQuestion = exports.selectChatCurrentQuestion = exports.selectSchemaQueueStatusIsFailed = exports.selectSchemaQueueStatusIsEnded = exports.makeSelectAnsweredQuestionsUpToQid = exports.makeSelectQuestionsRepliesUpToQid = exports.selectNextQuestion = exports.makeSelectAnswersTextByIdForQuestion = exports.makeSelectAnswersByIdForQuestion = exports.makeSelectAnswersForQuestion = exports.makeSelectQuestionByQid = exports.makeSelectQuestionById = exports.selectNextQuestionQid = exports.selectCurrentNextQid = exports.selectCurrentQuestionReply = exports.selectCurrentIsQuestionMultiple = exports.selectCurrentIsQuestionChoice = exports.selectCurrentSelectedAnswers = exports.selectCurrentAnswers = exports.selectQuestionReply = exports.selectSelectedAnswersIds = exports.selectFirstAnswerAttachments = exports.selectFirstAnswerMeta = exports.selectChoiceOpenAnswerText = exports.selectAnswersText = exports.selectChoiceOpenTextAnswers = exports.selectSelectedAnswers = exports.selectIsQuestionControl = exports.selectIsQuestionText = exports.selectIsQuestionMultiple = exports.selectIsQuestionChoiceOpen = exports.selectIsQuestionChoice = exports.makeListTrimmer = exports.filterTextAnswers = exports.filterSelectedAnswers = exports.getQuestionMetaMessageType = exports.selectAnswers = exports.getAnswerText = exports.getQuestionNextQid = exports.getQuestionQid = exports.getQuestionId = exports.getQuestionChoiceOpen = exports.getQuestionType = exports.getQuestionReplies = exports.getAnsweredQuestions = exports.getPreviousQuestion = exports.getCurrentQuestion = exports.getQuestions = exports.getSchemaQueueStatus = exports.getLoadingSchema = exports.getSchemaCode = exports.getSchemaType = exports.getSchemaId = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _immutable = require('immutable');

var _reselect = require('reselect');

var _mapKeys2 = require('lodash/mapKeys');

var _mapKeys3 = _interopRequireDefault(_mapKeys2);

var _forEach2 = require('lodash/forEach');

var _forEach3 = _interopRequireDefault(_forEach2);

var _reduce2 = require('lodash/reduce');

var _reduce3 = _interopRequireDefault(_reduce2);

var _camelCase2 = require('lodash/camelCase');

var _camelCase3 = _interopRequireDefault(_camelCase2);

var _nodeDijkstra = require('node-dijkstra');

var _nodeDijkstra2 = _interopRequireDefault(_nodeDijkstra);

var _selectors = require('../Chat/selectors');

var _selectors2 = require('../VariablesPool/selectors');

var _constants = require('./constants');

var _constants2 = require('../Message/constants');

var _utils = require('../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// TODO this ones should be named getX
var getSchemaId = exports.getSchemaId = function getSchemaId(state) {
  return state.get('schemaId');
};
var getSchemaType = exports.getSchemaType = function getSchemaType(state) {
  return state.get('schemaType');
};
var getSchemaCode = exports.getSchemaCode = function getSchemaCode(state) {
  return state.get('schemaCode');
};
var getLoadingSchema = exports.getLoadingSchema = function getLoadingSchema(state) {
  return state.get('loadingSchema');
};
var getSchemaQueueStatus = exports.getSchemaQueueStatus = function getSchemaQueueStatus(state) {
  return state.get('schemaQueueStatus');
};
var getQuestions = exports.getQuestions = function getQuestions(state) {
  return state.get('questions');
};
var getCurrentQuestion = exports.getCurrentQuestion = function getCurrentQuestion(state) {
  return state.get('currentQuestion');
};
var getPreviousQuestion = exports.getPreviousQuestion = function getPreviousQuestion(state) {
  return state.get('previousQuestion');
};
var getAnsweredQuestions = exports.getAnsweredQuestions = function getAnsweredQuestions(state) {
  return state.get('answeredQuestions');
};
var getQuestionReplies = exports.getQuestionReplies = function getQuestionReplies(state) {
  return state.get('questionsReplies');
};
var getQuestionType = exports.getQuestionType = function getQuestionType(state) {
  return state.get('type');
};
var getQuestionChoiceOpen = exports.getQuestionChoiceOpen = function getQuestionChoiceOpen(state) {
  return state.get('choiceOpen');
};
var getQuestionId = exports.getQuestionId = function getQuestionId(state) {
  return state.get('id');
};
var getQuestionQid = exports.getQuestionQid = function getQuestionQid(state) {
  return state.get('qid');
};
var getQuestionNextQid = exports.getQuestionNextQid = function getQuestionNextQid(state) {
  return state.get('nextQid');
};
var getAnswerText = exports.getAnswerText = function getAnswerText(state) {
  return state.get('text');
};
var selectAnswers = exports.selectAnswers = function selectAnswers(state) {
  return state.get('answers');
};
var getQuestionMetaMessageType = exports.getQuestionMetaMessageType = function getQuestionMetaMessageType(state) {
  var meta = state.get('meta');
  if (!meta || !meta.get('messageType')) {
    return _constants2.CHAT_MESSAGE_TYPE_TEXT;
  }

  return meta.get('messageType');
};

var filterSelectedAnswers = exports.filterSelectedAnswers = function filterSelectedAnswers(answers) {
  return (0, _selectors.filterBy)(answers, 'isSelected', true);
};

// not sure if the name is proper, can be also filterAnswersWithoutId
var filterTextAnswers = exports.filterTextAnswers = function filterTextAnswers(answers) {
  return (0, _selectors.filterBy)(answers, 'type', 'text');
};

var makeListTrimmer = exports.makeListTrimmer = function makeListTrimmer(listItemIdName, targetIdName) {
  return function (itemsList, targetItem) {
    var targetId = targetItem ? targetItem.get(targetIdName) : null;
    var size = itemsList.size;
    var trimmedList = new _immutable.List();

    for (var i = 0; i < size; i += 1) {
      var currentReply = itemsList.get(i);

      trimmedList = trimmedList.push(currentReply);

      if (currentReply.get(listItemIdName) === targetId) {
        break;
      }
    }

    return trimmedList;
  };
};

// / TODO

/* any question level selectors */

var selectIsQuestionChoice = exports.selectIsQuestionChoice = (0, _reselect.createSelector)(getQuestionType, function (type) {
  return type === 'choice' || type === 'multichoice' || type === 'yesno';
});

var selectIsQuestionChoiceOpen = exports.selectIsQuestionChoiceOpen = (0, _reselect.createSelector)([getQuestionType, getQuestionChoiceOpen], function (type, choiceOpen) {
  return type === 'choice' && choiceOpen;
});

var selectIsQuestionMultiple = exports.selectIsQuestionMultiple = (0, _reselect.createSelector)(getQuestionType, function (type) {
  return type === 'multichoice';
});

var selectIsQuestionText = exports.selectIsQuestionText = (0, _reselect.createSelector)(getQuestionType, function (type) {
  return type === 'text' || type === 'message' || type === 'date';
});

var selectIsQuestionControl = exports.selectIsQuestionControl = (0, _reselect.createSelector)(getQuestionType, function (type) {
  return type === 'control';
});

var selectSelectedAnswers = exports.selectSelectedAnswers = (0, _reselect.createSelector)(selectAnswers, filterSelectedAnswers);

var selectChoiceOpenTextAnswers = exports.selectChoiceOpenTextAnswers = (0, _reselect.createSelector)(selectAnswers, filterTextAnswers);

var selectAnswersText = exports.selectAnswersText = (0, _reselect.createSelector)(selectAnswers, function (answers) {
  return answers.map(function (answer) {
    return answer.get('text');
  });
});

// for choice open type we acceppt also text answer
// teoretically, we can should allow only one this kinda question
// but for backward compatibility strings are joined from array
var selectChoiceOpenAnswerText = exports.selectChoiceOpenAnswerText = (0, _reselect.createSelector)(selectChoiceOpenTextAnswers, function (answers) {
  return answers.map(function (answer) {
    return answer.get('text');
  });
});

var selectFirstAnswerMeta = exports.selectFirstAnswerMeta = (0, _reselect.createSelector)(selectAnswers, function (answers) {
  if (answers.size) {
    var meta = answers.last().get('meta');
    if (meta) {
      return meta.toJS();
    }
  }
  return {};
});

var selectFirstAnswerAttachments = exports.selectFirstAnswerAttachments = (0, _reselect.createSelector)(selectAnswers, function (answers) {
  if (answers.size) {
    var attachments = answers.first().get('attachments');
    if (attachments) {
      return attachments.toJS();
    }
  }
  return null;
});

var selectSelectedAnswersIds = exports.selectSelectedAnswersIds = (0, _reselect.createSelector)(selectSelectedAnswers, function (answers) {
  return answers.map(function (answer) {
    return answer.get('id');
  });
});

var selectQuestionReply = exports.selectQuestionReply = (0, _reselect.createSelector)([getQuestionId, getQuestionMetaMessageType, selectIsQuestionChoice, selectIsQuestionChoiceOpen, selectIsQuestionText, selectSelectedAnswersIds, selectAnswersText, selectFirstAnswerMeta, selectFirstAnswerAttachments, selectChoiceOpenAnswerText], function (questionId, messageType, isQuestionChoice, isQuestionChoiceOpen, isQuestionText, selectedAnswersIds, answersText, firstAnswerMeta, firstAnswerAttachments, textAnswers) {
  var reply = {
    threadId: -1,
    type: messageType,
    question: questionId
  };

  if (isQuestionChoice) {
    reply.answers = selectedAnswersIds.toJS();
    if (isQuestionChoiceOpen) {
      reply.text = textAnswers.toJS().join(', ');
    }
  } else if (isQuestionText) {
    reply.text = answersText.toJS().join(', ');
  }
  reply.meta = firstAnswerMeta;

  if (firstAnswerAttachments) {
    reply.attachments = firstAnswerAttachments;
  }

  return reply;
});

/* chat level selectors (for use in chat reducers) */

/* currentQuestion related selectors */

var selectCurrentAnswers = exports.selectCurrentAnswers = (0, _reselect.createSelector)(getCurrentQuestion, selectAnswers);

var selectCurrentSelectedAnswers = exports.selectCurrentSelectedAnswers = (0, _reselect.createSelector)(selectCurrentAnswers, filterSelectedAnswers);

var selectCurrentIsQuestionChoice = exports.selectCurrentIsQuestionChoice = (0, _reselect.createSelector)(getCurrentQuestion, selectIsQuestionChoice);

var selectCurrentIsQuestionMultiple = exports.selectCurrentIsQuestionMultiple = (0, _reselect.createSelector)(getCurrentQuestion, selectIsQuestionMultiple);

var selectCurrentQuestionReply = exports.selectCurrentQuestionReply = (0, _reselect.createSelector)(getCurrentQuestion, selectQuestionReply);

var selectCurrentNextQid = exports.selectCurrentNextQid = (0, _reselect.createSelector)(getCurrentQuestion, getQuestionNextQid);

var selectNextQuestionQid = exports.selectNextQuestionQid = (0, _reselect.createSelector)([selectCurrentIsQuestionChoice, selectCurrentSelectedAnswers, selectCurrentNextQid], function (isQuestionChoice, selectedAnswers, questionNextQid) {
  var nextQuestionQid = questionNextQid;

  if (isQuestionChoice && selectedAnswers.size) {
    nextQuestionQid = getQuestionNextQid(selectedAnswers.get(0));
  }

  return nextQuestionQid;
});

/* questions related selectors */

var makeSelectQuestionById = exports.makeSelectQuestionById = function makeSelectQuestionById(questionId) {
  return (0, _reselect.createSelector)(getQuestions, function (questions) {
    return (0, _selectors.findBy)(questions, 'id', questionId) || (0, _immutable.Map)();
  });
};

var makeSelectQuestionByQid = exports.makeSelectQuestionByQid = function makeSelectQuestionByQid(qid) {
  return (0, _reselect.createSelector)(getQuestions, function (questions) {
    return (0, _selectors.findBy)(questions, 'qid', qid);
  });
};

var makeSelectAnswersForQuestion = exports.makeSelectAnswersForQuestion = function makeSelectAnswersForQuestion(questionId) {
  return (0, _reselect.createSelector)(makeSelectQuestionById(questionId), function (question) {
    return question.get('answers');
  });
};

var makeSelectAnswersByIdForQuestion = exports.makeSelectAnswersByIdForQuestion = function makeSelectAnswersByIdForQuestion(answersIds, questionId) {
  return (0, _reselect.createSelector)(makeSelectAnswersForQuestion(questionId), function (answers) {
    return answers && (0, _selectors.filterByMultiple)(answers, 'id', answersIds);
  });
};

var makeSelectAnswersTextByIdForQuestion = exports.makeSelectAnswersTextByIdForQuestion = function makeSelectAnswersTextByIdForQuestion(answersIds, questionId) {
  return (0, _reselect.createSelector)(makeSelectAnswersByIdForQuestion(answersIds, questionId), function (answers) {
    return answers && (0, _selectors.listPick)(answers, 'text');
  });
};

var selectNextQuestion = exports.selectNextQuestion = (0, _reselect.createSelector)([getQuestions, selectNextQuestionQid, _selectors2.getVariablesPool], function (questions, nextQuestionQid, variablesPool) {
  var nextQuestion = (0, _selectors.filterBy)(questions, 'qid', nextQuestionQid);

  while (nextQuestion && nextQuestion.first() && nextQuestion.first().get('type') === _constants.CHAT_QUESTION_TYPE_FORK) {
    if (!variablesPool) {
      throw Error('Custom variables are required!');
    }

    var forkingRules = nextQuestion.first().get('forkingRules');

    if (!forkingRules) {
      throw Error('Forking rules are required!');
    }

    var forkedQuestionQid = (0, _utils.getForkedQid)(forkingRules.toJS(), variablesPool.toJS());

    if (!forkedQuestionQid) {
      var questionText = nextQuestion.get('text');

      throw Error('Unable to get forked qid. Check forking rules configuration. Next fork question is ' + questionText);
    }

    nextQuestion = (0, _selectors.filterBy)(questions, 'qid', forkedQuestionQid);
  }

  return nextQuestion;
});

/* questions replies related selectors */

var makeSelectQuestionsRepliesUpToQid = exports.makeSelectQuestionsRepliesUpToQid = function makeSelectQuestionsRepliesUpToQid(qid) {
  return (0, _reselect.createSelector)([getQuestionReplies, makeSelectQuestionByQid(qid)], makeListTrimmer('question', 'id'));
};

/* answeredQuestions related selectors */

var makeSelectAnsweredQuestionsUpToQid = exports.makeSelectAnsweredQuestionsUpToQid = function makeSelectAnsweredQuestionsUpToQid(qid) {
  return (0, _reselect.createSelector)([getAnsweredQuestions, makeSelectQuestionByQid(qid)], makeListTrimmer('id', 'id'));
};

/* schema related selectors */

var selectSchemaQueueStatusIsEnded = exports.selectSchemaQueueStatusIsEnded = (0, _reselect.createSelector)(getSchemaQueueStatus, function (queueStatus) {
  return queueStatus.get('isEnded');
});

var selectSchemaQueueStatusIsFailed = exports.selectSchemaQueueStatusIsFailed = (0, _reselect.createSelector)(getSchemaQueueStatus, function (queueStatus) {
  return queueStatus.get('isFailed');
});

/* top level selectors (for use in mapStateToProps or global reducers) */

var selectChatCurrentQuestion = exports.selectChatCurrentQuestion = (0, _reselect.createSelector)(_selectors.getChatDomain, getCurrentQuestion);

var selectChatPreviousQuestion = exports.selectChatPreviousQuestion = (0, _reselect.createSelector)(_selectors.getChatDomain, getPreviousQuestion);

var selectChatAnsweredQuestions = exports.selectChatAnsweredQuestions = (0, _reselect.createSelector)(_selectors.getChatDomain, getAnsweredQuestions);

var selectChatCurrentAnswers = exports.selectChatCurrentAnswers = (0, _reselect.createSelector)(_selectors.getChatDomain, selectCurrentAnswers);

var selectChatCurrentSelectedAnswers = exports.selectChatCurrentSelectedAnswers = (0, _reselect.createSelector)(_selectors.getChatDomain, selectCurrentSelectedAnswers);

var selectChatCurrentReply = exports.selectChatCurrentReply = (0, _reselect.createSelector)(_selectors.getChatDomain, selectCurrentQuestionReply);

var selectChatNextQuestionQid = exports.selectChatNextQuestionQid = (0, _reselect.createSelector)(_selectors.getChatDomain, selectNextQuestionQid);

var selectChatQuestionsReplies = exports.selectChatQuestionsReplies = (0, _reselect.createSelector)(_selectors.getChatDomain, getQuestionReplies);

var getChatLastQuestionReply = exports.getChatLastQuestionReply = (0, _reselect.createSelector)(getQuestionReplies, function (replies) {
  return replies.last();
});

var selectChatLastQuestionReply = exports.selectChatLastQuestionReply = (0, _reselect.createSelector)(_selectors.getChatDomain, getChatLastQuestionReply);

var selectSchemaId = exports.selectSchemaId = (0, _reselect.createSelector)(_selectors.getChatDomain, getSchemaId);

var selectSchemaType = exports.selectSchemaType = (0, _reselect.createSelector)(_selectors.getChatDomain, getSchemaType);

var selectSchemaCode = exports.selectSchemaCode = (0, _reselect.createSelector)(_selectors.getChatDomain, getSchemaCode);

var selectLoadingSchema = exports.selectLoadingSchema = (0, _reselect.createSelector)(_selectors.getChatDomain, getLoadingSchema);

var selectChatSchemaQueueStatus = exports.selectChatSchemaQueueStatus = (0, _reselect.createSelector)(_selectors.getChatDomain, getSchemaQueueStatus);

var selectSchemaQueueRequestsProgress = exports.selectSchemaQueueRequestsProgress = (0, _reselect.createSelector)(selectChatSchemaQueueStatus, function (queueStatus) {
  return queueStatus.get('requestsProgress');
});

var selectChatSchemaQueueStatusIsEnded = exports.selectChatSchemaQueueStatusIsEnded = (0, _reselect.createSelector)(_selectors.getChatDomain, selectSchemaQueueStatusIsEnded);

var selectChatSchemaQueueStatusIsFailed = exports.selectChatSchemaQueueStatusIsFailed = (0, _reselect.createSelector)(_selectors.getChatDomain, selectSchemaQueueStatusIsFailed);

var selectChatQuestions = exports.selectChatQuestions = (0, _reselect.createSelector)(_selectors.getChatDomain, getQuestions);

var selectChatNrOfStepsReplies = exports.selectChatNrOfStepsReplies = (0, _reselect.createSelector)(selectChatQuestions, selectChatQuestionsReplies, selectChatCurrentQuestion, function (questions, questionsReplies, currentQuestion) {
  var repliedSteps = questionsReplies.filter(function (reply) {
    return reply.get('question') !== currentQuestion.get('id');
  });
  var repliedQuestions = repliedSteps.filter(function (reply) {
    var question = (0, _selectors.findBy)(questions, 'id', reply.get('question'));

    return !question.getIn(['meta', 'doNotCountAsQuestion']);
  });

  return {
    nrOfSteps: repliedSteps.size,
    nrOfQuestions: repliedQuestions.size
  };
});

var selectChatCurrentQuestionId = exports.selectChatCurrentQuestionId = (0, _reselect.createSelector)(selectChatCurrentQuestion, getQuestionQid);

var selectChatQuestionsGroupByQid = exports.selectChatQuestionsGroupByQid = (0, _reselect.createSelector)(selectChatQuestions, function (questions) {
  return (0, _mapKeys3.default)(questions.toJS(), function (question) {
    return question.qid;
  });
});

var selectChatFinishQid = exports.selectChatFinishQid = (0, _reselect.createSelector)(selectChatQuestions, function (questions) {
  var finishQid = null;

  questions.forEach(function (question) {
    var _question$toJS = question.toJS(),
        qid = _question$toJS.qid,
        nextQid = _question$toJS.nextQid,
        meta = _question$toJS.meta;

    if (nextQid === -1 && meta && meta.finalStep) {
      finishQid = qid;
    }
  });

  return finishQid;
});

var selectMinNumberOfStepsToEnd = exports.selectMinNumberOfStepsToEnd = (0, _reselect.createSelector)(selectChatQuestionsGroupByQid, _selectors2.selectVariablesPool, selectChatCurrentQuestionId, selectChatFinishQid, function (questions, variablesPool, questionId, finishQid) {
  if (!questionId || !finishQid) {
    return {};
  }

  var shortestPath = getShortestPath(questions, variablesPool, questionId, finishQid);
  var shortestPathWithoutNoQuestions = excludeNoQuestionStepsFromPath(questions, shortestPath);

  return {
    nrOfSteps: shortestPath && shortestPath.length,
    nrOfQuestions: shortestPathWithoutNoQuestions.length
  };
});

function excludeNoQuestionStepsFromPath(questions, shortestPath) {
  if (!shortestPath) {
    return [];
  }

  return shortestPath.filter(function (qid) {
    var question = questions[qid];

    return !question.meta || !question.meta.doNotCountAsQuestion;
  });
}

var selectQuestionsRepliesProgress = exports.selectQuestionsRepliesProgress = (0, _reselect.createSelector)(selectChatNrOfStepsReplies, selectMinNumberOfStepsToEnd, function (nrOfStepsReplies, minNumberOfStepsToEnd) {
  var totalSteps = nrOfStepsReplies.nrOfSteps + minNumberOfStepsToEnd.nrOfSteps;
  var totalQuestions = nrOfStepsReplies.nrOfQuestions + minNumberOfStepsToEnd.nrOfQuestions;
  var totalProgress = Math.round(nrOfStepsReplies.nrOfSteps * 100 / totalSteps);

  return {
    minNumberOfStepsToEnd: minNumberOfStepsToEnd.nrOfSteps,
    minNumberOfQuestionsToEnd: minNumberOfStepsToEnd.nrOfQuestions,
    nrOfStepsReplies: nrOfStepsReplies.nrOfSteps,
    nrOfQuestionsReplies: nrOfStepsReplies.nrOfQuestions,
    totalQuestions: totalQuestions,
    totalSteps: totalSteps,
    totalProgress: totalProgress
  };
});

// TODO: calculate more sophisticated equations on assignment
//       supported: a = 0
//       possibly:  a = a + 1
//       possibly:  b = a + 1
var calculateVariablesPool = exports.calculateVariablesPool = function calculateVariablesPool(questionId, answers) {
  return (0, _reselect.createSelector)([makeSelectQuestionById(questionId), _selectors2.getVariablesPool], function (question, currentVariablesPool) {
    var newVariablesPool = {};
    var questionVariablesModification = question.get('variableModifications');
    var questionAnswers = question.get('answers');

    if (questionVariablesModification && questionVariablesModification.size) {
      questionVariablesModification.forEach(function (variable) {
        var variableName = (0, _camelCase3.default)(variable.get(0));
        var variableValue = parseInt(variable.get(1), 10);
        if (Number.isNaN(variableValue)) {
          variableValue = variable.get(1);
        }
        newVariablesPool[variableName] = variableValue;
      });
    }

    if (answers && questionAnswers.size) {
      answers.forEach(function (answer) {
        var questionAnswerVariablesMofidication = questionAnswers.find(function (v) {
          return v.get('id') === answer;
        }).get('variableModifications');

        if (questionAnswerVariablesMofidication) {
          questionAnswerVariablesMofidication.forEach(function (variable) {
            var variableName = (0, _camelCase3.default)(variable.get(0));
            var variableValue = parseInt(variable.get(1), 10);
            if (Number.isNaN(variableValue)) {
              variableValue = variable.get(1);
            }
            newVariablesPool[variableName] = variableValue;
          });
        }
      });
    }

    return _extends({}, currentVariablesPool.toObject(), newVariablesPool);
  });
};

function getShortestPath(questions, variablesPool, fromQid, toQid) {
  if (questions && variablesPool) {
    var route = new _nodeDijkstra2.default();
    var questionsRelationGraph = getQuestionsRelationGraph(questions, variablesPool);

    (0, _forEach3.default)(questionsRelationGraph, function (nodeToQids, nodeFromQid) {
      route.addVertex(nodeFromQid, nodeToQids);
    });

    route.addVertex(toQid, {});

    return route.shortestPath(fromQid.toString(), toQid.toString());
  }

  return [];
}

function getQuestionsRelationGraph(questions, variablesPool) {
  var questionsRelationGraph = (0, _immutable.Map)();
  var forkQuestions = [];

  (0, _forEach3.default)(questions, function (question) {
    var answers = question.answers,
        forkingRules = question.forkingRules,
        nextQid = question.nextQid,
        qid = question.qid,
        type = question.type;


    if (type === 'fork') {
      forkQuestions.push(qid);
    }

    if (Array.isArray(answers) && answers.length) {
      answers.forEach(function (answer) {
        if (isValidNextQid(answer.nextQid)) {
          questionsRelationGraph = questionsRelationGraph.setIn([qid, answer.nextQid], 1);
        }
      });
    } else if (nextQid) {
      if (isValidNextQid(nextQid)) {
        questionsRelationGraph = questionsRelationGraph.setIn([qid, nextQid], 1);
      }
    } else if (Array.isArray(forkingRules)) {
      var allForkedPathArePossible = true;
      var forkingRulesLength = forkingRules.length;

      for (var i = 0; i < forkingRulesLength; i += 1) {
        var forkingRule = forkingRules[i];

        if (isValidForkedPath(forkingRule, variablesPool)) {
          questionsRelationGraph = addForkedPathToQuestionsRelationGraph(questionsRelationGraph, qid, forkingRule);
          allForkedPathArePossible = false;
          break;
        }
      }

      if (allForkedPathArePossible) {
        questionsRelationGraph = addAllForkedPaths(questionsRelationGraph, qid, forkingRules);
      }
    }
  });

  return reduceForkQuestions(questionsRelationGraph, forkQuestions);
}

function isValidNextQid(nextQid) {
  return nextQid && nextQid !== -1;
}

function isValidForkedPath(forkingRule, variablesPool) {
  var rule = forkingRule[1].split('==');

  if (rule.length === 2) {
    var variableName = (0, _camelCase3.default)(rule[0].trim());
    var variableValue = rule[1].trim();
    var variableValueInVariablesPool = '' + variablesPool.get(variableName);

    if (variableValueInVariablesPool === variableValue) {
      return true;
    }
  }
  return false;
}

function addAllForkedPaths(questionsRelationGraph, qid, forkingRules) {
  var questionsRelationGraphWithForkedPaths = questionsRelationGraph;

  forkingRules.forEach(function (forkingRule) {
    questionsRelationGraphWithForkedPaths = addForkedPathToQuestionsRelationGraph(questionsRelationGraphWithForkedPaths, qid, forkingRule);
  });

  return questionsRelationGraphWithForkedPaths;
}

function addForkedPathToQuestionsRelationGraph(questionsRelationGraph, qid, forkingRule) {
  var questionsRelationGraphWithForkedPaths = questionsRelationGraph;
  var nextForkQid = Number(forkingRule[0]);

  if (isValidNextQid(nextForkQid)) {
    questionsRelationGraphWithForkedPaths = questionsRelationGraphWithForkedPaths.setIn([qid, nextForkQid], 1);
  }

  return questionsRelationGraphWithForkedPaths;
}

function reduceForkQuestions(questionsRelationGraph, forkQuestions) {
  var reducedQuestionsRelationGraph = questionsRelationGraph.toJS();

  forkQuestions.forEach(function (forkQid) {
    var questionsRelatedWithFork = getQuestionsRelatedWithFork(reducedQuestionsRelationGraph, forkQid);

    (0, _forEach3.default)(questionsRelatedWithFork, function (questionRelation, questionRelationKey) {
      delete reducedQuestionsRelationGraph[questionRelationKey][forkQid];

      reducedQuestionsRelationGraph[questionRelationKey] = _extends({}, reducedQuestionsRelationGraph[questionRelationKey], reducedQuestionsRelationGraph[forkQid]);
    });
  });

  forkQuestions.forEach(function (forkQid) {
    delete reducedQuestionsRelationGraph[forkQid];
  });

  return reducedQuestionsRelationGraph;
}

function getQuestionsRelatedWithFork(questionsRelationGraph, forkQid) {
  return (0, _reduce3.default)(questionsRelationGraph, function (reducedQuestionsRelationGraph, question, key) {
    if (question[forkQid]) {
      return _extends({}, reducedQuestionsRelationGraph, _defineProperty({}, key, question));
    }

    return reducedQuestionsRelationGraph;
  }, {});
}