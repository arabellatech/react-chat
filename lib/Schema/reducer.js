'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.onChatSchemaQueueSetRequestsProgressAction = exports.onThreadRollbackQuestionAction = exports.composeMessageFromQuestion = exports.onNextQuestionAction = exports.getAnswersWithUpdatedSelected = exports.mergeCurrentQuestionAnswers = exports.onChangeQuestion = exports.composeMessageFromQuestionReply = exports.getNextQuestion = exports.addQuestionReply = exports.onSendQuestionReply = exports.onAnswerSet = exports.onAnswerUnselect = exports.onAnswerSelect = exports.onChatSchemaQueueFailed = exports.onChatSchemaQueueEnded = exports.onChatSchemaQueueStarted = exports.onSchemaLoadFailed = exports.onSchemaLoadSuccess = exports.onSchemaLoad = exports.onChatQuestionAnimationEnd = exports.onChatQuestionAnimationStart = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _immutable = require('immutable');

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _find2 = require('lodash/find');

var _find3 = _interopRequireDefault(_find2);

var _selectors = require('./selectors');

var _selectors2 = require('../Message/selectors');

var _reducer = require('../Message/reducer');

var _utils = require('../utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* animations */

var onChatQuestionAnimationStart = exports.onChatQuestionAnimationStart = function onChatQuestionAnimationStart(state, animation) {
  return state.mergeDeep({
    animations: {
      question: animation + '-started'
    }
  });
};

var onChatQuestionAnimationEnd = exports.onChatQuestionAnimationEnd = function onChatQuestionAnimationEnd(state, animation) {
  return state.mergeDeep({
    animations: {
      question: animation + '-ended'
    }
  });
};

/* schema */

var onSchemaLoad = exports.onSchemaLoad = function onSchemaLoad(state) {
  return state.merge({
    loadingSchema: true
  });
};

var onSchemaLoadSuccess = exports.onSchemaLoadSuccess = function onSchemaLoadSuccess(state, action) {
  var questions = (0, _utils.parseSchema)(action.payload);
  var _action$payload = action.payload,
      schemaCode = _action$payload.code,
      schemaId = _action$payload.id,
      schemaType = _action$payload.type;

  var currentQuestion = _extends({}, questions[0]);

  return state.merge({
    currentQuestion: currentQuestion,
    loadingSchema: false,
    questions: questions,
    schemaCode: schemaCode,
    schemaId: schemaId,
    schemaType: schemaType
  });
};

var onSchemaLoadFailed = exports.onSchemaLoadFailed = function onSchemaLoadFailed(state) {
  return state.merge({
    loadingSchema: false
  });
};

var onChatSchemaQueueStarted = exports.onChatSchemaQueueStarted = function onChatSchemaQueueStarted(state) {
  return state.mergeIn(['schemaQueueStatus'], {
    failureReason: null,
    isEnded: false,
    isFailed: false,
    isStarted: true
  });
};

var onChatSchemaQueueEnded = exports.onChatSchemaQueueEnded = function onChatSchemaQueueEnded(state) {
  return state.merge({
    schemaQueueStatus: {
      failureReason: null,
      isEnded: true,
      isFailed: false,
      isStarted: false,
      requestsProgress: 0
    }
  });
};

var onChatSchemaQueueFailed = exports.onChatSchemaQueueFailed = function onChatSchemaQueueFailed(state, action) {
  return state.merge({
    schemaQueueStatus: {
      failureReason: action.reason,
      isEnded: false,
      isFailed: true,
      isStarted: false,
      requestsProgress: 0
    }
  });
};

/* answers */

var onAnswerSelect = exports.onAnswerSelect = function onAnswerSelect(state, action) {
  return mergeCurrentQuestionAnswers(state, getAnswersWithUpdatedSelected(state, (0, _get3.default)(action, 'answer.id'), true));
};

var onAnswerUnselect = exports.onAnswerUnselect = function onAnswerUnselect(state, action) {
  return mergeCurrentQuestionAnswers(state, getAnswersWithUpdatedSelected(state, (0, _get3.default)(action, 'answer.id'), false));
};

// export const onAnswerSet = (state: ChatStateType, action: ChatSetAnswerActionType): ChatStateType => mergeCurrentQuestionAnswers(
//   state,
//   [action.answer]
// );

var onAnswerSet = exports.onAnswerSet = function onAnswerSet(state, action) {
  var currentQuestion = state.get('currentQuestion').toJS();
  var answer = (0, _find3.default)(currentQuestion.answers, { type: 'text' });
  if (answer) {
    answer.text = action.answer.text;
    answer.meta = action.answer.meta;
  } else {
    currentQuestion.answers.push(action.answer);
  }

  return state.merge({
    currentQuestion: currentQuestion
  });
};

/* questions */

var onSendQuestionReply = exports.onSendQuestionReply = function onSendQuestionReply(state, action) {
  var questionsReplies = addQuestionReply(state, action);
  var newMessage = composeMessageFromQuestionReply(state, action.reply);

  var variablesPool = (0, _selectors.calculateVariablesPool)(action.reply.question, action.reply.answers)(state);
  var variablesPoolHistory = state.get('variablesPoolHistory').push(variablesPool);
  var currentQuestion = (0, _selectors.getCurrentQuestion)(state);
  var answeredQuestions = (0, _selectors.getAnsweredQuestions)(state).push(currentQuestion);
  var results = {
    questionsReplies: questionsReplies,
    answeredQuestions: answeredQuestions,
    variablesPool: variablesPool,
    variablesPoolHistory: variablesPoolHistory,
    showAnimations: true
  };

  if (newMessage.type !== 'hidden' && (0, _get3.default)(newMessage, 'meta.messageType') !== 'hidden') {
    results = _extends({}, results, {
      showAnimations: true,
      messages: (0, _reducer.addOutgoingMessage)(state, newMessage)
    });
  }

  return state.merge(results);
};

var addQuestionReply = exports.addQuestionReply = function addQuestionReply(state, action) {
  var questionsReplies = (0, _selectors.getQuestionReplies)(state);

  var modifiedAction = (0, _immutable.fromJS)(action).get('reply').setIn(['meta', 'redux'], undefined);

  questionsReplies = questionsReplies.push(modifiedAction);

  return questionsReplies;
};

var getNextQuestion = exports.getNextQuestion = function getNextQuestion(state) {
  var nextQuestion = (0, _selectors.selectNextQuestion)(state);

  if (nextQuestion && nextQuestion.get) {
    nextQuestion = nextQuestion.get(0);
  }

  return nextQuestion;
};

var composeMessageFromQuestionReply = exports.composeMessageFromQuestionReply = function composeMessageFromQuestionReply(state, reply) {
  var question = (0, _selectors.makeSelectQuestionById)(reply.question)(state);
  var modifiedReply = (0, _immutable.fromJS)(reply).setIn(['meta', 'redux'], undefined).toJS();
  var questionReply = _extends({}, modifiedReply, {
    isOwn: true,
    qid: question.get('qid')
  });

  var answersText = [questionReply.text];

  // Answers in reply are only ID's but text is required to show it in chat immediately
  if (questionReply.answers && questionReply.answers.length) {
    answersText = (0, _selectors.makeSelectAnswersTextByIdForQuestion)(questionReply.answers, questionReply.question)(state);
  }

  questionReply.text = answersText ? answersText.join(', ') : '';

  // convert reply to expected message format
  var message = (0, _utils.createMessage)(questionReply);

  return message;
};

/* current question */

var onChangeQuestion = exports.onChangeQuestion = function onChangeQuestion(state, action) {
  return state.merge({
    currentQuestion: action.question
  });
};

var mergeCurrentQuestionAnswers = exports.mergeCurrentQuestionAnswers = function mergeCurrentQuestionAnswers(state, answers) {
  return state.mergeIn(['currentQuestion', 'answers'], answers);
};

var getAnswersWithUpdatedSelected = exports.getAnswersWithUpdatedSelected = function getAnswersWithUpdatedSelected(state, answerId, selected) {
  var isMultiple = (0, _selectors.selectCurrentIsQuestionMultiple)(state);
  var answers = (0, _selectors.selectCurrentAnswers)(state) || [];

  answers = answers.map(function (answer) {
    if (answer.get('id') === answerId) {
      return answer.set('isSelected', selected);
    } else if (selected && (!isMultiple || answer.get('exclusive'))) {
      return answer.set('isSelected', false);
    }

    return answer;
  });

  return answers;
};

/* next question */

var onNextQuestionAction = exports.onNextQuestionAction = function onNextQuestionAction(state) {
  var currentQuestionFromState = state.get('currentQuestion');
  var nextQuestion = getNextQuestion(state);

  // if current question is the last in the flow and already has been answered, clear it!
  if (currentQuestionFromState.get('nextQid') === -1) {
    var lastQuestionReply = (0, _selectors.getChatLastQuestionReply)(state);
    // TODO: MM: probably if the last question has no answers, it should also pass
    if (lastQuestionReply.question === currentQuestionFromState.get('id')) {
      return state.merge({
        currentQuestion: {
          answers: []
        }
      });
    }
  }

  // TODO: PP: figure out how is it possible for
  // currentQuestion to be undefined here
  if (!nextQuestion) {
    return state;
  }

  var results = {
    previousQuestion: currentQuestionFromState,
    currentQuestion: nextQuestion
  };

  var newMessage = composeMessageFromQuestion(nextQuestion.toJS());

  if (newMessage.type !== 'hidden' && (0, _get3.default)(newMessage, 'meta.messageType') !== 'hidden') {
    results = _extends({}, results, {
      messages: (0, _reducer.addIncomingMessage)(state, newMessage)
    });
  }

  return state.merge(results);
};

var composeMessageFromQuestion = exports.composeMessageFromQuestion = function composeMessageFromQuestion(question) {
  var message = (0, _utils.createMessage)(_extends({}, question, {
    question: question.id
  }));

  return message;
};

/* rollback question */

var onThreadRollbackQuestionAction = exports.onThreadRollbackQuestionAction = function onThreadRollbackQuestionAction(state, action) {
  var qid = (0, _utils.readQidFromFeId)(action.feId);
  // @TODO: !!! WARNING !!!
  // variables pool history works only one step back while this function genuinely
  // allow to rollback to any point in history
  // it is fine for current way we use it but this need to be fixed in the future
  // https://ydtechnology.atlassian.net/browse/ARD-474
  var variablesPoolHistory = state.get('variablesPoolHistory').pop();
  var variablesPool = variablesPoolHistory.last() || new _immutable.Map();
  var messages = new _immutable.List();
  var questionsReplies = new _immutable.List();
  var answeredQuestions = new _immutable.List();
  var currentQuestion = void 0;
  var previousQuestion = state.get('currentQuestion');

  if (action.feId) {
    messages = (0, _selectors2.makeSelectMessagesListUpToFeId)(action.feId)(state);
  }

  if (qid) {
    questionsReplies = (0, _selectors.makeSelectQuestionsRepliesUpToQid)(qid)(state).pop();
    answeredQuestions = (0, _selectors.makeSelectAnsweredQuestionsUpToQid)(qid)(state);
    currentQuestion = answeredQuestions.size ? answeredQuestions.last() : (0, _selectors.makeSelectQuestionByQid)(qid)(state);
  } else {
    currentQuestion = (0, _selectors.getQuestions)(state).get(0);
  }

  return state.merge({
    answeredQuestions: answeredQuestions.pop(),
    currentQuestion: currentQuestion,
    messages: messages,
    previousQuestion: previousQuestion,
    questionsReplies: questionsReplies,
    variablesPool: variablesPool,
    variablesPoolHistory: variablesPoolHistory
  });
};

var onChatSchemaQueueSetRequestsProgressAction = exports.onChatSchemaQueueSetRequestsProgressAction = function onChatSchemaQueueSetRequestsProgressAction(state, action) {
  return state.setIn(['schemaQueueStatus', 'requestsProgress'], action.requestsProgress);
};