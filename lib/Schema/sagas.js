'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchChatLoadSchemaSaga = watchChatLoadSchemaSaga;
exports.chatLoadSchemaSaga = chatLoadSchemaSaga;
exports.watchChatLoadOnboardingSchemaSaga = watchChatLoadOnboardingSchemaSaga;
exports.chatLoadOnboardingSchemaSaga = chatLoadOnboardingSchemaSaga;
exports.watchSendQuestionReplySaga = watchSendQuestionReplySaga;
exports.onSendQuestionReplySaga = onSendQuestionReplySaga;
exports.watchSchemaQueueSaga = watchSchemaQueueSaga;
exports.chatSendQuestionReplySaga = chatSendQuestionReplySaga;
exports.chatThreadRollbackSaga = chatThreadRollbackSaga;
exports.watchChatSchemaQueueClearFinishedRequestsSaga = watchChatSchemaQueueClearFinishedRequestsSaga;
exports.chatSchemaQueueClearFinishedRequestsSaga = chatSchemaQueueClearFinishedRequestsSaga;
exports.retryUntilLimitReached = retryUntilLimitReached;

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _reduxSaga = require('redux-saga');

var _effects = require('redux-saga/effects');

var _actions = require('./actions');

var _selectors = require('./selectors');

var _constants = require('./constants');

var _api = require('../api');

var _requestsProgess = require('./requestsProgess');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(watchChatLoadSchemaSaga),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(chatLoadSchemaSaga),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(watchChatLoadOnboardingSchemaSaga),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(chatLoadOnboardingSchemaSaga),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(watchSendQuestionReplySaga),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(onSendQuestionReplySaga),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(watchSchemaQueueSaga),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(chatSendQuestionReplySaga),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadRollbackSaga),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSchemaQueueClearFinishedRequestsSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(chatSchemaQueueClearFinishedRequestsSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(retryUntilLimitReached); // TODO: use flow

function watchChatLoadSchemaSaga() {
  return regeneratorRuntime.wrap(function watchChatLoadSchemaSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_LOAD_SCHEMA_ACTION, chatLoadSchemaSaga);

        case 2:
        case 'end':
          return _context.stop();
      }
    }
  }, _marked, this);
}

function chatLoadSchemaSaga(action /* : ChatLoadSchemaActionType */) {
  var response;
  return regeneratorRuntime.wrap(function chatLoadSchemaSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(_api.getChatSchema, action.id);

        case 3:
          response = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)((0, _actions.chatLoadSchemaSuccessAction)(response.data));

        case 6:
          _context2.next = 12;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2['catch'](0);
          _context2.next = 12;
          return (0, _effects.put)((0, _actions.chatLoadSchemaFailedAction)(_context2.t0));

        case 12:
        case 'end':
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 8]]);
}

function watchChatLoadOnboardingSchemaSaga() {
  return regeneratorRuntime.wrap(function watchChatLoadOnboardingSchemaSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_LOAD_ONBOARDING_SCHEMA_ACTION, chatLoadOnboardingSchemaSaga);

        case 2:
        case 'end':
          return _context3.stop();
      }
    }
  }, _marked3, this);
}

function chatLoadOnboardingSchemaSaga() {
  var response;
  return regeneratorRuntime.wrap(function chatLoadOnboardingSchemaSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.call)(_api.getChatOnboardingSchema);

        case 3:
          response = _context4.sent;
          _context4.next = 6;
          return (0, _effects.put)((0, _actions.chatLoadOnboardingSchemaSuccessAction)(response.data));

        case 6:
          _context4.next = 12;
          break;

        case 8:
          _context4.prev = 8;
          _context4.t0 = _context4['catch'](0);
          _context4.next = 12;
          return (0, _effects.put)((0, _actions.chatLoadOnboardingSchemaFailedAction)(_context4.t0));

        case 12:
        case 'end':
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 8]]);
}

function watchSendQuestionReplySaga() {
  return regeneratorRuntime.wrap(function watchSendQuestionReplySaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return [(0, _effects.takeEvery)(_constants.CHAT_SEND_QUESTION_REPLY_ACTION, onSendQuestionReplySaga), (0, _effects.takeEvery)(_constants.CHAT_THREAD_ROLLBACK_ACTION, onSendQuestionReplySaga)];

        case 2:
        case 'end':
          return _context5.stop();
      }
    }
  }, _marked5, this);
}

function onSendQuestionReplySaga(action) {
  return regeneratorRuntime.wrap(function onSendQuestionReplySaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.put)((0, _actions.chatSchemaPutRequestToQueue)(action));

        case 2:
          _context6.next = 4;
          return (0, _effects.call)(_requestsProgess.addPendingRequest, action.requestUid);

        case 4:
        case 'end':
          return _context6.stop();
      }
    }
  }, _marked6, this);
}

function watchSchemaQueueSaga() {
  var schemaRepliesBuffer, schemaRepliesChannel, action, requestAction, type, isEndedSchemaQueueStatus, isFailedSchemaQueueStatus;
  return regeneratorRuntime.wrap(function watchSchemaQueueSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          schemaRepliesBuffer = _reduxSaga.buffers.expanding(30);
          _context7.next = 3;
          return (0, _effects.actionChannel)(_constants.CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION, schemaRepliesBuffer);

        case 3:
          schemaRepliesChannel = _context7.sent;

        case 4:
          if (!true) {
            _context7.next = 37;
            break;
          }

          _context7.next = 7;
          return (0, _effects.take)(schemaRepliesChannel);

        case 7:
          action = _context7.sent;
          requestAction = action.requestAction, type = action.requestAction.type;
          _context7.next = 11;
          return (0, _effects.put)((0, _actions.chatSchemaQueueStartedAction)());

        case 11:
          if (!(type === _constants.CHAT_SEND_QUESTION_REPLY_ACTION)) {
            _context7.next = 16;
            break;
          }

          _context7.next = 14;
          return (0, _effects.call)(chatSendQuestionReplySaga, requestAction);

        case 14:
          _context7.next = 19;
          break;

        case 16:
          if (!(type === _constants.CHAT_THREAD_ROLLBACK_ACTION)) {
            _context7.next = 19;
            break;
          }

          _context7.next = 19;
          return (0, _effects.call)(chatThreadRollbackSaga, requestAction);

        case 19:
          _context7.next = 21;
          return (0, _effects.select)(_selectors.selectChatSchemaQueueStatusIsEnded);

        case 21:
          isEndedSchemaQueueStatus = _context7.sent;
          _context7.next = 24;
          return (0, _effects.select)(_selectors.selectChatSchemaQueueStatusIsFailed);

        case 24:
          isFailedSchemaQueueStatus = _context7.sent;

          if (!(!isEndedSchemaQueueStatus && schemaRepliesBuffer.isEmpty())) {
            _context7.next = 30;
            break;
          }

          _context7.next = 28;
          return (0, _effects.put)((0, _actions.chatSchemaQueueEndedAction)());

        case 28:
          _context7.next = 33;
          break;

        case 30:
          if (!isFailedSchemaQueueStatus) {
            _context7.next = 33;
            break;
          }

          _context7.next = 33;
          return (0, _effects.flush)(schemaRepliesChannel);

        case 33:
          _context7.next = 35;
          return (0, _effects.call)(_reduxSaga.delay, _constants.QUEUE_SUCCESS_DELAY);

        case 35:
          _context7.next = 4;
          break;

        case 37:
        case 'end':
          return _context7.stop();
      }
    }
  }, _marked7, this);
}

function chatSendQuestionReplySaga(action) {
  return regeneratorRuntime.wrap(function chatSendQuestionReplySaga$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return retryUntilLimitReached({
            requestUid: action.requestUid,
            apiCallFn: _api.sendQuestionReply,
            apiCallFnArguments: [action.reply],
            successAction: _actions.chatSendQuestionReplySuccessAction,
            failedAction: _actions.chatSendQuestionReplyFailedAction
          });

        case 2:
          return _context8.abrupt('return', _context8.sent);

        case 3:
        case 'end':
          return _context8.stop();
      }
    }
  }, _marked8, this);
}

function chatThreadRollbackSaga(action) {
  return regeneratorRuntime.wrap(function chatThreadRollbackSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return retryUntilLimitReached({
            requestUid: action.requestUid,
            apiCallFn: _api.threadRollback,
            apiCallFnArguments: [action.threadId, action.questionId],
            successAction: _actions.chatThreadRollbackSuccessAction,
            failedAction: _actions.chatThreadRollbackFailedAction
          });

        case 2:
          return _context9.abrupt('return', _context9.sent);

        case 3:
        case 'end':
          return _context9.stop();
      }
    }
  }, _marked9, this);
}

function watchChatSchemaQueueClearFinishedRequestsSaga() {
  return regeneratorRuntime.wrap(function watchChatSchemaQueueClearFinishedRequestsSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION, chatSchemaQueueClearFinishedRequestsSaga);

        case 2:
        case 'end':
          return _context10.stop();
      }
    }
  }, _marked10, this);
}

var lastTotalRequestsProgress = 0;

function chatSchemaQueueClearFinishedRequestsSaga() {
  var currentTotalRequestsProgress;
  return regeneratorRuntime.wrap(function chatSchemaQueueClearFinishedRequestsSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _effects.call)(_requestsProgess.clearFinishedRequests);

        case 2:
          _context11.next = 4;
          return (0, _effects.call)(_requestsProgess.getTotalRequestsProgress);

        case 4:
          currentTotalRequestsProgress = _context11.sent;

          lastTotalRequestsProgress = currentTotalRequestsProgress;

          _context11.next = 8;
          return (0, _effects.put)((0, _actions.chatSchemaQueueSetRequestsProgressAction)(currentTotalRequestsProgress));

        case 8:
        case 'end':
          return _context11.stop();
      }
    }
  }, _marked11, this);
}

function retryUntilLimitReached(_ref) {
  var apiCallFn = _ref.apiCallFn,
      apiCallFnArguments = _ref.apiCallFnArguments,
      failedAction = _ref.failedAction,
      requestUid = _ref.requestUid,
      successAction = _ref.successAction;

  var retries, requestChannel, _ref2, _ref2$loaded, loaded, _ref2$total, total, error, response, status, currentTotalRequestsProgress, totalRequestsDiff;

  return regeneratorRuntime.wrap(function retryUntilLimitReached$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          retries = 0;

        case 1:
          if (!(retries < _constants.QUEUE_RETRIES_LIMIT)) {
            _context12.next = 49;
            break;
          }

          _context12.next = 4;
          return (0, _effects.call)(_requestsProgess.createRequestChannel, apiCallFn, apiCallFnArguments);

        case 4:
          requestChannel = _context12.sent;

        case 5:
          if (!true) {
            _context12.next = 44;
            break;
          }

          _context12.next = 8;
          return (0, _effects.take)(requestChannel);

        case 8:
          _ref2 = _context12.sent;
          _ref2$loaded = _ref2.loaded;
          loaded = _ref2$loaded === undefined ? 0 : _ref2$loaded;
          _ref2$total = _ref2.total;
          total = _ref2$total === undefined ? 0 : _ref2$total;
          error = _ref2.error;
          response = _ref2.response;

          if (!response) {
            _context12.next = 19;
            break;
          }

          _context12.next = 18;
          return (0, _effects.put)(successAction(response.data));

        case 18:
          return _context12.abrupt('return', _context12.sent);

        case 19:
          if (!error) {
            _context12.next = 32;
            break;
          }

          status = (0, _get3.default)(error, 'response.status');

          if (status) {
            _context12.next = 25;
            break;
          }

          // if there is no status, then its a network issue,
          // so don't count this as retry
          retries -= 1;
          _context12.next = 29;
          break;

        case 25:
          if (!(status >= 400 && status <= 500)) {
            _context12.next = 29;
            break;
          }

          _context12.next = 28;
          return (0, _effects.put)((0, _actions.chatSchemaQueueFailedAction)(_constants.QUEUE_FAILURE_BAD_REQUEST, error));

        case 28:
          return _context12.abrupt('return', _context12.sent);

        case 29:
          _context12.next = 31;
          return (0, _effects.put)(failedAction(error));

        case 31:
          return _context12.abrupt('break', 44);

        case 32:
          _context12.next = 34;
          return (0, _effects.call)(_requestsProgess.updateRequestProgress, requestUid, loaded, total);

        case 34:
          _context12.next = 36;
          return (0, _effects.call)(_requestsProgess.getTotalRequestsProgress);

        case 36:
          currentTotalRequestsProgress = _context12.sent;
          totalRequestsDiff = Math.abs(currentTotalRequestsProgress - lastTotalRequestsProgress);

          if (!(totalRequestsDiff >= _requestsProgess.MIN_PROGRESS_DIFF)) {
            _context12.next = 42;
            break;
          }

          lastTotalRequestsProgress = currentTotalRequestsProgress;
          _context12.next = 42;
          return (0, _effects.put)((0, _actions.chatSchemaQueueSetRequestsProgressAction)(currentTotalRequestsProgress));

        case 42:
          _context12.next = 5;
          break;

        case 44:
          _context12.next = 46;
          return (0, _effects.call)(_reduxSaga.delay, _constants.QUEUE_RETRY_DELAY);

        case 46:
          retries += 1;
          _context12.next = 1;
          break;

        case 49:
          _context12.next = 51;
          return (0, _effects.put)((0, _actions.chatSchemaQueueFailedAction)(_constants.QUEUE_FAILURE_RETRIES_LIMIT_REACHED));

        case 51:
          return _context12.abrupt('return', _context12.sent);

        case 52:
        case 'end':
          return _context12.stop();
      }
    }
  }, _marked12, this);
}

exports.default = [watchChatLoadSchemaSaga, watchChatLoadOnboardingSchemaSaga, watchSchemaQueueSaga, watchSendQuestionReplySaga, watchChatSchemaQueueClearFinishedRequestsSaga];