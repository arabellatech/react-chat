'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _selectors = require('./selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var withSchemaQueueStatus = function withSchemaQueueStatus(WrappedComponent) {
  var mapStateToProps = function mapStateToProps(state) {
    return {
      isSchemaQueueEnded: (0, _selectors.selectChatSchemaQueueStatusIsEnded)(state),
      isSchemaQueueFailed: (0, _selectors.selectChatSchemaQueueStatusIsFailed)(state)
    };
  };

  var SchemaQueueStatusHOC = function SchemaQueueStatusHOC(props) {
    return _react2.default.createElement(WrappedComponent, props);
  };

  return (0, _reactRedux.connect)(mapStateToProps)(SchemaQueueStatusHOC);
};

exports.default = withSchemaQueueStatus;