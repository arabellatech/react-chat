'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chatQuestionAnimationEndAction = exports.chatQuestionAnimationStartAction = exports.chatSchemaQueueClearFinishedRequestsFromProgressAction = exports.chatSchemaQueueSetRequestsProgressAction = exports.chatSchemaQueueFailedAction = exports.chatSchemaQueueEndedAction = exports.chatSchemaQueueStartedAction = exports.chatSchemaPutRequestToQueue = exports.chatLoadOnboardingSchemaFailedAction = exports.chatLoadOnboardingSchemaSuccessAction = exports.chatLoadOnboardingSchemaAction = exports.chatLoadSchemaFailedAction = exports.chatLoadSchemaSuccessAction = exports.chatLoadSchemaAction = exports.chatThreadRollbackFailedAction = exports.chatThreadRollbackSuccessAction = exports.chatThreadRollbackAction = exports.chatSetThreadIdAction = undefined;
exports.chatSelectAnswerAction = chatSelectAnswerAction;
exports.chatUnselectAnswerAction = chatUnselectAnswerAction;
exports.chatSetAnswerAction = chatSetAnswerAction;
exports.chatSendQuestionReplyAction = chatSendQuestionReplyAction;
exports.chatSendQuestionReplySuccessAction = chatSendQuestionReplySuccessAction;
exports.chatSendQuestionReplyFailedAction = chatSendQuestionReplyFailedAction;
exports.chatChangeQuestionAction = chatChangeQuestionAction;
exports.chatNextQuestionAction = chatNextQuestionAction;

var _constants = require('./constants');

/* thread */

var chatSetThreadIdAction = exports.chatSetThreadIdAction = function chatSetThreadIdAction(id) {
  return {
    type: _constants.CHAT_SET_THREAD_ID_ACTION,
    id: id
  };
};

var chatThreadRollbackAction = exports.chatThreadRollbackAction = function chatThreadRollbackAction(threadId, feId, questionId) {
  return {
    type: _constants.CHAT_THREAD_ROLLBACK_ACTION,
    threadId: threadId,
    feId: feId,
    questionId: questionId,
    requestUid: new Date().getTime()
  };
};

var chatThreadRollbackSuccessAction = exports.chatThreadRollbackSuccessAction = function chatThreadRollbackSuccessAction(payload) {
  return {
    type: _constants.CHAT_THREAD_ROLLBACK_SUCCESS_ACTION,
    payload: payload
  };
};

var chatThreadRollbackFailedAction = exports.chatThreadRollbackFailedAction = function chatThreadRollbackFailedAction(payload) {
  return {
    type: _constants.CHAT_THREAD_ROLLBACK_FAILED_ACTION,
    payload: payload
  };
};

/* schema */

var chatLoadSchemaAction = exports.chatLoadSchemaAction = function chatLoadSchemaAction(id) {
  return {
    type: _constants.CHAT_LOAD_SCHEMA_ACTION,
    id: id
  };
};

var chatLoadSchemaSuccessAction = exports.chatLoadSchemaSuccessAction = function chatLoadSchemaSuccessAction(payload) {
  return {
    type: _constants.CHAT_LOAD_SCHEMA_SUCCESS_ACTION,
    payload: payload
  };
};

var chatLoadSchemaFailedAction = exports.chatLoadSchemaFailedAction = function chatLoadSchemaFailedAction(payload) {
  return {
    type: _constants.CHAT_LOAD_SCHEMA_FAILED_ACTION,
    payload: payload
  };
};

var chatLoadOnboardingSchemaAction = exports.chatLoadOnboardingSchemaAction = function chatLoadOnboardingSchemaAction() {
  return {
    type: _constants.CHAT_LOAD_ONBOARDING_SCHEMA_ACTION
  };
};

var chatLoadOnboardingSchemaSuccessAction = exports.chatLoadOnboardingSchemaSuccessAction = function chatLoadOnboardingSchemaSuccessAction(payload) {
  return {
    type: _constants.CHAT_LOAD_ONBOARDING_SCHEMA_SUCCESS_ACTION,
    payload: payload
  };
};

var chatLoadOnboardingSchemaFailedAction = exports.chatLoadOnboardingSchemaFailedAction = function chatLoadOnboardingSchemaFailedAction(payload) {
  return {
    type: _constants.CHAT_LOAD_ONBOARDING_SCHEMA_FAILED_ACTION,
    payload: payload
  };
};

var chatSchemaPutRequestToQueue = exports.chatSchemaPutRequestToQueue = function chatSchemaPutRequestToQueue(requestAction) {
  return {
    type: _constants.CHAT_SCHEMA_PUT_REQUEST_TO_QUEUE_ACTION,
    requestAction: requestAction
  };
};

var chatSchemaQueueStartedAction = exports.chatSchemaQueueStartedAction = function chatSchemaQueueStartedAction() {
  return {
    type: _constants.CHAT_SCHEMA_QUEUE_STARTED_ACTION
  };
};

var chatSchemaQueueEndedAction = exports.chatSchemaQueueEndedAction = function chatSchemaQueueEndedAction() {
  return {
    type: _constants.CHAT_SCHEMA_QUEUE_ENDED_ACTION
  };
};

var chatSchemaQueueFailedAction = exports.chatSchemaQueueFailedAction = function chatSchemaQueueFailedAction(reason, payload) {
  return {
    type: _constants.CHAT_SCHEMA_QUEUE_FAILED_ACTION,
    reason: reason,
    payload: payload
  };
};

var chatSchemaQueueSetRequestsProgressAction = exports.chatSchemaQueueSetRequestsProgressAction = function chatSchemaQueueSetRequestsProgressAction(requestsProgress) {
  return {
    type: _constants.CHAT_SCHEMA_QUEUE_SET_REQUESTS_PROGRESS_ACTION,
    requestsProgress: requestsProgress
  };
};

var chatSchemaQueueClearFinishedRequestsFromProgressAction = exports.chatSchemaQueueClearFinishedRequestsFromProgressAction = function chatSchemaQueueClearFinishedRequestsFromProgressAction() {
  return {
    type: _constants.CHAT_SCHEMA_QUEUE_CLEAR_FINISHED_REQUESTS_ACTION
  };
};

/* answers */

function chatSelectAnswerAction(answer) {
  return {
    type: _constants.CHAT_SELECT_ANSWER_ACTION,
    answer: answer
  };
}

function chatUnselectAnswerAction(answer) {
  return {
    type: _constants.CHAT_UNSELECT_ANSWER_ACTION,
    answer: answer
  };
}

function chatSetAnswerAction(answer) {
  return {
    type: _constants.CHAT_SET_ANSWER_ACTION,
    answer: answer
  };
}

/* current question */

function chatSendQuestionReplyAction(reply) {
  return {
    type: _constants.CHAT_SEND_QUESTION_REPLY_ACTION,
    reply: reply,
    requestUid: new Date().getTime()
  };
}

function chatSendQuestionReplySuccessAction(payload) {
  return {
    type: _constants.CHAT_SEND_QUESTION_REPLY_SUCCESS_ACTION,
    payload: payload
  };
}

function chatSendQuestionReplyFailedAction(payload) {
  return {
    type: _constants.CHAT_SEND_QUESTION_REPLY_FAILED_ACTION,
    payload: payload
  };
}

function chatChangeQuestionAction(question) {
  return {
    type: _constants.CHAT_CHANGE_QUESTION_ACTION,
    question: question
  };
}

function chatNextQuestionAction() {
  return {
    type: _constants.CHAT_NEXT_QUESTION_ACTION
  };
}

/* animations */

var chatQuestionAnimationStartAction = exports.chatQuestionAnimationStartAction = function chatQuestionAnimationStartAction(animation) {
  return {
    type: _constants.CHAT_QUESTION_ANIMATION_START,
    animation: animation
  };
};

var chatQuestionAnimationEndAction = exports.chatQuestionAnimationEndAction = function chatQuestionAnimationEndAction(animation) {
  return {
    type: _constants.CHAT_QUESTION_ANIMATION_END,
    animation: animation
  };
};