'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MIN_PROGRESS_DIFF = undefined;
exports.createRequestChannel = createRequestChannel;
exports.addPendingRequest = addPendingRequest;
exports.updateRequestProgress = updateRequestProgress;
exports.clearFinishedRequests = clearFinishedRequests;
exports.getTotalRequestsProgress = getTotalRequestsProgress;
exports.getRequestsLoadedAmount = getRequestsLoadedAmount;
exports.getRequestsTotalAmount = getRequestsTotalAmount;

var _immutable = require('immutable');

var _reduxSaga = require('redux-saga');

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var requests = (0, _immutable.fromJS)({
  prevTotal: 0,
  items: {}
});

var MIN_PROGRESS_DIFF = exports.MIN_PROGRESS_DIFF = 2;

function createRequestChannel(apiCallFn, apiCallFnArguments) {
  return (0, _reduxSaga.eventChannel)(function (emitter) {
    var requestConfig = {
      onUploadProgress: function onUploadProgress(event) {
        if (event.lengthComputable) {
          var loaded = event.loaded,
              total = event.total;


          emitter({
            loaded: loaded,
            total: total
          });
        }
      }
    };

    apiCallFn.apply(undefined, _toConsumableArray(apiCallFnArguments).concat([requestConfig])).then(function (response) {
      emitter({
        response: response
      });
    }).catch(function (error) {
      emitter({
        error: error
      });
    }).then(function () {
      emitter(_reduxSaga.END);
    });

    // The subscriber must return an unsubscribe function
    return function () {};
  });
}

function addPendingRequest(requestUid) {
  var approximateTotal = requests.get('prevTotal') * 1.05;

  requests = requests.mergeIn(['items', requestUid], {
    isApproximate: true,
    loaded: 0,
    total: approximateTotal
  });
}

function updateRequestProgress(requestUid, loaded, total) {
  var isApproximate = requests.getIn(['items', requestUid, 'isApproximate']);

  if (isApproximate) {
    requests = requests.update('prevTotal', function (prevTotal) {
      return prevTotal > total ? prevTotal : total;
    });
  }

  requests = requests.mergeIn(['items', requestUid], {
    loaded: loaded,
    total: total,
    isApproximate: false
  });
}

function clearFinishedRequests() {
  requests = requests.updateIn(['items'], function (items) {
    return items.filter(function (item) {
      return item.get('total') !== item.get('loaded');
    });
  });
}

function getTotalRequestsProgress() {
  var loaded = getRequestsLoadedAmount();
  var total = getRequestsTotalAmount();

  return total ? Math.round(100 * loaded / total) : 100;
}

function getRequestsLoadedAmount() {
  return requests.get('items').reduce(function (sum, item) {
    return sum + item.get('loaded');
  }, 0);
}

function getRequestsTotalAmount() {
  return requests.get('items').reduce(function (sum, item) {
    return sum + item.get('total');
  }, 0);
}