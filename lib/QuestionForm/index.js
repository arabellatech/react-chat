'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _bindAll2 = require('lodash/bindAll');

var _bindAll3 = _interopRequireDefault(_bindAll2);

var _isFunction2 = require('lodash/isFunction');

var _isFunction3 = _interopRequireDefault(_isFunction2);

var _delay2 = require('lodash/delay');

var _delay3 = _interopRequireDefault(_delay2);

var _actions = require('../Schema/actions');

var _actions2 = require('../VariablesPool/actions');

var _selectors = require('../Schema/selectors');

var _selectors2 = require('../Chat/selectors');

var _selectors3 = require('../Message/selectors');

var _selectors4 = require('../VariablesPool/selectors');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapStateToProps = function mapStateToProps(state) {
  return {
    currentQuestion: (0, _selectors.selectChatCurrentQuestion)(state),
    currentQuestionReply: (0, _selectors.selectChatCurrentReply)(state),
    answeredQuestions: (0, _selectors.selectChatAnsweredQuestions)(state),
    customVariables: (0, _selectors4.selectChatCustomVariables)(state),
    animations: (0, _selectors2.selectChatAnimations)(state),
    threadId: (0, _selectors3.selectChatThreadId)(state)
  };
};

var mapDispatchToProps = {
  selectAnswer: _actions.chatSelectAnswerAction,
  unselectAnswer: _actions.chatUnselectAnswerAction,
  setAnswer: _actions.chatSetAnswerAction,
  sendQuestionReply: _actions.chatSendQuestionReplyAction,
  setCustomVariables: _actions2.chatSetCustomVariablesAction,
  changeQuestion: _actions.chatChangeQuestionAction,
  animationStart: _actions.chatQuestionAnimationStartAction,
  animationEnd: _actions.chatQuestionAnimationEndAction
};

var QuestionForm = function (_PureComponent) {
  _inherits(QuestionForm, _PureComponent);

  function QuestionForm(props) {
    _classCallCheck(this, QuestionForm);

    var _this = _possibleConstructorReturn(this, (QuestionForm.__proto__ || Object.getPrototypeOf(QuestionForm)).call(this, props));

    _this.state = {
      submitting: false
    };


    if (!props.type) {
      throw Error('props.type must be defined!');
    }

    if (!props.component) {
      throw Error('props.component must be defined!');
    }

    (0, _bindAll3.default)(_this, ['animateCSSClass', 'ensureSingleSubmit', 'getAnswerById', 'getSelectedAnswers', 'handleSubmit', 'startAnimation']);
    return _this;
  }

  _createClass(QuestionForm, [{
    key: 'getAnswerById',
    value: function getAnswerById(id) {
      // TODO fix type
      var foundAnswer = (0, _selectors.selectAnswers)(this.props.currentQuestion).find(function (answer) {
        return answer.get('id') === id;
      });

      return foundAnswer ? foundAnswer.toJS() : null;
    }
  }, {
    key: 'getSelectedAnswers',
    value: function getSelectedAnswers() {
      // TODO fix type
      var foundAnswers = (0, _selectors.selectSelectedAnswers)(this.props.currentQuestion);

      return foundAnswers.toJS();
    }
  }, {
    key: 'getQuestionType',
    value: function getQuestionType() {
      var _props = this.props,
          currentQuestion = _props.currentQuestion,
          getType = _props.getType;

      var type = void 0;

      if (getType && (0, _isFunction3.default)(getType)) {
        type = getType(currentQuestion);
      } else {
        var metaType = currentQuestion.getIn(['meta', 'type']);
        var questionType = currentQuestion.get('type');

        type = metaType || questionType;
      }

      return type;
    }
  }, {
    key: 'handleSubmit',
    value: function handleSubmit(event) {
      if (event && event.preventDefault) {
        event.preventDefault();
      }

      var _props2 = this.props,
          currentQuestionReply = _props2.currentQuestionReply,
          sendQuestionReply = _props2.sendQuestionReply,
          threadId = _props2.threadId;

      var questionReply = _extends({}, currentQuestionReply, {
        threadId: threadId
      });

      // $FlowFixMe
      sendQuestionReply(questionReply);
    }
  }, {
    key: 'startAnimation',
    value: function startAnimation(_ref) {
      var type = _ref.type,
          duration = _ref.duration;

      if (type === 'IN') {
        this.animateIn(duration);
      } else if (type === 'OUT') {
        this.animateOut(duration);
      }
    }
  }, {
    key: 'animateOut',
    value: function animateOut(duration) {
      var _props3 = this.props,
          animationStart = _props3.animationStart,
          animationEnd = _props3.animationEnd;


      animationStart('out');

      (0, _delay3.default)(function () {
        animationEnd('out');
      }, duration);
    }
  }, {
    key: 'animateIn',
    value: function animateIn(duration) {
      var _props4 = this.props,
          animationStart = _props4.animationStart,
          animationEnd = _props4.animationEnd;


      animationStart('in');

      (0, _delay3.default)(function () {
        animationEnd('in');
      }, duration);
    }
  }, {
    key: 'animateCSSClass',
    value: function animateCSSClass(baseClassName) {
      var animations = this.props.animations;

      var classNames = [baseClassName];
      var questionAnimation = animations.get('question');

      if (questionAnimation === 'out-started') {
        classNames.push(baseClassName + '--out');
      } else if (questionAnimation !== 'out-ended') {
        classNames.push(baseClassName + '--in');
      }

      return classNames.join(' ');
    }
  }, {
    key: 'isMatchingType',
    value: function isMatchingType() {
      var expectedType = this.props.type;
      var currentType = this.getQuestionType();
      var isMatchingType = expectedType === currentType;

      return isMatchingType;
    }
  }, {
    key: 'ensureSingleSubmit',
    value: function ensureSingleSubmit(callback) {
      var _this2 = this;

      if (!this.submitting) {
        this.setSubmitting(true);

        callback();

        setTimeout(function () {
          _this2.setSubmitting(false);
        }, 2000);
      }
    }
  }, {
    key: 'setSubmitting',
    value: function setSubmitting(submitting) {
      this.submitting = submitting;

      this.setState({
        submitting: submitting
      });
    }
  }, {
    key: 'composeProps',
    value: function composeProps() {
      var _props5 = this.props,
          currentQuestion = _props5.currentQuestion,
          animations = _props5.animations,
          animationStart = _props5.animationStart,
          animationEnd = _props5.animationEnd,
          parentProps = _objectWithoutProperties(_props5, ['currentQuestion', 'animations', 'animationStart', 'animationEnd']);

      var props = _extends({}, parentProps, {
        animateCSSClass: this.animateCSSClass,
        onSubmit: this.handleSubmit,
        startAnimation: this.startAnimation,
        getAnswerById: this.getAnswerById,
        getSelectedAnswers: this.getSelectedAnswers,
        submitting: this.state.submitting,
        ensureSingleSubmit: this.ensureSingleSubmit,
        // TODO http://redux.js.org/docs/recipes/UsingImmutableJS.html#use-a-higher-order-component-to-convert-your-smart-components-immutablejs-props-to-your-dumb-components-javascript-props
        // convert from immutable to simple object
        // for use in dumb components
        // TODO fix type
        question: currentQuestion.toJS()
      });

      return props;
    }
  }, {
    key: 'render',
    value: function render() {
      if (!this.isMatchingType()) {
        return null;
      }

      var props = this.composeProps();
      var Component = this.props.component;

      return _react2.default.createElement(Component, props);
    }
  }]);

  return QuestionForm;
}(_react.PureComponent);

// override state and dispatch props with own props


var mergeProps = function mergeProps(stateProps, dispatchProps, ownProps) {
  return _extends({}, stateProps, dispatchProps, ownProps);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, mergeProps)(QuestionForm);