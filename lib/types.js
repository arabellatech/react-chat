'use strict';

var _immutable = require('immutable');

/* chat message */

// Possible statuses to determine the current state of sending the message to API.


// TODO
// {
//  questions: Array<ChatQuestionType>,
//  currentQuestion: ChatQuestionType | null,
//  confirmedQuestions: Array<ChatQuestionType> | null,
// }
var chatMessageStatuses = {
  loading: 'loading',
  success: 'success',
  error: 'error'
};

// Shape of the single message


// Shape of the message bubble group
// Bubbles are grouped by the sender to handle single avatar for each group
// of messages sent by the same user
// showOnRight is to determine the side of appearance
//  if true show bubbles group on the right-hand side
//  if false show bubbles group on the left-hand side with avatar


// Shape of the list of messages grouped by the sender


// Shape of the sender
// TODO: MM: possibly should be moved to more generic file.


// TODO: MM: create shape
// TODO: MM: possibly should be moved to more generic file.


/* chat question */

/* props */

/* Backend data format */
/* this is exactly what we get from API response */


/* actions */
/* thread */

/* schema */

/* onboarding schema */

/* answers */

// TODO pass only answer id


// TODO pass only answer id


// TODO pass only answer id


/* questions */

/* message */

/* schema queue */

/* variables pool */