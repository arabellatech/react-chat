'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chatSetCustomVariablesAction = exports.chatSetVariablesPoolAction = undefined;

var _constants = require('./constants');

var chatSetVariablesPoolAction = exports.chatSetVariablesPoolAction = function chatSetVariablesPoolAction(variablesPool) {
  return {
    type: _constants.CHAT_SET_VARIABLES_POOL_ACTION,
    variablesPool: variablesPool
  };
};

var chatSetCustomVariablesAction = exports.chatSetCustomVariablesAction = function chatSetCustomVariablesAction(variables) {
  return {
    type: _constants.CHAT_SET_CUSTOM_VARIABLES_ACTION,
    variables: variables
  };
};