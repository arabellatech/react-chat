'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});


/* actions */

var CHAT_SET_VARIABLES_POOL_ACTION = exports.CHAT_SET_VARIABLES_POOL_ACTION = 'app/Chat/SET_VARIABLES_POOL';
var CHAT_SET_CUSTOM_VARIABLES_ACTION = exports.CHAT_SET_CUSTOM_VARIABLES_ACTION = 'app/Chat/SET_CUSTOM_VARIABLES';