'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var onChatSetCustomVariablesAction = exports.onChatSetCustomVariablesAction = function onChatSetCustomVariablesAction(state, action) {
  return state.mergeIn(['customVariables'], action.variables || {});
};
var onChatSetVariablesPoolAction = exports.onChatSetVariablesPoolAction = function onChatSetVariablesPoolAction(state, action) {
  return state.mergeIn(['variablesPool'], action.variablesPool || {});
};