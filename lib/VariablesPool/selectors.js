'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectChatCustomVariables = exports.selectVariablesPool = exports.getCustomVariables = exports.getVariablesPool = undefined;

var _reselect = require('reselect');

var _selectors = require('../Chat/selectors');

var getVariablesPool = exports.getVariablesPool = function getVariablesPool(state) {
  return state.get('variablesPool');
};
var getCustomVariables = exports.getCustomVariables = function getCustomVariables(state) {
  return state.get('customVariables');
};

var selectVariablesPool = exports.selectVariablesPool = (0, _reselect.createSelector)(_selectors.getChatDomain, getVariablesPool);

var selectChatCustomVariables = exports.selectChatCustomVariables = (0, _reselect.createSelector)(_selectors.getChatDomain, getCustomVariables);