'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.threadRollback = exports.threadWaitForResponse = exports.threadClose = exports.threadOpen = exports.sendMessage = exports.sendQuestionReply = exports.getChatOnboardingSchema = exports.getChatSchema = undefined;
exports.loadMessages = loadMessages;

var _apiClient = require('api-client');

var _apiClient2 = _interopRequireDefault(_apiClient);

var _constants = require('../Thread/constants');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getChatSchema = exports.getChatSchema = function getChatSchema(id) {
  return _apiClient2.default.get('/schemas/' + id);
};

var getChatOnboardingSchema = exports.getChatOnboardingSchema = function getChatOnboardingSchema() {
  return _apiClient2.default.get('/public/onboarding-schema');
};

var sendQuestionReply = exports.sendQuestionReply = function sendQuestionReply(reply, requestConfig) {
  return _apiClient2.default.post('/threads/' + reply.threadId + '/messages', reply, requestConfig);
};

var sendMessage = exports.sendMessage = function sendMessage(message) {
  return _apiClient2.default.post('/threads/' + message.threadId + '/messages', message);
};

function loadMessages(threadId) {
  var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { limit: 9999, filtering: 'type__in:message', ordering: 'id' };

  return _apiClient2.default.get('/threads/' + threadId + '/messages', { params: params });
}

var threadOpen = exports.threadOpen = function threadOpen(threadId) {
  return _apiClient2.default.patch('/threads/' + threadId, {
    state: _constants.OPEN
  });
};

var threadClose = exports.threadClose = function threadClose(threadId) {
  return _apiClient2.default.patch('/threads/' + threadId, {
    state: _constants.CLOSED
  });
};

var threadWaitForResponse = exports.threadWaitForResponse = function threadWaitForResponse(threadId) {
  return _apiClient2.default.patch('/threads/' + threadId, {
    state: _constants.WAITING_FOR_RESPONSE
  });
};

var threadRollback = exports.threadRollback = function threadRollback(threadId, questionId, requestConfig) {
  return _apiClient2.default.post('/threads/' + threadId + '/rollback', { threadId: threadId, question: questionId }, requestConfig);
};