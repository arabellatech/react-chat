'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.chatThreadWaitForResponseFailedAction = exports.chatThreadWaitForResponseSuccessAction = exports.chatThreadWaitForResponseAction = exports.chatThreadCloseFailedAction = exports.chatThreadCloseSuccessAction = exports.chatThreadCloseAction = exports.chatThreadOpenFailedAction = exports.chatThreadOpenSuccessAction = exports.chatThreadOpenAction = undefined;

var _constants = require('./constants');

/* thread */

var chatThreadOpenAction = exports.chatThreadOpenAction = function chatThreadOpenAction(threadId) {
  return {
    type: _constants.CHAT_THREAD_OPEN_ACTION,
    threadId: threadId
  };
};

var chatThreadOpenSuccessAction = exports.chatThreadOpenSuccessAction = function chatThreadOpenSuccessAction(payload) {
  return {
    type: _constants.CHAT_THREAD_OPEN_SUCCESS_ACTION,
    payload: payload
  };
};

var chatThreadOpenFailedAction = exports.chatThreadOpenFailedAction = function chatThreadOpenFailedAction(payload) {
  return {
    type: _constants.CHAT_THREAD_OPEN_FAILED_ACTION,
    payload: payload
  };
};

var chatThreadCloseAction = exports.chatThreadCloseAction = function chatThreadCloseAction(threadId) {
  return {
    type: _constants.CHAT_THREAD_CLOSE_ACTION,
    threadId: threadId
  };
};

var chatThreadCloseSuccessAction = exports.chatThreadCloseSuccessAction = function chatThreadCloseSuccessAction(payload) {
  return {
    type: _constants.CHAT_THREAD_CLOSE_SUCCESS_ACTION,
    payload: payload
  };
};

var chatThreadCloseFailedAction = exports.chatThreadCloseFailedAction = function chatThreadCloseFailedAction(payload) {
  return {
    type: _constants.CHAT_THREAD_CLOSE_FAILED_ACTION,
    payload: payload
  };
};

var chatThreadWaitForResponseAction = exports.chatThreadWaitForResponseAction = function chatThreadWaitForResponseAction(threadId) {
  return {
    type: _constants.CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION,
    threadId: threadId
  };
};

var chatThreadWaitForResponseSuccessAction = exports.chatThreadWaitForResponseSuccessAction = function chatThreadWaitForResponseSuccessAction(payload) {
  return {
    type: _constants.CHAT_THREAD_WAIT_FOR_RESPONSE_SUCCESS_ACTION,
    payload: payload
  };
};

var chatThreadWaitForResponseFailedAction = exports.chatThreadWaitForResponseFailedAction = function chatThreadWaitForResponseFailedAction(payload) {
  return {
    type: _constants.CHAT_THREAD_WAIT_FOR_RESPONSE_FAILED_ACTION,
    payload: payload
  };
};