'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchChatThreadOpenSaga = watchChatThreadOpenSaga;
exports.chatThreadOpenSaga = chatThreadOpenSaga;
exports.watchChatThreadCloseSaga = watchChatThreadCloseSaga;
exports.chatThreadCloseSaga = chatThreadCloseSaga;
exports.watchChatThreadWaitForResponseSaga = watchChatThreadWaitForResponseSaga;
exports.chatThreadWaitForResponseSaga = chatThreadWaitForResponseSaga;
exports.watchChatSendMessageAndCloseThreadSaga = watchChatSendMessageAndCloseThreadSaga;
exports.chatThreadCloseAfterMessageSendSaga = chatThreadCloseAfterMessageSendSaga;
exports.watchChatSendMessageAndOpenThreadSaga = watchChatSendMessageAndOpenThreadSaga;
exports.chatThreadOpenAfterMessageSendSaga = chatThreadOpenAfterMessageSendSaga;
exports.watchChatSendMessageAndWaitForResponseSaga = watchChatSendMessageAndWaitForResponseSaga;
exports.chatThreadWaitForResponseAfterMessageSendSaga = chatThreadWaitForResponseAfterMessageSendSaga;

var _effects = require('redux-saga/effects');

var _get2 = require('lodash/get');

var _get3 = _interopRequireDefault(_get2);

var _actions = require('./actions');

var _constants = require('./constants');

var _constants2 = require('../Message/constants');

var _api = require('../api');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(watchChatThreadOpenSaga),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadOpenSaga),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(watchChatThreadCloseSaga),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadCloseSaga),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(watchChatThreadWaitForResponseSaga),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadWaitForResponseSaga),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSendMessageAndCloseThreadSaga),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadCloseAfterMessageSendSaga),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSendMessageAndOpenThreadSaga),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadOpenAfterMessageSendSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(watchChatSendMessageAndWaitForResponseSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(chatThreadWaitForResponseAfterMessageSendSaga); // TODO: use flow

function watchChatThreadOpenSaga() {
  return regeneratorRuntime.wrap(function watchChatThreadOpenSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_THREAD_OPEN_ACTION, chatThreadOpenSaga);

        case 2:
        case 'end':
          return _context.stop();
      }
    }
  }, _marked, this);
}

function chatThreadOpenSaga(action) {
  var response;
  return regeneratorRuntime.wrap(function chatThreadOpenSaga$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(_api.threadOpen, action.threadId);

        case 3:
          response = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)((0, _actions.chatThreadOpenSuccessAction)(response.data));

        case 6:
          _context2.next = 12;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2['catch'](0);
          _context2.next = 12;
          return (0, _effects.put)((0, _actions.chatThreadOpenFailedAction)(_context2.t0));

        case 12:
        case 'end':
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 8]]);
}

function watchChatThreadCloseSaga() {
  return regeneratorRuntime.wrap(function watchChatThreadCloseSaga$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_THREAD_CLOSE_ACTION, chatThreadCloseSaga);

        case 2:
        case 'end':
          return _context3.stop();
      }
    }
  }, _marked3, this);
}

function chatThreadCloseSaga(action) {
  var response;
  return regeneratorRuntime.wrap(function chatThreadCloseSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.call)(_api.threadClose, action.threadId);

        case 3:
          response = _context4.sent;
          _context4.next = 6;
          return (0, _effects.put)((0, _actions.chatThreadCloseSuccessAction)(response.data));

        case 6:
          _context4.next = 12;
          break;

        case 8:
          _context4.prev = 8;
          _context4.t0 = _context4['catch'](0);
          _context4.next = 12;
          return (0, _effects.put)((0, _actions.chatThreadCloseFailedAction)(_context4.t0));

        case 12:
        case 'end':
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 8]]);
}

function watchChatThreadWaitForResponseSaga() {
  return regeneratorRuntime.wrap(function watchChatThreadWaitForResponseSaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _effects.takeEvery)(_constants.CHAT_THREAD_WAIT_FOR_RESPONSE_ACTION, chatThreadWaitForResponseSaga);

        case 2:
        case 'end':
          return _context5.stop();
      }
    }
  }, _marked5, this);
}

function chatThreadWaitForResponseSaga(action) {
  var response;
  return regeneratorRuntime.wrap(function chatThreadWaitForResponseSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.call)(_api.threadWaitForResponse, action.threadId);

        case 3:
          response = _context6.sent;
          _context6.next = 6;
          return (0, _effects.put)((0, _actions.chatThreadWaitForResponseSuccessAction)(response.data));

        case 6:
          _context6.next = 12;
          break;

        case 8:
          _context6.prev = 8;
          _context6.t0 = _context6['catch'](0);
          _context6.next = 12;
          return (0, _effects.put)((0, _actions.chatThreadWaitForResponseFailedAction)(_context6.t0));

        case 12:
        case 'end':
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 8]]);
}

function watchChatSendMessageAndCloseThreadSaga() {
  return regeneratorRuntime.wrap(function watchChatSendMessageAndCloseThreadSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _effects.takeEvery)(_constants2.CHAT_SEND_MESSAGE_AND_CLOSE_THREAD_ACTION, chatThreadCloseAfterMessageSendSaga);

        case 2:
        case 'end':
          return _context7.stop();
      }
    }
  }, _marked7, this);
}

function chatThreadCloseAfterMessageSendSaga() {
  var action, threadId;
  return regeneratorRuntime.wrap(function chatThreadCloseAfterMessageSendSaga$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.take)([_constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION, _constants2.CHAT_SEND_MESSAGE_FAILED_ACTION]);

        case 2:
          action = _context8.sent;
          threadId = (0, _get3.default)(action, 'payload.thread');

          if (!(threadId && action.type === _constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION)) {
            _context8.next = 7;
            break;
          }

          _context8.next = 7;
          return (0, _effects.put)((0, _actions.chatThreadCloseAction)(threadId));

        case 7:
        case 'end':
          return _context8.stop();
      }
    }
  }, _marked8, this);
}

function watchChatSendMessageAndOpenThreadSaga() {
  return regeneratorRuntime.wrap(function watchChatSendMessageAndOpenThreadSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _effects.takeEvery)(_constants2.CHAT_SEND_MESSAGE_AND_OPEN_THREAD_ACTION, chatThreadOpenAfterMessageSendSaga);

        case 2:
        case 'end':
          return _context9.stop();
      }
    }
  }, _marked9, this);
}

function chatThreadOpenAfterMessageSendSaga() {
  var action, threadId;
  return regeneratorRuntime.wrap(function chatThreadOpenAfterMessageSendSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.take)([_constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION, _constants2.CHAT_SEND_MESSAGE_FAILED_ACTION]);

        case 2:
          action = _context10.sent;
          threadId = (0, _get3.default)(action, 'payload.thread');

          if (!(threadId && action.type === _constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION)) {
            _context10.next = 7;
            break;
          }

          _context10.next = 7;
          return (0, _effects.put)((0, _actions.chatThreadOpenAction)(threadId));

        case 7:
        case 'end':
          return _context10.stop();
      }
    }
  }, _marked10, this);
}

function watchChatSendMessageAndWaitForResponseSaga() {
  return regeneratorRuntime.wrap(function watchChatSendMessageAndWaitForResponseSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _effects.takeEvery)(_constants2.CHAT_SEND_MESSAGE_AND_WAIT_FOR_RESPONSE_ACTION, chatThreadWaitForResponseAfterMessageSendSaga);

        case 2:
        case 'end':
          return _context11.stop();
      }
    }
  }, _marked11, this);
}

function chatThreadWaitForResponseAfterMessageSendSaga() {
  var action, threadId;
  return regeneratorRuntime.wrap(function chatThreadWaitForResponseAfterMessageSendSaga$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.next = 2;
          return (0, _effects.take)([_constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION, _constants2.CHAT_SEND_MESSAGE_FAILED_ACTION]);

        case 2:
          action = _context12.sent;
          threadId = (0, _get3.default)(action, 'payload.thread');

          if (!(threadId && action.type === _constants2.CHAT_SEND_MESSAGE_SUCCESS_ACTION)) {
            _context12.next = 7;
            break;
          }

          _context12.next = 7;
          return (0, _effects.put)((0, _actions.chatThreadWaitForResponseAction)(threadId));

        case 7:
        case 'end':
          return _context12.stop();
      }
    }
  }, _marked12, this);
}

// All sagas to be loaded
exports.default = [watchChatThreadOpenSaga, watchChatThreadCloseSaga, watchChatThreadWaitForResponseSaga, watchChatSendMessageAndCloseThreadSaga, watchChatSendMessageAndOpenThreadSaga, watchChatSendMessageAndWaitForResponseSaga];