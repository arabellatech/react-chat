'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _dec, _class, _class2, _temp;

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _bindAll2 = require('lodash/bindAll');

var _bindAll3 = _interopRequireDefault(_bindAll2);

var _isFunction2 = require('lodash/isFunction');

var _isFunction3 = _interopRequireDefault(_isFunction2);

var _fecha = require('fecha');

var _fecha2 = _interopRequireDefault(_fecha);

var _EditBtnDesktop = require('./EditBtnDesktop');

var _EditBtnDesktop2 = _interopRequireDefault(_EditBtnDesktop);

var _EditBtnMobile = require('./EditBtnMobile');

var _EditBtnMobile2 = _interopRequireDefault(_EditBtnMobile);

var _MarchingAnts = require('../MarchingAnts');

var _MarchingAnts2 = _interopRequireDefault(_MarchingAnts);

var _actions = require('../Message/actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var collapseTimeNormal = 400;
var collapseMediaTimeNormal = 500;
var loadingTimeNormal = 1200;
var timeMultiplier = 0.9;
var collapseTime = timeMultiplier * collapseTimeNormal;
var collapseMediaTime = timeMultiplier * collapseMediaTimeNormal;
var loadingTime = timeMultiplier * loadingTimeNormal;

var mapDispatchToProps = {
  animationStart: _actions.chatBubbleAnimationStartAction,
  animationEnd: _actions.chatBubbleAnimationEndAction
};

var BubbleUgly = (_dec = (0, _reactRedux.connect)(null, mapDispatchToProps), _dec(_class = (_temp = _class2 = function (_Component) {
  _inherits(BubbleUgly, _Component);

  function BubbleUgly(props) {
    _classCallCheck(this, BubbleUgly);

    var _this = _possibleConstructorReturn(this, (BubbleUgly.__proto__ || Object.getPrototypeOf(BubbleUgly)).call(this, props));

    _this.state = {
      loading: false,
      collapse: false,
      collapsePhase: '',
      showEditBtn: false
    };

    _this.handleEditClick = function (event) {
      if (event && event.preventDefault) {
        event.preventDefault();
      }

      var _this$props = _this.props,
          onEditClick = _this$props.onEditClick,
          feId = _this$props.feId,
          questionId = _this$props.questionId;


      onEditClick(feId, questionId);
    };

    _this.handleBubbleClick = function () {
      _this.toggleShowEditBtn();
    };

    _this.animationsTimers = {};
    return _this;
  }

  _createClass(BubbleUgly, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.showAnimations) {
        this.props.animationStart();

        // CASE 1 - no typing indicator
        this.answerAnimation();

        // CASE 2 - typing indicator
        this.botAnimation();

        // CASE 3 - media animation
        this.mediaAnimation();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      Object.values(this.animationsTimers).map(clearTimeout);
    }
  }, {
    key: 'onLastAnimationStart',
    value: function onLastAnimationStart() {
      var duration = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1000;
      var animationEnd = this.props.animationEnd;


      this.animationsTimers.animation1 = setTimeout(animationEnd, duration);
    }
  }, {
    key: 'setBubbleHeight',
    value: function setBubbleHeight(callback) {
      this.bubbleWrap.style.position = 'absolute';
      this.bubbleWrap.style.visibility = 'hidden';
      var bubbleHeight = this.bubble.offsetHeight;
      this.bubble.style.height = bubbleHeight + 'px';
      this.bubbleWrap.style.position = 'relative';
      this.bubbleWrap.style.visibility = 'visible';
      if (callback) {
        callback();
      }
    }
  }, {
    key: 'restoreDefaultBubbleHeight',
    value: function restoreDefaultBubbleHeight() {
      if (this.bubble) {
        this.bubble.style.height = 'auto';
      }
    }
  }, {
    key: 'mediaAnimation',
    value: function mediaAnimation() {
      var _this2 = this;

      if (this.props.mediaUrl) {
        // First phase - animate height
        this.setState({
          collapse: true
        });

        // Second phase - show the media bubble
        this.animationsTimers.animation2 = setTimeout(function () {
          _this2.setState({
            collapse: false
          });

          _this2.onLastAnimationStart();
        }, collapseMediaTime);
      }
    }
  }, {
    key: 'answerAnimation',
    value: function answerAnimation() {
      var _this3 = this;

      if (!this.props.showMarchingAnts && !this.props.mediaUrl) {
        // Set bubble height needed for the animation
        this.setBubbleHeight(function () {
          // Start the animation
          _this3.setState({
            collapse: true,
            collapsePhase: 'third'
          });

          // finish the animation
          _this3.animationsTimers.animation3 = setTimeout(function () {
            _this3.setState({
              collapse: false
            });
            _this3.restoreDefaultBubbleHeight();
            _this3.onLastAnimationStart();
          }, collapseTime);
        });
      }
    }
  }, {
    key: 'botAnimation',
    value: function botAnimation() {
      var _this4 = this;

      if (this.props.showMarchingAnts && !this.props.mediaUrl) {
        this.setState({
          collapse: true,
          collapsePhase: 'first'
        });

        // Second phase - show the loading animation for $loadingTime
        this.animationsTimers.animation4 = setTimeout(function () {
          // But first set the container height to minimum 48px(loading bubble height) to prevent jumpy animation
          _this4.bubbleOuterWrap.style.minHeight = '48px';
          _this4.setState({
            loading: true,
            collapse: false,
            collapsePhase: ''
          });
        }, collapseTime);

        // Third phase - hide the loading animation and toggle height to text bubble size
        this.animationsTimers.animation5 = setTimeout(function () {
          // hide loading animation
          _this4.setState({
            loading: false
          });
          // toggle height to text bubble size
          _this4.setBubbleHeight(function () {
            _this4.setState({
              collapse: true,
              collapsePhase: 'second'
            });
          });
        }, collapseTime + loadingTime);
        // Fifth phase - display text bubble
        this.animationsTimers.animation6 = setTimeout(function () {
          _this4.setState({
            collapse: false,
            collapsePhase: ''
          });
          _this4.restoreDefaultBubbleHeight();
          _this4.onLastAnimationStart();
        }, collapseTime * 2 + loadingTime);
      }
    }
  }, {
    key: 'toggleShowEditBtn',
    value: function toggleShowEditBtn() {
      var showEditBtn = this.state.showEditBtn;

      this.setState({
        showEditBtn: !showEditBtn
      });
    }
  }, {
    key: 'renderTimestamp',
    value: function renderTimestamp() {
      var _props = this.props,
          time = _props.time,
          timeFormat = _props.timeFormat,
          renderTimestamp = _props.renderTimestamp,
          messagesGroup = _props.messagesGroup;


      if (renderTimestamp) {
        var timestampProps = {
          time: time,
          timeFormat: timeFormat,
          messagesGroup: messagesGroup
        };

        return renderTimestamp(timestampProps);
      }

      if (!time || !timeFormat) {
        return null;
      }

      var timeFormatted = _fecha2.default.format(time, timeFormat);

      return _react2.default.createElement(
        'div',
        { className: 'chat-msg-timestamp' },
        timeFormatted
      );
    }
  }, {
    key: 'renderBubble',
    value: function renderBubble() {
      var _this5 = this;

      var _props2 = this.props,
          children = _props2.children,
          message = _props2.message,
          messagesGroup = _props2.messagesGroup,
          renderAdditionalMessageContent = _props2.renderAdditionalMessageContent,
          renderMessageFooter = _props2.renderMessageFooter,
          showMarchingAnts = _props2.showMarchingAnts;
      var _state = this.state,
          collapse = _state.collapse,
          collapsePhase = _state.collapsePhase,
          loading = _state.loading;

      var editButtons = this.renderEditButtons();
      var isLoading = loading && !collapse;
      var isLoaded = (showMarchingAnts && !loading || !showMarchingAnts) && !collapse;
      var collapseClass = collapse ? 'chat-msg-bubble--collapse chat-msg-bubble--collapse_' + collapsePhase : '';
      var content = void 0;

      if (loading && showMarchingAnts || collapsePhase === 'first') {
        content = _react2.default.createElement(_MarchingAnts2.default, null);
      } else {
        content = _react2.default.createElement(
          'div',
          { className: 'chat-msg-text' },
          _react2.default.createElement('span', { dangerouslySetInnerHTML: { __html: children } }),
          renderAdditionalMessageContent && renderAdditionalMessageContent(message)
        );
      }

      return _react2.default.createElement(
        'div',
        {
          className: 'chat-msg-bubble-outer_wrap',
          ref: function ref(bubbleOuterWrap) {
            _this5.bubbleOuterWrap = bubbleOuterWrap;
          }
        },
        _react2.default.createElement(
          'div',
          {
            className: 'chat-msg-bubble-wrap',
            ref: function ref(bubbleWrap) {
              _this5.bubbleWrap = bubbleWrap;
            }
          },
          editButtons,
          _react2.default.createElement(
            'div',
            {
              className: 'chat-msg-bubble ' + (isLoading ? 'chat-msg-bubble--loading' : '') + ' ' + (isLoaded ? 'chat-msg-bubble--loaded' : '') + ' ' + collapseClass,
              ref: function ref(bubble) {
                _this5.bubble = bubble;
              },
              onClick: this.handleBubbleClick,
              role: 'link',
              tabIndex: 0
            },
            _react2.default.createElement(
              'div',
              { className: 'chat-msg-bubble-inner' },
              _react2.default.createElement('div', { className: 'chat-msg-bubble-bg' }),
              content
            )
          ),
          renderMessageFooter(),
          this.renderTimestamp()
        )
      );
    }
  }, {
    key: 'renderEditButtons',
    value: function renderEditButtons() {
      var onEditClick = this.props.onEditClick;

      var canEdit = (0, _isFunction3.default)(onEditClick);

      if (!canEdit) {
        return null;
      }

      return [_react2.default.createElement(_EditBtnDesktop2.default, {
        show: true,
        onEditClick: this.handleEditClick
      }), _react2.default.createElement(_EditBtnMobile2.default, {
        show: this.state.showEditBtn,
        onEditClick: this.handleEditClick
      })];
    }
  }, {
    key: 'renderMediaBubble',
    value: function renderMediaBubble() {
      var isLoading = this.props.showAnimations && this.state.loading && !this.state.collapse;
      var isLoaded = (!this.props.showAnimations || this.props.showMarchingAnts && !this.state.loading || !this.props.showMarchingAnts) && !this.state.collapse;
      var collapseClass = this.state.collapse ? 'chat-msg-media--collapse' : '';

      return _react2.default.createElement(
        'div',
        { className: 'chat-msg-media ' + (isLoading ? 'chat-msg-media--loading' : '') + ' ' + (isLoaded ? 'chat-msg-media--loaded' : '') + ' ' + collapseClass },
        _react2.default.createElement(
          'div',
          { className: 'chat-msg-media-thumb' },
          _react2.default.createElement('img', { src: this.props.mediaUrl, alt: '', className: 'chat-msg-media-thumb-img' })
        ),
        _react2.default.createElement(
          'div',
          { className: 'chat-msg-media-bubble' },
          _react2.default.createElement('div', { className: 'chat-msg-media-bubble-bg' }),
          _react2.default.createElement(
            'div',
            { className: 'chat-msg-text' },
            _react2.default.createElement(
              'div',
              { className: 'chat-msg-text' },
              this.props.children
            )
          ),
          this.renderTimestamp()
        )
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var bubble = this.props.mediaUrl ? this.renderMediaBubble() : this.renderBubble();

      return bubble;
    }
  }]);

  return BubbleUgly;
}(_react.Component), _class2.propTypes = {
  animationEnd: _propTypes2.default.func.isRequired,
  animationStart: _propTypes2.default.func.isRequired,
  children: _propTypes2.default.any,
  feId: _propTypes2.default.string.isRequired,
  mediaUrl: _propTypes2.default.any,
  message: _propTypes2.default.object.isRequired,
  messagesGroup: _propTypes2.default.object,
  onEditClick: _propTypes2.default.func,
  questionId: _propTypes2.default.number,
  renderAdditionalMessageContent: _propTypes2.default.func,
  renderMessageFooter: _propTypes2.default.func.isRequired,
  renderTimestamp: _propTypes2.default.func,
  showAnimations: _propTypes2.default.bool,
  showMarchingAnts: _propTypes2.default.bool,
  time: _propTypes2.default.string,
  timeFormat: _propTypes2.default.string
}, _class2.defaultProps = {
  children: undefined,
  feId: '',
  mediaUrl: undefined,
  messagesGroup: {},
  onEditClick: null,
  questionId: null,
  renderAdditionalMessageContent: undefined,
  renderTimestamp: undefined,
  showAnimations: false,
  showMarchingAnts: undefined,
  time: null,
  timeFormat: null
}, _temp)) || _class);
exports.default = BubbleUgly;