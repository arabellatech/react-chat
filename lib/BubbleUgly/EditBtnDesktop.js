"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EditBtnDesktop = function EditBtnDesktop(props) {
  if (!props.show) {
    return null;
  }

  return _react2.default.createElement(
    "button",
    {
      className: "chat-edit",
      onClick: props.onEditClick
    },
    _react2.default.createElement(
      "span",
      { className: "chat-edit-icon" },
      _react2.default.createElement("i", { className: "icon-edit" })
    ),
    _react2.default.createElement(
      "span",
      { className: "chat-edit-text" },
      "EDIT"
    )
  );
};

exports.default = EditBtnDesktop;