"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EditBtnMobile = function EditBtnMobile(props) {
  if (!props.show) {
    return null;
  }

  return _react2.default.createElement(
    "button",
    {
      className: "chat-change_answer chat-change_answer--visible hidden-desktop",
      onClick: props.onEditClick
    },
    "Change answer?"
  );
};

exports.default = EditBtnMobile;