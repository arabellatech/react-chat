'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Avatar = function Avatar(props) {
  var avatarStyle = {
    backgroundImage: 'url(\'' + props.src + '\')'
  };

  var image = _react2.default.createElement('div', { className: 'chat-msg-avatar-thumb', style: avatarStyle });
  var alt = _react2.default.createElement(
    'div',
    { className: 'chat-msg-avatar-letter' },
    props.alt
  );

  var avatar = props.src ? image : alt;

  return _react2.default.createElement(
    'div',
    { className: 'chat-msg-user' },
    _react2.default.createElement(
      'div',
      { className: 'chat-msg-avatar' },
      avatar
    )
  );
};


Avatar.defaultProps = {
  alt: '!',
  src: undefined
};

exports.default = Avatar;